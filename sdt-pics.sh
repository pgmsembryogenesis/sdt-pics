#!/bin/bash
# testinfra_execution layout = runIC runID groupID suffix_statemod  (eg. : EXE-;C001a;group01;_test)
# 1) run : if EXEC than testcase will be run with ./sdt-pics.sh command
# 2) runID : testcase label
# 3) groupID : optional, to be used with -g 
# 4) suffix_statemod : optional , will be concatenated to INPUT_STATE_MOD (testinfra.xls) (if you want to redirect to another statemodifier file)
# useage
# ./sdt-pics.sh                  => all EXEC testcases will be run
# ./sdt-pics.sh restart 5        => all EXEC testcases will be run, all restarted from fifth module
# ./sdt-pics.sh C001f            => C001f testcase will be run
# ./sdt-pics.sh C001f 5          => C001f testcase will be run, restart from fifth module
# ./sdt-pics.sh -g paperTRA      => all testcases with grouplabel "paperTRA" will be run
# ./sdt-pics.sh -g -b paperTRA 5 => all testcases with grouplabel "paperTRA" will be run, all restarted from fifth module, with no debugging

# naming testcases
#  1) modulenotation = M012b : module test of module 012, run b (C012b = chain starting from module 012
#  2) free notation = D020 or T001
#  3) group notation = 7cell0304 : testcase belonging to group '7cell' third testcase (=replicate), fourth run_nb = state_nb = separate tab in excel
#################################################TESTINFRASTRUCTURE MAIN DRIVER#############################################

#Functions
function throw()
{
	rsync "${DATA_FOLDER}/consoleoutput/consoleOutput_${runID}.txt"  "${OUTPUT_FOLDER}/_LOG"
	exit $1
}

timestamp() {
  date +"%T" # current time
}

start=$(date +%s)

source testinfra_argparser.sh

#source ~/anaconda3/etc/profile.d/conda.sh #get access to conda functions (no longer needed)
# echo "$HOSTNAME"
source testinfra_config  # set paths and docker setup for the current environment
SCRIPT_FOLDER="${SCRIPT_FOLDER/./$PWD}" #resolve relative paths
DATA_FOLDER="${DATA_FOLDER/./$PWD}" #resolve relative paths

if [[ "$batch_mode" == true ]]; then
	echo ""
fi

# echo "script folder = ${SCRIPT_FOLDER} // data folder = ${DATA_FOLDER} // param folder = ${PARAM_FOLDER} "

#Set derived locations----------------------------------------------------------------------------------------------------------------------------------------
declare -r INFILE="${PARAM_FOLDER}/testinfra_execution.txt"  #example record EXEC;M10001;group01;_paper_data;
declare -r XML_OUTPUT_FOLDER="${DATA_FOLDER}/XML"  #this also points to the outputfolder
#declare -r XML_INPUT="${PARAM_FOLDER}/PARAMS.xml"  
declare -r BACKUP_FOLDER="${DATA_FOLDER}/BACKUP"
  # docker equivalents
declare -r ROOT_DOCKER="/home/docker"
declare -r SCRIPT_FOLDER_DOCKER="${ROOT_DOCKER}/SCRIPT"  # 
declare -r DATA_FOLDER_DOCKER="${ROOT_DOCKER}/DATA"  # 
#declare -r PARAM_FOLDER_DOCKER=$SCRIPT_FOLDER_DOCKER # #moved to config
declare -r XML_OUTPUT_FOLDER_DOCKER="DATA/XML"  


a_mpacts_container=(sphere_meshing.py mpacts_PiCS.py mpacts_PiCS_remesh.py)
# start docker containers
if [[ $MPACTS_CONTAINER != "" ]] ; then
	if [ $(docker inspect -f '{{.State.Running}}' ${MPACTS_CONTAINER}) = "false" ]; then
		# printf "Starting $MPACTS_CONTAINER "
		docker start ${MPACTS_CONTAINER}; 
	fi
	printf "$MPACTS_CONTAINER is running : "
	containerID= docker ps | awk '$2 ~ /mpacts/' | awk 'NR==1{print $1}'
	
fi

if [[ $SDT_CONTAINER != "" ]] ; then
	if [ $(docker inspect -f '{{.State.Running}}' ${SDT_CONTAINER}) = "false" ]; then
		printf "Starting $SDT_CONTAINER "
		docker start ${SDT_CONTAINER}; 
	fi
	printf "$SDT_CONTAINER is running : "
	containerID= docker ps | awk '$2 ~ /sdt/' | awk 'NR==1{print $1}'
fi


#EXECUTE TESTCASES========================================================================================================================================
	
filelines=`cat $INFILE`
for line in $filelines ; do				#looping over testcases in testinfra_execution.txt
	IFS=';' read runIC runID groupID suffix_statemod suffix_state<<<$line  #parse line
	if  [[ ($flag_use_exec == true   &&   $runIC == "EXEC" ) ||  ( $flag_use_exec == false   &&  $runID == $testcase ) ||  ( $grouping == true   &&  ${groupID} == ${testcase}* ) ]]
	then
		echo "========================================================= executing $runID ======================================================================================"
		# echo "run parameters parsed : " $runIC " run="  $runID " group="  $groupID " statemod=" $suffix_statemod  " state base PARAM_Vx.xml="$suffix_state
		export IFS=","
		if [ $dry_run == true ]
		then
			continue
		fi

		#direct to state setters
		declare XML_INPUT="${PARAM_FOLDER}/PARAMS${suffix_state}.xml"  
		declare XML_INPUT_DOCKER="${PARAM_FOLDER_DOCKER}/PARAMS${suffix_state}.xml"
		declare INPUT_STATE_MOD="${PARAM_FOLDER}/testinfra${suffix_statemod}.xlsx"
		declare INPUT_STATE_MOD_DOCKER="${PARAM_FOLDER_DOCKER}/testinfra${suffix_statemod}.xlsx"

		if [[ "$SDT_CONTAINER" != "" ]] ; then
			# printf "start docker run for xml_modifier\n"
			docker exec -t ${SDT_CONTAINER}  python "${SCRIPT_FOLDER_DOCKER}/xml_modifier.py"  $INPUT_STATE_MOD_DOCKER $XML_OUTPUT_FOLDER_DOCKER $runID $XML_INPUT_DOCKER $DATA_FOLDER_DOCKER $SCRIPT_FOLDER_DOCKER  $PARAM_FOLDER_DOCKER $restart_step
		else
			python -Wignore  "${SCRIPT_FOLDER}/xml_modifier.py"  $INPUT_STATE_MOD $XML_OUTPUT_FOLDER $runID $XML_INPUT $DATA_FOLDER $SCRIPT_FOLDER  $PARAM_FOLDER $restart_step
		fi

		if [ $? -eq 0 ] 
		then 
		  # echo "State succesfully initialised" 
		  echo ""
		else 
		  echo "<ERROR> Error in initialising state. " >&2 
		  throw 1
		fi

		#EXTRACT RUN VARIABLES and OUTPUT FOLDER
		modules=$(grep -o -P -m 1 '(?<=modules value=\").+(?=\")' "${XML_OUTPUT_FOLDER}/testinfra_${runID}.xml")
		modules_snapshot=$(grep -o -P '(?<=modules_snapshot value=\").+(?=\")' "${XML_OUTPUT_FOLDER}/testinfra_${runID}.xml")
		modules_standard_copy="xml_modifier.py,param_XML.py"
		if [ "${modules_snapshot}" == "None" ] ; then
			modules_snapshot="${modules},${modules_standard_copy}"
		else
			modules_snapshot="${modules},${modules_standard_copy},${modules_snapshot}" 
		fi

		OUTPUT_FOLDER=$(grep -o -P '(?<=root_output_folder value=\").+(?=\")' "${XML_OUTPUT_FOLDER}/testinfra_${runID}.xml")
		OUTPUT_FOLDER_ROOT_SUB=$(grep -o -P '(?<=root_output_subfolder value=\").+(?=\")' "${XML_OUTPUT_FOLDER}/testinfra_${runID}.xml")
		if [ "$OUTPUT_FOLDER_ROOT_SUB" != "" ] ; then
			echo "OUTPUT_FOLDER_ROOT_SUB is added = ${OUTPUT_FOLDER_ROOT_SUB}"
			OUTPUT_FOLDER="${OUTPUT_FOLDER}/${OUTPUT_FOLDER_ROOT_SUB}"
		fi
		OUTPUT_FOLDER="${OUTPUT_FOLDER/$DATA_FOLDER_DOCKER/$DATA_FOLDER}"

		# echo "OUTPUT_FOLDER = ${OUTPUT_FOLDER}"
		# echo "DATA_FOLDER = ${DATA_FOLDER}"

		backupIC=$(grep -o -P '(?<=backupIC value=\").+(?=\")' "${XML_OUTPUT_FOLDER}/testinfra_${runID}.xml")

		#take a snapshot of selected pythonmodules
		for module in $modules_snapshot; do
			#echo "snapshot module=" $module  "from-->" "${SCRIPT_FOLDER}/${module}" "TO->>"  "${OUTPUT_FOLDER}/_LOG/modules_snapshot/"
			rsync  -r "${SCRIPT_FOLDER}/${module}"  "${OUTPUT_FOLDER}/_LOG/modules_snapshot/"
		done

		rsync  $INPUT_STATE_MOD  "${OUTPUT_FOLDER}/_LOG"
		mv -f "${OUTPUT_FOLDER}/_LOG/testinfra${suffix_statemod}.xlsx" "${OUTPUT_FOLDER}/_LOG/testinfra_${runID}.xlsx"  #rename
		rsync  "${XML_OUTPUT_FOLDER}/testinfra_${runID}.xml"  "${OUTPUT_FOLDER}/_LOG"  
			
		#EXECUTING
		ix_module=0
		set -o pipefail # get exit code of command, not tee
		printf "<START RUN> ${runID} $(date) \n " >  "${DATA_FOLDER}/consoleoutput/consoleOutput_${runID}.txt"
		for module in $modules; do            				#looping over executing steps for one
			
			ix_module=$(( ix_module + 1 ))
			ic_module_executed=false

			if [ $ix_module -lt $restart_step ] ; then
				continue
			fi

			echo -e "\n  -------------------------------------------------------------------------------------------$module "
			printf "<MODULE> ${runID} ${module} $(timestamp) \n " >>  "${DATA_FOLDER}/consoleoutput/consoleOutput_${runID}.txt"

			# R run
			if [[ "$module" == *".R" ]]; then
				Rscript "${SCRIPT_FOLDER}/${module}" 2>&1 | tee -a "${DATA_FOLDER}/consoleoutput/consoleOutput_${runID}.txt"
				rc=$?
				ic_module_executed=true
			fi

			if [[ "$module" == *".ipynb" ]]; then
				jupyter nbconvert --to notebook --execute "${SCRIPT_FOLDER}/${module}" 2>&1 | tee -a  "${DATA_FOLDER}/consoleoutput/consoleOutput_${runID}.txt"
				rc=$?
				ic_module_executed=true
			fi

			if [[ "$module" == "sphere_meshing_CGAL.py" ]]; then
				input_file_spheres=$(grep -o -P '(?<=input_file_spheres value=\").+(?=\")' "${XML_OUTPUT_FOLDER}/testinfra_${runID}.xml")
				output_folder_meshes=$(grep -o -P '(?<=output_folder_meshes value=\").+(?=\")' "${XML_OUTPUT_FOLDER}/testinfra_${runID}.xml")
				shrink_factor=$(grep -o -P '(?<=shrink_factor value=\").+(?=\")' "${XML_OUTPUT_FOLDER}/testinfra_${runID}.xml")
				mkdir -p "${OUTPUT_FOLDER}/010_sphere_meshing"  -m 777 #temp
				
				if [[ "$SDT_CONTAINER" != "" ]] ; then
					# printf "start docker run for C binary sphere_meshing_CGAL"
					docker exec -t ${SDT_CONTAINER}  "${SCRIPT_FOLDER_DOCKER}/sphere_meshing_CGAL" ${input_file_spheres} ${output_folder_meshes} ${shrink_factor}
				else
					# echo "  -  starting binary sphere_meshing_CGAL ; ${SCRIPT_FOLDER}/sphere_meshing_CGAL ${input_file_spheres} ${output_folder_meshes} ${shrink_factor}"
					"${SCRIPT_FOLDER}/sphere_meshing_CGAL" ${input_file_spheres} ${output_folder_meshes} ${shrink_factor} 2>&1 | tee -a  "${DATA_FOLDER}/consoleoutput/consoleOutput_${runID}.txt"
				fi
				rc=$?
			fi

			# Mpacts run (always in container)
			for i in "${a_mpacts_container[@]}"; do       # run under docker image mpacts_container
				if [ "$i" == $module ] ; then

					if [[ "$ic_dockerenv" == "true" ]] ; then
						python -Wignore "${SCRIPT_FOLDER}/${module}" "${XML_OUTPUT_FOLDER}/testinfra_${runID}.xml"  #already in docker env 
					else
						# printf "start docker run for $module within $MPACTS_CONTAINER \n"
						docker exec -t ${MPACTS_CONTAINER}  python "${SCRIPT_FOLDER_DOCKER}/${module}" "${XML_OUTPUT_FOLDER_DOCKER}/testinfra_${runID}.xml"  2>&1 | tee -a  "${DATA_FOLDER}/consoleoutput/consoleOutput_${runID}.txt"
						# if  [[ "$batch_mode" == true ]] ; then
						# 	docker exec -t ${MPACTS_CONTAINER}  python "${SCRIPT_FOLDER_DOCKER}/${module}" "${XML_OUTPUT_FOLDER_DOCKER}/testinfra_${runID}.xml"  2>&1 | tee -a  "${DATA_FOLDER}/consoleoutput/consoleOutput_${runID}.txt"
						# else
						# 	docker exec -it ${MPACTS_CONTAINER}  python "${SCRIPT_FOLDER_DOCKER}/${module}" "${XML_OUTPUT_FOLDER_DOCKER}/testinfra_${runID}.xml" 2>&1 | tee -a   "${DATA_FOLDER}/consoleoutput/consoleOutput_${runID}.txt"
						# fi
					fi
					rc=$?
					ic_module_executed=true
					if [ $(docker inspect -f '{{.State.Running}}' ${MPACTS_CONTAINER}) = "false" ]; then
						printf "Starting $MPACTS_CONTAINER "
						docker start ${MPACTS_CONTAINER}; 
					fi
				fi
			done

			
			# python3 run (=default)
			if [ "$ic_module_executed" != true ] ; then

				if [[ "$SDT_CONTAINER" != "" ]] ; then
					# printf "start docker run for $module under $SDT_CONTAINER \n"
					docker exec -t ${SDT_CONTAINER}  python "${SCRIPT_FOLDER_DOCKER}/${module}" "${XML_OUTPUT_FOLDER_DOCKER}/testinfra_${runID}.xml"   2>&1 | tee -a  "${DATA_FOLDER}/consoleoutput/consoleOutput_${runID}.txt"
				else
					python "${SCRIPT_FOLDER}/${module}" "${XML_OUTPUT_FOLDER}/testinfra_${runID}.xml"   2>&1 | tee -a "${DATA_FOLDER}/consoleoutput/consoleOutput_${runID}.txt"
					# python -Wignore "${SCRIPT_FOLDER}/${module}" "${XML_OUTPUT_FOLDER}/testinfra_${runID}.xml"   2>&1 | tee -a "${DATA_FOLDER}/consoleoutput/consoleOutput_${runID}.txt"
				fi
				rc=$?
				ic_module_executed=true

			fi
			
			if [ ${rc} -eq 0 ] 
			then
				echo "Successful execution of module ${module}" >> "${DATA_FOLDER}/consoleoutput/consoleOutput_${runID}.txt"
			else 
				echo "<ERROR> Error in module ${module} (returncode=${rc}) "  | tee -a "${DATA_FOLDER}/consoleoutput/consoleOutput_${runID}.txt"
				rsync "${DATA_FOLDER}/consoleoutput/consoleOutput_${runID}.txt"  "${OUTPUT_FOLDER}/_LOG"
				break 1
			fi
		done
		rsync "${DATA_FOLDER}/consoleoutput/consoleOutput_${runID}.txt"  "${OUTPUT_FOLDER}/_LOG"    #copy consoleoutput for debugging
		echo "========================================================= end $runID ======================================================================================"
	else
		printf ""
	fi
done


set +o pipefail # restore default behaviour

end=$(date +%s)
echo "timestamp: $(timestamp)"
awk -v t=$SECONDS 'BEGIN{t=int(t*1000); printf "Elapsed Time (HH:MM:SS): %d:%02d:%02d\n", t/3600000, t/60000%60, t/1000%60}'

# This prevents script chaining (exits also encompassing script)
# if [[ "$ic_dockerenv" != "true" ]] ; then  
# 	$SHELL  #keep shell open
# fi

# rc=$?
# echo "returncode sdt-pics.sh= $rc.  Resetting to rc=0 in any case"
exit 0