'''
Created on 19dec 2021
pre processing modules
@author: wimth

'''

from helper_functions import zoom_data, imsave_fiji
from z_membrane_locator import locate_z_membrane
from scipy.spatial import Delaunay, ConvexHull
from skimage.draw import polygon2mask
# import meshplex
import matplotlib.path as mplPath
import open3d as o3d
import warnings
import numpy as np
import pandas as pd

from skimage.filters import threshold_otsu, frangi
from skimage.morphology import (remove_small_objects,
                                reconstruction, skeletonize, dilation)

def _calc_circumcenter_circumsphere_triangle_vectorized(triangle_coord_array):
    ''' alternative to meshplex :  (mostly exactly the same results)
    An alternative implementation based on http://mathworld.wolfram.com/Circumsphere.html because of issues with the initial implementation from the Berkeley page.
    Vectorized version for use with multiple triangles in triangle_coord_array -- the latter should have shape (N, 3, 3).
    from https://github.com/tylerjereddy/py_sphere_Voronoi/blob/master/circumcircle.py
    '''
    num_triangles = triangle_coord_array.shape[0]
    # reshape the triangle_coord_array to place all triangles
    # consecutively without nesting
    triangle_coord_array = np.reshape(
        triangle_coord_array,
        (triangle_coord_array.shape[0] *
         triangle_coord_array.shape[1],
         triangle_coord_array.shape[2]))
    array_stacked_a_matrices = np.hstack(
        (triangle_coord_array, np.ones(
            (num_triangles * 3, 1))))
    first_column_array_determinant_arrays = triangle_coord_array[..., 0] ** 2 + \
        triangle_coord_array[..., 1] ** 2 
    first_column_array_determinant_arrays = first_column_array_determinant_arrays[:, np.newaxis]
    final_column_array_determinant_arrays = np.ones((first_column_array_determinant_arrays.shape[0], 1))
    array_D_x_contents_before_determinant_calculation = np.hstack((first_column_array_determinant_arrays, triangle_coord_array[..., 1:], final_column_array_determinant_arrays))
    array_D_y_contents_before_determinant_calculation = np.hstack(
        (first_column_array_determinant_arrays, triangle_coord_array[..., :-1], final_column_array_determinant_arrays))

    #split the arrays back to stacks of matrices
    array_D_x_contents_before_determinant_calculation = np.array(
        np.split(array_D_x_contents_before_determinant_calculation, num_triangles))
    array_D_y_contents_before_determinant_calculation = np.array(
        np.split(array_D_y_contents_before_determinant_calculation, num_triangles))
    array_a_contents_before_determinant_calculation = np.array(
        np.split(array_stacked_a_matrices, num_triangles))

    #compute the determinants for the stacks of matrices assembled above
    array_Dx_values = np.linalg.det(
        array_D_x_contents_before_determinant_calculation)
    array_Dy_values = - \
        np.linalg.det(array_D_y_contents_before_determinant_calculation)
    array_a_values = np.linalg.det(
        array_a_contents_before_determinant_calculation)
    array_denominator_values = 2. * array_a_values

    # avoid divide by zero
    np.seterr(divide='ignore')
    array_denominator_values[np.where(
        array_denominator_values == 0)] = 1.e-12

    array_x0_values = array_Dx_values / array_denominator_values
    array_y0_values = array_Dy_values / array_denominator_values

    circumcenter_array = np.hstack(
        (array_x0_values[:, np.newaxis], array_y0_values[:, np.newaxis]))
    return circumcenter_array

def filter_rescale(stack, kwargs, p_out=None):

    l_out = zoom_data(stack, verbose=False, **kwargs)
    img_rescaled, zoom, spacing = l_out

    if p_out:
        with warnings.catch_warnings():
            p_out.parent.mkdir(parents=True, exist_ok=True)
            imsave_fiji(p_out, img_rescaled.astype('uint16'))

    return img_rescaled, zoom


def filter_frangi(a_stack, kwargs, scale_max=True, p_out=None):
    a_frangi = np.zeros(a_stack.shape, dtype='float32')

    for ix_slice, a_slice in enumerate(a_stack):
        a_frangi[ix_slice] = frangi(a_slice, **kwargs)

    if scale_max:
        if np.max(a_frangi) > 0:
            a_frangi = a_frangi * (10000 / np.max(a_frangi))

    if p_out:
        with warnings.catch_warnings():
            p_out.parent.mkdir(parents=True, exist_ok=True)
            imsave_fiji(p_out, a_frangi.astype('float32'))

    return a_frangi


def _set_no_signal_range(l_has_signal):
    a_has_signal_range = np.array(l_has_signal)
    a_has_signal_range.fill(True)

    for ix_z, has_signal in enumerate(reversed(l_has_signal)):
        if has_signal:
            a_has_signal_range[-ix_z:] = False
            break

    for ix_z, has_signal in enumerate(l_has_signal):
        if has_signal:
            a_has_signal_range[:ix_z] = False
            break
    return list(a_has_signal_range)


def set_thresholds(a_stack, df_thresholds=None, seed_thr_divide_factor=1,
                   membrane_acceptance_level=3,
                   no_signal_thresh=60, z_range=None):

    l_otsu, l_abs_thresh_slice_seed,\
        l_abs_thresh_slice, l_has_signal = [], [], [], []

    l_calc_switch = [True, True, True]
    if df_thresholds is not None:
        if 'abs_thresh_slice_seed' in df_thresholds:
            l_abs_thresh_slice_seed = \
                list(df_thresholds['abs_thresh_slice_seed'])
            l_calc_switch[0] = False
        if 'abs_thresh_slice' in df_thresholds:
            l_abs_thresh_slice = list(df_thresholds['abs_thresh_slice'])
            l_calc_switch[1] = False
        if 'has_signal' in df_thresholds:
            l_has_signal = list(df_thresholds['has_signal'])
            l_calc_switch[2] = False

    for ix_slice, a_slice in enumerate(a_stack):
        if a_slice[a_slice > 0].any():
            l_otsu.append(threshold_otsu(a_slice))
            if l_calc_switch[0]:
                l_abs_thresh_slice_seed.append(
                    l_otsu[-1] / seed_thr_divide_factor)  # use otsu as seed
            if l_calc_switch[1]:
                l_abs_thresh_slice.append(l_abs_thresh_slice_seed[ix_slice]
                                          / membrane_acceptance_level)
            if l_calc_switch[2]:
                if z_range:
                    l_has_signal.append(
                        True if
                        (ix_slice >= z_range[0] - 1
                         and ix_slice <= z_range[1] - 1) else False)
                else:
                    l_has_signal.append(
                        l_abs_thresh_slice_seed[ix_slice] > no_signal_thresh)
        else:
            l_otsu.append(0)
            if l_calc_switch[0]:
                l_abs_thresh_slice_seed.append(0)
            if l_calc_switch[1]:
                l_abs_thresh_slice.append(0)
            if l_calc_switch[2]:
                l_has_signal.append(False)

    if not z_range:
        l_has_signal = _set_no_signal_range(l_has_signal)

    if df_thresholds is None:
        df_thresholds = pd.DataFrame()

    df_thresholds['otsu'] = l_otsu
    df_thresholds['abs_thresh_slice_seed'] = l_abs_thresh_slice_seed
    df_thresholds['abs_thresh_slice'] = l_abs_thresh_slice
    df_thresholds['has_signal'] = l_has_signal


    return df_thresholds


def filter_reconstruction(a_stack, df_thresholds, kwargs, p_out=None):
    '''
    This is a downhill filter. It expects as input the frangi output
    '''

    a_threshold = np.zeros(a_stack.shape, dtype='uint16')
    for ix_slice, a_slice in enumerate(a_stack):
        thresholds = df_thresholds.iloc[ix_slice]
        seed = a_slice.copy()
        if thresholds.has_signal:
            seed[seed < thresholds['abs_thresh_slice_seed']] = 0
            a_threshold[ix_slice] = reconstruction(
                seed=seed,
                mask=a_slice, **kwargs) >= thresholds['abs_thresh_slice']
        else:
            a_threshold[ix_slice] = np.zeros_like(a_slice)

    if p_out:
        with warnings.catch_warnings():
            p_out.parent.mkdir(parents=True, exist_ok=True)
            imsave_fiji(p_out, a_threshold.astype('uint16'))

    return a_threshold


def filter_remove_small_objects(a_stack, kwargs, p_out=None):
    '''
    Clean up : remove 3D blobs
    '''
    remove_small_objects(a_stack.astype(bool), in_place=True, **kwargs)

    if p_out:
        with warnings.catch_warnings():
            p_out.parent.mkdir(parents=True, exist_ok=True)
            imsave_fiji(p_out, a_stack.astype('uint16'))

    return a_stack


def filter_skeletonize(a_stack, p_out=None):
    '''
    skeletonize a binary image
    '''

    a_membrane = np.zeros(a_stack.shape, dtype='uint16')

    for ix_slice, a_slice in enumerate(a_stack):
        a_membrane[ix_slice] = skeletonize(a_slice)

    if p_out:
        with warnings.catch_warnings():
            p_out.parent.mkdir(parents=True, exist_ok=True)
            imsave_fiji(p_out, a_membrane.astype('uint16'))

    return a_membrane


def filter_dilation(a_stack, p_out=None):
    '''
    dilate a binary image
    '''

    a_membrane = np.zeros(a_stack.shape, dtype='uint16')

    for ix_slice, a_slice in enumerate(a_stack):
        a_membrane[ix_slice] = dilation(a_slice)

    if p_out:
        with warnings.catch_warnings():
            p_out.parent.mkdir(parents=True, exist_ok=True)
            imsave_fiji(p_out, a_membrane.astype('uint16'))

    return a_membrane


def _get_voronoi_points(a_points):
    ''' get voronoi points, together with their circumcenters and circumradii
      alternative version, since meshplex has turned commercial'''
    deln = Delaunay(a_points)
    a_points_simp = a_points[deln.simplices]
    a_cc = _calc_circumcenter_circumsphere_triangle_vectorized(a_points_simp.astype(float))
    a_cr = np.linalg.norm((a_cc - (a_points_simp[:, 0, :])), axis=1)

    return a_cc.astype(int), a_cr.astype(float), deln

# def _get_voronoi_points(a_points):
#     """ get voronoi points, together with their circumcenters
#     and circumradii"""
#     deln = Delaunay(a_points)
#     mesh = meshplex.Mesh(a_points.astype(float), deln.simplices.astype(int))
#     return (np.round(np.array(mesh.cell_circumcenters)).astype(int),
#             np.array(mesh.cell_circumradius).astype(float), deln)


def _test_points_inside_convex_hull(a_check, a_points_hull):
    """ a_points_hull : the points that will be used to construct the
    convex hull, i.e the membrane pixels
        a_check : the points the will be checked to see if they fall
        inside the convex hull i.e the circumcenters"""

    hull = ConvexHull(a_points_hull)
    poly_path = mplPath.Path(a_points_hull[hull.vertices])
    a_selector_inside_hull = poly_path.contains_points(a_check, radius=0.01)

    return a_selector_inside_hull, hull


def _get_boundary_edges(a_simplices):
    """ input NX3 array indicating the simplices (triangles),
    output Mx2 array indicating the edges on the outside border of the mesh"""
    a_edges = np.vstack([np.transpose(np.transpose(a_simplices)[[0, 1], :]),
                         np.transpose(np.transpose(a_simplices)[[1, 2], :]),
                         np.transpose(np.transpose(a_simplices)[[2, 0], :])])
    a_edges.sort(axis=1)  # orientation not important
    v, c = np.unique(a_edges, axis=0, return_counts=True)
    a_boundary_edges = v[np.where(c == 1)]  # boundary edges are unique

    return a_boundary_edges


def _get_polygon_loop(a_boundary_edges, verbose=False):
    def prepare_vertices_maps():
        d_start_end_1 = {}
        d_start_end_2 = {}
        for p1, p2 in a_boundary_edges:  # always 2 entries per node,
            # to store both directions because we don't no which
            # way the node will be approached when building the loop

            if p1 not in d_start_end_1:
                d_start_end_1[p1] = p2
            else:
                d_start_end_2[p1] = p2

            if p2 not in d_start_end_1:
                d_start_end_1[p2] = p1
            else:
                d_start_end_2[p2] = p1

        return d_start_end_1, d_start_end_2

    def tie_together_polygon(current_node, current_loop):
        if current_loop and (
                current_node is None or current_node == current_loop[0]):
            return current_loop
        else:
            current_loop.append(current_node)
            next_node = d_start_end_1.get(current_node)
            if not next_node:
                next_node = d_start_end_2.get(current_node)

            if next_node:
                try:
                    if d_start_end_1[next_node] == current_node:
                        del d_start_end_1[next_node]  # burn reverse link
                    else:
                        del d_start_end_2[next_node]  # burn reverse link
                except Exception as e:
                    return current_loop

            tie_together_polygon(current_node=next_node,
                                 current_loop=current_loop)  # recursion

        return current_loop

    d_start_end_1, d_start_end_2 = prepare_vertices_maps()
    start_node = a_boundary_edges[0, 0]
    l_polygon_loop = tie_together_polygon(
        current_node=start_node, current_loop=[])

    return l_polygon_loop


def _downsample_membrane_points(a_img, verbose=False):
    downsampling_factor = 10

    if a_img.ndim == 2:
        a_points = np.asarray(np.where(a_img > 0))
        # present as 3D pointcloud
        a_points = np.transpose(
            np.vstack([np.zeros((a_points.shape[1])), a_points]))
    else:
        a_points = np.argwhere(a_img)

    if verbose:
        print(f"The raw img has {np.sum(a_img>0)} \
            membrane pixels. shape = {a_img.shape} ")

    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(a_points)

    downpcd = pcd.voxel_down_sample(voxel_size=downsampling_factor)
    if verbose:
        print(f"The downsampled slice ({downsampling_factor} times) \
            has {np.asarray(downpcd.points).shape[0]} membrane pixels. \
             A reduction  by a factor of \
             {np.sum(a_img>0)/np.asarray(downpcd.points).shape[0]}")

    a_points = np.asarray(downpcd.points)

    if a_img.ndim == 2:
        return a_points[:, [1, 2]]
    else:
        return a_points


def get_exterior_mask(a_stack, p_out=None, threeD=False, verbose=False):
    '''
    This filter will build a non-convex exterior mask based on the
    alpha-complex approach. After Delaunay triangulation, exterior
    triangles are filtered out by checking if their circumcenter falls
    inside the convex hull. The outline of the interior triangles is
    then retrieved and a mask is created This approach can be an
    alternative for the active contour""
    '''

    a_exterior_mask = np.zeros(a_stack.shape, dtype='uint16')

    prev_slice = None

    with warnings.catch_warnings():
        warnings.simplefilter("ignore")

        if not threeD:

            for ix_slice, a_slice in enumerate(a_stack):
                if not a_slice.any():
                    # a_exterior_mask[ix_slice] = a_slice
                    continue
                try:
                    a_points = _downsample_membrane_points(a_slice)
                    a_circumcenters, a_circumradii, tri = _get_voronoi_points(
                        a_points)
                    a_selector_inside_hull, hull = _test_points_inside_convex_hull(
                        a_check=a_circumcenters, a_points_hull=a_points)
                    a_non_convex_hull_simplices = \
                        tri.simplices[a_selector_inside_hull]
                    a_boundary_edges = _get_boundary_edges(
                        a_non_convex_hull_simplices)
                    l_polygon_loop = _get_polygon_loop(
                        a_boundary_edges,
                        verbose=False)
                    # orders matters, has to be sequential outline of a polygon
                    a_exterior_mask[ix_slice] = polygon2mask(
                        a_slice.shape, a_points[l_polygon_loop])
                    prev_slice = a_exterior_mask[ix_slice]
                except Exception as e:
                    # traceback.print_exc()
                    if prev_slice is not None and prev_slice.any():
                        if verbose:
                            print(f"{ix_slice}<", end="")
                        a_exterior_mask[ix_slice] = prev_slice
                    else:
                        if verbose:
                            print(f"{ix_slice}x", end="")
                        a_exterior_mask[ix_slice] = a_slice
        else:
            print('Constructing exterior mask in 3D...')
            a_points = _downsample_membrane_points(a_stack)
            a_circumcenters, a_circumradii, deln = _get_voronoi_points(
                a_points)
            a_selector_inside_hull = deln.find_simplex(a_circumcenters) > -1
            tets_inside = np.where(a_selector_inside_hull)[0]

            idx_2d = np.indices(a_stack.shape[1:], np.int16)
            idx_2d = np.moveaxis(idx_2d, 0, -1)

            idx_3d = np.zeros((*a_stack.shape[1:], a_stack.ndim), np.int16)
            idx_3d[:, :, 1:] = idx_2d

            for z in range(len(a_stack)):
                idx_3d[:, :, 0] = z
                s = deln.find_simplex(idx_3d)
                # a_exterior_mask[z, (s != -1)] = 1 # for convex hull
                # for non-convex hull
                a_exterior_mask[z, np.isin(s, tets_inside)] = 1

    if p_out:
        with warnings.catch_warnings():
            p_out.parent.mkdir(parents=True, exist_ok=True)
            imsave_fiji(p_out, a_exterior_mask.astype('uint8'))

    return a_exterior_mask


def filter_blend_z_membrane(a_membrane, a_raw, a_threshold,
                            a_exterior_mask, kwargs,
                            df_thresholds, p_out=None):
    """
    a_membrane = the binary membrane stack
    a_raw = the image stack (raw)
    a_threshold= the thresholded stack (=the otsu cutoff)
    a_exterior_mask = the exterior mask
    """
    a_peaks_flooded, d_log = locate_z_membrane(
        a_raw=a_raw,
        a_membrane_mask=np.where(a_threshold, 0, 1),
        a_exterior_mask=a_exterior_mask,
        df_thresholds=df_thresholds,
        **kwargs)

    # z-membrane gets '3' as a value, just for ease of indentification
    a_membrane_blend_z = np.where(a_peaks_flooded, 3, a_membrane)
    # keep empty slices empty
    a_membrane_blend_z[np.sum(a_membrane, axis=(1, 2)) == 0] = 0

    if p_out:
        with warnings.catch_warnings():
            p_out.parent.mkdir(parents=True, exist_ok=True)
            imsave_fiji(p_out, a_membrane_blend_z.astype('uint16'))

    return a_membrane_blend_z, d_log


def smooth_exterior_mask(a_exterior_mask, p_out=None, weight_center=3,
                         weight_top_bottom=2, weight_left_right=1):
    """ smooths an exterior mask (entire time lapse),
    by default it will smooth over time"""

    # print("...Smoothing the exterior mask...", flush=True)
    a_ext_mask_smoothed = np.zeros_like(a_exterior_mask)

    single_stack = True if len(a_exterior_mask.shape) == 3 else False

    if not single_stack:
        t_dim, z_dim, _, _ = a_exterior_mask.shape
    else:
        z_dim, _, _ = a_exterior_mask.shape
        t_dim = 1

    for t in range(t_dim):
        for z in range(z_dim):
            if single_stack:
                a_slice = a_exterior_mask[z, :]
            else:
                a_slice = a_exterior_mask[t, z, :]
            z_plus = 1 if z < z_dim - 1 else 0
            t_plus = 1 if t < t_dim - 1 else 0
            z_min = 1 if z > 0 else 0
            t_min = 1 if t > 0 else 0

            if single_stack:
                a_ext_mask_smoothed[z, :] \
                    = a_ext_mask_smoothed[z, :] + \
                    (a_slice * weight_center)
                a_ext_mask_smoothed[z - z_min, :] \
                    = a_ext_mask_smoothed[z - z_min, :] + \
                    (a_slice * weight_top_bottom)
                a_ext_mask_smoothed[z + z_plus, :] \
                    = a_ext_mask_smoothed[z + z_plus, :] +\
                    (a_slice * weight_top_bottom)
            else:
                a_ext_mask_smoothed[t, z, :] \
                    = a_ext_mask_smoothed[t, z, :] +\
                    (a_slice * weight_center)
                a_ext_mask_smoothed[t, z - z_min, :] \
                    = a_ext_mask_smoothed[t, z - z_min, :] +\
                    (a_slice * weight_top_bottom)
                a_ext_mask_smoothed[t, z + z_plus, :] \
                    = a_ext_mask_smoothed[t, z + z_plus, :] +\
                    (a_slice * weight_top_bottom)
                a_ext_mask_smoothed[t - t_min, z, :] \
                    = a_ext_mask_smoothed[t - t_min, z, :] +\
                    (a_slice * weight_left_right)
                a_ext_mask_smoothed[t + t_plus, z, :] \
                    = a_ext_mask_smoothed[t + t_plus, z, :] +\
                    (a_slice * weight_left_right)

    if single_stack:
        WEIGHT_THRESHOLD = (weight_center + 2 * (weight_top_bottom)) / 1.5
    else:
        WEIGHT_THRESHOLD = (
            weight_center + 2 * (weight_top_bottom)
            + 2 * (weight_left_right)) / 1.5

    a_ext_mask_smoothed_binary = a_ext_mask_smoothed > WEIGHT_THRESHOLD

    if p_out:
        with warnings.catch_warnings():
            p_out.parent.mkdir(parents=True, exist_ok=True)
            imsave_fiji(p_out, a_ext_mask_smoothed_binary.astype('uint8'))

    return a_ext_mask_smoothed_binary
