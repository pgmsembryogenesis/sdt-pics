"""
Created on 29 may 2020
extract geometry data from an embryo
@author: wimth
"""
import numpy as np
import pandas as pd
import sys
import os
import re
import shutil
from param_XML import Param_xml
from geom_classes import Cell

verbose = True


def read_parms():
    param_xml = Param_xml.get_param_xml(
        sys.argv,
        l_main_keys=[
            'body',
            'extract_geom_data'],
        verbose=True)  # param file must be passed as first argument

    input_folder = param_xml.get_value('input_folder', ['paths'])
    output_folder = param_xml.get_value('output_folder', ['paths'])
    input_file_vtp = param_xml.get_value('input_file_vtp', ['paths'])

    return input_folder, output_folder, input_file_vtp


def _extract_cells(input_file_vtp, output_folder, input_folder):
    from VTK_utils import read_vtp_file, extract_cells

    poly_embryo = read_vtp_file(input_file_vtp)
    meshes_folder = output_folder / "cells"
    meshes_folder.mkdir(parents=True, exist_ok=True)
    extract_cells(
        poly_embryo,
        meshes_folder,
        write_stl_files=True,
        verbose=True)

    return meshes_folder


def _init_ds_cells(input_file_vtp, input_folder):
    if input_folder is not None:
        print(input_folder)
        for ix, stl_i in enumerate(sorted(input_folder.glob('*.stl'))):
            if verbose:
                print(
                    'Extracting geom data from stl ->  {0}'.format(stl_i.name))
            Cell(stl_i, ic_GT=False, cm=None)
    else:
        from VTK_utils import read_vtp_file, extract_cells, poly_to_trimesh
        poly_embryo = read_vtp_file(input_file_vtp)
        d_cellName_poly = extract_cells(
            poly_embryo, output_folder=None, verbose=True)
        for cell_name_i, poly_i in d_cellName_poly.items():
            Cell(
                p_cell=None,
                ic_GT=False,
                cm=None,
                tmh=poly_to_trimesh(
                    poly_i,
                    scale=1.e6),
                cell_name=cell_name_i,
                f_type='tmh')

    return


def write_excel_vol_data():
    d_df = {}
    for cell_i in Cell.l_RES:
        d_df.setdefault('cell_name', []).append(cell_i.name)
        d_df.setdefault('volume', []).append(cell_i.volume)
        d_df.setdefault('surface_area', []).append(cell_i.surface_area)
        d_df.setdefault('sphericity', []).append(cell_i.sphericity)
        d_df.setdefault('scale', []).append(cell_i.scale)
        l, w, h = cell_i.extents
        d_df.setdefault('extents_length', []).append(l)
        d_df.setdefault('extents_width', []).append(w)
        d_df.setdefault('extents_height', []).append(h)
        x, y, z = cell_i.centroid
        d_df.setdefault('centroid_x', []).append(x)
        d_df.setdefault('centroid_y', []).append(y)
        d_df.setdefault('centroid_z', []).append(z)
        x, y, z = cell_i.center_mass
        d_df.setdefault('center_mass_x', []).append(x)
        d_df.setdefault('center_mass_y', []).append(y)
        d_df.setdefault('center_mass_z', []).append(z)
        x, y, z = cell_i.principal_inertia_components
        d_df.setdefault('principal_inertia_x', []).append(x)
        d_df.setdefault('principal_inertia_y', []).append(y)
        d_df.setdefault('principal_inertia_z', []).append(z)

    df = pd.DataFrame.from_dict(d_df)
    df.to_csv(str(output_folder / "geom_info.csv"), index=False, mode='w')

    if verbose:
        print("excel with cell volumes written (geom_info.csv)")

    return


if __name__ == '__main__':
    # Read parms
    input_folder, output_folder, input_file_vtp = read_parms()
    output_folder.mkdir(parents=True, exist_ok=True)

    clean_up = False
    extract_meshes_to_disk = False
    if input_file_vtp is not None and os.path.exists(input_file_vtp):
        if extract_meshes_to_disk:
            input_folder = _extract_cells(
                input_file_vtp, output_folder, input_folder)
            clean_up = True
            _init_ds_cells(input_file_vtp=None, input_folder=input_folder)
        else:
            _init_ds_cells(input_file_vtp, input_folder=None)
    else:
        _init_ds_cells(input_file_vtp=None, input_folder=input_folder)

    write_excel_vol_data()

    if clean_up:
        shutil.rmtree(input_folder)
