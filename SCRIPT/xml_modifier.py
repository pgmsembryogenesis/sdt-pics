"""modifies a XML based on excel input (parameterised).

Created on 17 Aug 2019.
@author: wimth
run in bash

"""
import sys
import os
import shutil
import re
import xml.etree.ElementTree as ET
from pathlib import Path
import numpy as np

import openpyxl as xl
import pandas as pd
import datetime
from param_XML import Param_xml
import traceback


def _parse_args():
    d_args = {}
    d_args['input_state_mod'] = sys.argv[1] if len(
        sys.argv) > 1 else 'testinfra.xlsx'
    d_args['output_xml_folder'] = Path(
        sys.argv[2]) if len(sys.argv) > 2 else Path("./XML")
    d_args['output_xml'] = d_args['output_xml_folder'] / \
        "testinfra_{0}.xml".format(str(sys.argv[3]))
    d_args['xml_input'] = Path(sys.argv[4]) if len(
        sys.argv) > 4 else Path("./PARAMS.xml")
    d_args['data_folder'] = Path(sys.argv[5]) if len(
        sys.argv) > 5 else Path("./testinfra")
    d_args['script_folder'] = Path(
        sys.argv[6]) if len(sys.argv) > 6 else Path(".")
    d_args['param_folder'] = Path(sys.argv[7]) if len(
        sys.argv) > 7 else d_args['script_folder']
    d_args['restart_module'] = int(sys.argv[8]) if len(sys.argv) > 8 else 0

    return d_args


def _pick_state_file(d_config, d_args):
    if d_config.get('xml_input'):
        state_file = d_config['xml_input']
    else:
        state_file = d_args['xml_input']

    # print(f"*** state file = {state_file} ***")
    modify_state = True
    if d_config:
        # print(f"*** state modifier = {d_args['input_state_mod']} ***")
        if 'xml_input' in d_config:
            modify_state = True
    else:
        print(f"*** No state modifier used. ({d_args['input_state_mod']}"
              f" is not found) ***")
        modify_state = False

    if not modify_state:
        print(f"*** State from input is used, without modifications")

    return state_file, modify_state


def _read_excel(d_args):

    def load_testcase_info():
        try:
            converters = {
                'suffix': str,
                'offset': str,
                'replicate': str,
                'time': str,
                'nb_stack_raw': str,
                't_stack_mpacts': str,
                'TRA_run': str,
                'nb_stack_input': int,
                'ix_stack_input': str}
            df_tc_info = pd.read_excel(
                d_args['input_state_mod'],
                sheet_name='tc_info',
                converters=converters).fillna('')
            sel = df_tc_info['testcase'] == d_config['NB_RUN']
            if "replicate" in df_tc_info:
                try:
                    key = ['replicate']
                    if "TRA_run" in df_tc_info:
                        key.append('TRA_run')
                    converters = {
                        'replicate': str,
                        'TRA_run': str,
                        'nb_cells_first_stack': int,
                        'build_exterior_mask_3D': str,
                        'shrink_spheres': str,
                        'label_passing': bool,
                        'pad_seeds_bottom_stack': bool}
                    df_tc_info = df_tc_info.merge(
                        pd.read_excel(
                            d_args['input_state_mod'],
                            sheet_name='replicate',
                            converters=converters).fillna(''),
                        how='left',
                        on=key)
                except Exception as e:
                    pass
            if "tc_category" in df_tc_info:
                try:
                    df_tc_info = df_tc_info.merge(pd.read_excel(
                        d_args['input_state_mod'], sheet_name='tc_category'), how='left', on='tc_category')
                except Exception as e:
                    pass
            # if "TRA_run" in df_tc_info:
            #     try:
            #         df_tc_info = df_tc_info.merge(pd.read_excel(d_args['input_state_mod'], sheet_name='TRA_run', converters={
            #                                       'replicate': str, 'offset_input_img': str, 'offset_exterior_mask': str}), how='left', on=['replicate', 'TRA_run'])
                except Exception as e:
                    pass

            df_tc_info = df_tc_info.loc[np.where(sel)].reset_index()

        except Exception as e:
            # traceback.print_exc()
            df_tc_info = pd.DataFrame({})

        return df_tc_info

    wb = xl.load_workbook(filename=d_args['input_state_mod'])

    d_config = {}
    if len(sys.argv) > 3:
        d_config['NB_RUN'] = str(sys.argv[3])
    else:
        print('Run number (=testcaseID) not given in input. Exitting')
        exit(-1)

    df_tc_info = load_testcase_info()

    try:
        ws_data = wb["{0}".format(d_config['NB_RUN'])]
    except Exception as e:
        ws_data = None

    if not ws_data:
        try:
            d_config['NB_RUN'] = df_tc_info['use_tab'][0]
            ws_data = wb[d_config['NB_RUN']]
        except Exception as e:
            print(f'Loading parameter data from excel failed : error={e}')
            exit(-1)

# extract dynamic variables (=variables derived from fields on the
# testcase tab) and
    d_config['output_folder'] = None
    d_config['init_folder'] = None
    d_config['sub_folder'] = ""
    d_config['switch_tab'] = []
    d_config['xml_transfer'], d_config['xml_transfer_scheme'] = "", ""

    for xrow in ws_data.iter_rows(
            min_row=1, min_col=1, max_row=ws_data.max_row, max_col=ws_data.max_column):

        try:
            if xrow[0].value.endswith('root_output_folder'):
                d_config['output_folder'] = Path(
                    fill_in_variables(xrow[1].value, df_tc_info, d_args))

            if xrow[0].value.endswith('root_output_subfolder'):
                d_config['sub_folder'] = fill_in_variables(
                    xrow[1].value, df_tc_info, d_args)

            if xrow[0].value.endswith('<INIT_FOLDER>'):
                if xrow[1].value:
                    d_config['init_folder'] = Path(
                        fill_in_variables(xrow[1].value, df_tc_info, d_args))

            if xrow[0].value == '<SWITCH_TAB>':
                if xrow[1].value:
                    if xrow[1].value.endswith('#'):
                        d_config['switch_tab'].append(xrow[1].value)
                    else:
                        d_config['switch_tab'] = fill_in_variables(
                            xrow[1].value, df_tc_info, d_args).split(";")

            if xrow[0].value == '<XML_TRANSFER>':
                d_config['xml_transfer'] = xrow[1].value
            if xrow[0].value == '<XML_TRANSFER_SCHEME>':
                d_config['xml_transfer_scheme'] = xrow[1].value

            if xrow[0].value == '<XML_INPUT>':
                d_config['xml_input'] = Path(xrow[1].value)

        except Exception as e:
            traceback.print_exc()

    d_config['switch_tab'].append(d_config['NB_RUN'])

    return wb, d_config, df_tc_info


def substitute_hashtags(input_value, df_tc_info):
    s_new_value = str(input_value)
    for substitution in re.findall(r"#(.+?)#", str(input_value)):
        s_new_value = s_new_value.replace(
            f"#{substitution}#", str(
                df_tc_info[substitution][0]))
        if 'offset' in substitution:  # offset should be last term and appear only once in the expression
            s_new_value = str(eval(s_new_value))

    if '#' in s_new_value:  # recursion  for nested substitutions
        s_new_value = substitute_hashtags(s_new_value, df_tc_info)

    return s_new_value


def fill_in_variables(xrow_value, df_tc_info, d_args):
    """
    $ variables are defined in other fields of the tab (=dynamic parameters),
    used for path resolving
    # variables are listed in the tc_info tab and can also be interpreted
    as expressions
    """

    s_new_value = substitute_hashtags(xrow_value, df_tc_info)
    if "$DATA_FOLDER" in s_new_value:
        s_new_value = s_new_value.replace(
            "$DATA_FOLDER", str(d_args['data_folder']))

    if "$SCRIPT_FOLDER" in s_new_value:
        s_new_value = s_new_value.replace(
            "$SCRIPT_FOLDER", str(d_args['script_folder']))

    return s_new_value


def modify_based_on_excel_tab(ws_data, d_config, df_tc_info):
    ''' modify based on excel
    this will set @value (if there is an text entry it will create
    a orderedDict
    eg OrderedDict([('@value','TL11_emb1of1_t1.tif'),('#text', 'file1')])),
    tip : break on a row via e.g. xrow[0].row == 46
    '''
    for xrow in ws_data.iter_rows(
            min_row=1, min_col=1, max_row=ws_data.max_row, max_col=ws_data.max_column):
        try:
            if xrow[0].value.strip() in ['<INIT_FOLDER>', '<SWITCH_TAB>',
                                         '<XML_TRANSFER>', '<XML_TRANSFER_SCHEME>', '<XML_INPUT>']:
                continue
            # gives back a list in theory, we only use first match
            xml_element = root.findall(xrow[0].value.strip())[0]

            s_new_value = fill_in_variables(xrow[1].value, df_tc_info, d_args)

            if s_new_value == 'blanc':
                xml_element.set('value', '')
            else:
                xml_element.set('value', s_new_value)

            if ("ix_restart" in xrow[0].value):
                if s_new_value not in ["", "0",
                                       0] and d_config.get('init_folder'):
                    d_config['flag_cancel_init_folder'] = True

            if (('testcase' in df_tc_info) and ('TRA' in df_tc_info.testcase[0]) and (
                    "annotated_membrane_file" in xrow[0].value) and (s_new_value not in ["", "blanc"])):
                d_config['flag_cancel_init_folder'] = True
                print(
                    'TRA run using annotated_membrane_file sets flag_cancel_init_folder to false !')

            if verbose:
                print(xrow[0].value.strip(), "=>", xml_element.get('value'))
        except Exception as e:
            if verbose:
                print("{0} : xml value not found".format(xrow[0]))

    return


def _init_folders(d_config):
    if d_config.get('flag_cancel_init_folder', False):
        d_config.pop('init_folder', None)
        print('*** output directory not initialized ***')

    if 'init_folder' in d_config:
        if d_config.get('init_folder') == "":
            if d_config['output_folder']:
                if d_config['sub_folder']:
                    d_config['init_folder'] = d_config['output_folder'] / \
                        d_config['sub_folder']
                else:
                    d_config['init_folder'] = d_config['output_folder']

        if d_config['init_folder']:
            if d_args['restart_module'] > 1:
                print(
                    "*** not deleting init-folder because resuming from module {0} ***".format(d_args['restart_module']))
            else:
                if d_config['init_folder'].exists():
                    if 'OUTPUT' not in d_config['init_folder'].parts or d_config['init_folder'].parts[-1] == 'OUTPUT':
                        print('*** init_folder does not have OUTPUT in the pathname (or OUTPUT is the root folder).'
                              'Are you really sure you want to remove this folder ? if so, do it manually ***')
                else:
                    shutil.rmtree(
                        str(d_config['init_folder']), ignore_errors=True)
                    if verbose:
                        print("***", str(d_config['init_folder']),
                              " is removed by xml-modifier ***")
    else:
        print('*** Folders are not intialized (no init folder given) ***')
    return


def _store_RAM_data(root, d_args, d_config):
    xml_element = root.findall('./RAM/timestamp')[0]
    xml_element.set('value', str(datetime.datetime.now()).
                    translate({ord(c): None for c in "._-!: "}))
    xml_element = root.findall('./RAM/paths/root_output_folder')[0]
    xml_element.set('value', str(d_config['output_folder']))
    xml_element = root.findall('./RAM/paths/script_folder')[0]
    # Note: if xml-modifier is run under docker, then the default
    # script folder will be docker (only use docker_ic to force docker
    # script folder)
    xml_element.set('value', str(d_args['script_folder']))
    xml_element = root.findall('./RAM/paths/data_folder')[0]
    xml_element.set('value', str(d_args['data_folder']))
    xml_element = root.findall('./RAM/paths/data_folder')[0]
    xml_element.set('value', str(d_args['data_folder']))
    xml_element = root.findall('./RAM/paths/param_xml_base')[0]
    xml_element.set('value', str(d_config['xml_input'] if 'xml_input' in
                                 d_config else str(d_args['xml_input'])))
    xml_element = root.findall('./RAM/paths/param_xml_mod')[0]
    xml_element.set('value', str(d_args['output_xml']))
    xml_element = root.findall('./RAM/paths/state_mod_file')[0]
    xml_element.set('value', str(d_args['input_state_mod']))

    return


def _copy_dynamic_XML(root, d_args):
    '''copy dynamic XML fields from previous resolved XML-file
    in case of restart.  Update if dynamic xml-value is added'''
    print(f"Copying Dynamic XML parms")
    l_dyn_keys = ['./RAM/paths/img_raw_file_rescaled',
                  './RAM/RAW_IMG_DIM_TZYX_rescaled', './RAM/RAW_IMG_DIM_TZYX',
                  './RAM/scaling_ZYX_rescaled',
                  './RAM/timestamp_mesh']
    tree2 = ET.parse(d_args['output_xml'])
    root2 = tree2.getroot()

    for dyn_key_i in l_dyn_keys:
        xml_element = root.findall(dyn_key_i)[0]
        xml_element2 = root2.findall(dyn_key_i)
        if xml_element2:
            xml_element2 = xml_element2[0]
        else:
            continue
        dyn_value = xml_element2.get('value')
        xml_element.set('value', dyn_value)
    return


def _transfer_values_from_other_tc(d_config, d_args, df_tc_info, root):
    '''parms can be copied from a another testcase
    (different schemes can be definied ad-hoc) '''
    d_config['xml_transfer'] = substitute_hashtags(
        d_config['xml_transfer'], df_tc_info)
    d_config['xml_transfer_scheme'] = substitute_hashtags(
        d_config['xml_transfer_scheme'], df_tc_info)
    p_xml_transfer = d_args['output_xml_folder'] / \
        "testinfra_{0}.xml".format(d_config['xml_transfer'])

    if d_config['xml_transfer_scheme'] == 'TRA':
        try:
            param_xml_transfer = Param_xml.get_param_xml(
                ['', p_xml_transfer], l_main_keys=['body'], verbose=False)
            xml_element = root.findall('./RAM/paths/img_raw_file_rescaled')[0]
            xml_element.set('value', str(param_xml_transfer.get_value(
                'img_raw_file_rescaled', ['RAM', 'paths'])))

            xml_element = root.findall('./RAM/RAW_IMG_DIM_TZYX')[0]
            value = param_xml_transfer.get_value('RAW_IMG_DIM_TZYX', ['RAM'])
            if value:
                xml_element.set('value', ';'.join([str(i) for i in value]))

            xml_element = root.findall('./RAM/RAW_IMG_DIM_TZYX_rescaled')[0]
            value = param_xml_transfer.get_value(
                'RAW_IMG_DIM_TZYX_rescaled', ['RAM'])
            if value:
                xml_element.set('value', ';'.join([str(i) for i in value]))

            xml_element = root.findall(
                './preprocessing/paths/output_folder')[0]
            xml_element.set('value', str(param_xml_transfer.get_value(
                'output_folder', ['preprocessing', 'paths'])))

            xml_element = root.findall('./spheresDT/paths/output_folder')[0]
            xml_element.set('value', str(param_xml_transfer.get_value(
                'output_folder', ['spheresDT', 'paths'])))

            xml_element = root.findall(
                './lineager_feeder/paths/output_folder')[0]
            xml_element.set('value', str(param_xml_transfer.get_value(
                'output_folder', ['lineager_feeder', 'paths'])))

            print(
                f"*** Parms copied from testcase {d_config['xml_transfer']} using scheme {d_config['xml_transfer_scheme']} ***")

        except Exception as e:
            print(
                f"Transfer xml file can not be found or transfer keys are not in this file :{str(d_config['xml_transfer'])}. Continuing without transfer...")

    return


def _create_folders(d_config):
    if not d_config['output_folder']:
        d_config['output_folder'] = d_config['init_folder']
        if not d_config['output_folder']:
            print(
                f"*** WARNING : output_folder nor init folder is defined.  add to excel and restart ***")
    if d_config['sub_folder']:
        (d_config['output_folder'] / d_config['sub_folder'] /
         '_LOG' / 'modules_snapshot').mkdir(parents=True, exist_ok=True)
        # print("***", (d_config['output_folder'] / d_config['sub_folder'] /
        #               '_LOG' / 'modules_snapshot'), " has been created ***")
    else:
        (d_config['output_folder'] / '_LOG' /
         'modules_snapshot').mkdir(parents=True, exist_ok=True)
        # print("***", str(d_config['output_folder'] / '_LOG' /
        #                  'modules_snapshot'), " has been created ***")
    return


def _store_param_xml(d_args, tree):

    if d_args['output_xml'].exists():
        os.remove(d_args['output_xml'])
    d_args['output_xml_folder'].mkdir(parents=True, exist_ok=True)
    tree.write(str(d_args['output_xml']))
    # print('*** output_xml is written :', d_args['output_xml'], ' ***')

    return


def _pre_resolve_variables(d_args, root, tree):
    '''pre resolve variables ad-hoc (the param file needs some variables
    to be fully resolved to allow for extraction by a simple grep or
    text search outside a python context)'''
    param_xml = Param_xml.get_param_xml(
        ['', d_args['output_xml']], l_main_keys=['body'], verbose=False)
    param_xml.add_prms_to_dict(l_keys=['sphere_meshing'])
    for key, item in param_xml.prm.items():
        item = str(item)
        if key in ['shrink_factor', 'threshold_radius_micron',
                   'nb_nodes_cell', 'surface_to_nodes_factor']:
            search_key = './sphere_meshing/parms/' + key
        else:
            search_key = './sphere_meshing/paths/' + key
        xml_element = root.findall(search_key)[0]
        if xml_element:
            xml_element.set('value', item)

    tree.write(str(d_args['output_xml']))  # update the xml-file
    return


def _copy_parms_prev(d_args, root):

    if not d_args['output_xml'].exists():
        return False

    if os.path.getsize(d_args['output_xml']) == 0:
        return False

    try:
        testTree = ET.parse(d_args['output_xml'])
    except BaseException:
        print(
            f"Copying previous parms from {d_args['output_xml']} was not possible, because the file could not be loaded and parsed")
        return False

    if (d_args['restart_module'] > 1):
        return True

    xml_element = root.findall(
        './preprocessing/paths/annotated_membrane_file')[0]
    if xml_element.get('value', ''):
        return True

    return False


if __name__ == '__main__':
    verbose = False
    wb, d_config, df_tc_info = {}, {}, {}

    d_args = _parse_args()

    if os.path.exists(d_args['input_state_mod']):
        wb, d_config, df_tc_info = _read_excel(d_args)

    state_file, modify_state = _pick_state_file(d_config, d_args)

    tree = ET.parse(str(state_file))
    root = tree.getroot()
    if modify_state:
        for tab_i in d_config['switch_tab']:
            if verbose:
                print(">>modifying PARAM state with tab={0} from {1}<<"
                      .format(tab_i, d_args['input_state_mod']))
            ws_data = wb["{0}".format(tab_i)]
            modify_based_on_excel_tab(ws_data, d_config, df_tc_info)

        _store_RAM_data(root, d_args, d_config)

        if _copy_parms_prev(d_args, root):
            _copy_dynamic_XML(root, d_args)

        if d_config['xml_transfer']:
            _transfer_values_from_other_tc(d_config, d_args, df_tc_info, root)

    _init_folders(d_config)
    _create_folders(d_config)
    _store_param_xml(d_args, tree)
    _pre_resolve_variables(d_args, root, tree)
