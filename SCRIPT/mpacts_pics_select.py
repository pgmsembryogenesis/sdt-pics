'''
Created on 17jan 2021
selects the embryo from mpacts PiCS output and extracts the cells (in vtp and stl format).  stl's are scaled
This selection will be used further on (for SEG validation, geometry extraction etc)

@author: wimth
'''
from VTK_utils import write_vtp_file, read_vtp_file, extract_cells
import os
from param_XML import Param_xml
import sys


def read_parms():
    param_xml = Param_xml.get_param_xml(
        sys.argv, l_main_keys=['body'], verbose=True)
    param_xml.add_prms_to_dict(l_keys=['mpacts_pics_select'])
    prm = param_xml.prm
    prm['timestamp'] = param_xml.get_value('timestamp', l_path_keys=['RAM'])
    return prm


if __name__ == '__main__':
    prm = read_parms()
    prm['output_folder'].mkdir(parents=True, exist_ok=True)
    for f in list(prm['output_folder'].glob('*')):  # clear outputfolder
        os.remove(str(f))

    # pick a VTP to extract cells
    input_file_vtp = ""
    if prm['input_file']:
        input_file_vtp = prm['input_file']
    else:
        input_file_vtp = [
            vtp for vtp in prm['input_folder'].glob('*enriched.vtp')]
        if input_file_vtp:
            input_file_vtp = input_file_vtp[0]

    print(
        'vtp file {0} will be used for extraction cells'.format(
            input_file_vtp),
        flush=True) if input_file_vtp else print(
        'no vtp file found !',
        flush=True)
    poly_in = read_vtp_file(input_file_vtp)

    # write VTP file per cell
    if prm['extract_cells']:
        extract_cells(poly_in, prm['output_folder'], prm['write_stl_files'])

    # write vtp embryo
    print(f"Start writing selected_embryo to output...", flush=True)
    write_vtp_file(poly_in, str(prm['output_folder'] / "selected_embryo.vtp"))
    print(f"selected_embryo.vtp written to output", flush=True)

    with open(prm['output_folder'] / f"ref_{prm['timestamp']}", 'w') as f:
        f.write("")

    if prm['clean_up']:
        print(f"cleaning up input folder {prm['input_folder']}")
        l_regex_remove = ["Seeding_*[0-9].vtp", "Seeding_*.mpactsd"]
        for regex_remove_i in l_regex_remove:
            for file_i in prm['input_folder'].rglob(regex_remove_i):
                os.remove(file_i)
                # print(f"x{file_i}", end="")
    print('')
