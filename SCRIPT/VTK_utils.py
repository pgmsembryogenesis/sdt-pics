'''
Created on 30 nov 2019 
VTK utilities
        
@author: wimth
'''
import vtk
import vtk.numpy_interface.dataset_adapter as dsa
import vtk.numpy_interface.algorithms as algos
import numpy as np
import os
import re
import pyvista as pv
import trimesh
import pprint
from pathlib import Path
import shutil
from itertools import permutations, combinations
pp = pprint.PrettyPrinter(indent=4)
import pandas as pd
from pymeshfix import _meshfix
import pymeshfix
# from joblib import Parallel, delayed
from trimesh.smoothing import *
import traceback
from pyacvd import clustering
import pyvista as pv
from scipy.spatial import KDTree

def read_vtp_file(vtp_file):

    vtp_reader = vtk.vtkXMLPolyDataReader()
    vtp_reader.SetFileName(str(vtp_file))
    vtp_reader.Update()

    return vtp_reader.GetOutput()  # vtkPolyData


def copy_poly_minimal(poly_in, scale=1):
    """ a copy of poly_in only keeping the minimal data (needed for mesh construction, STL generation etc), With optional scaling"""
    wdo_in = dsa.WrapDataObject(poly_in)
    # we need a new poly, because updating values through dsa API not possible
    wdo_out = dsa.WrapDataObject(vtk.vtkPolyData())

    wdo_out.Points = wdo_in.Points * scale

    wdo_out.GetAttributes(vtk.vtkDataObject.POINT).append(
        wdo_in.PointData['x'] * scale, 'x')
    wdo_out.GetAttributes(vtk.vtkDataObject.POINT).append(
        wdo_in.PointData['localIndex'], 'localIndex')
    wdo_out.GetAttributes(vtk.vtkDataObject.POINT).append(
        wdo_in.PointData['normal'], 'normal')

    wdo_out.GetAttributes(vtk.vtkDataObject.CELL).append(
        wdo_in.CellData['x'] * scale, 'x')
    wdo_out.GetAttributes(vtk.vtkDataObject.CELL).append(
        wdo_in.CellData['localIndex'], 'localIndex')
    wdo_out.GetAttributes(vtk.vtkDataObject.CELL).append(
        wdo_in.CellData['normal'], 'normal')
    wdo_out.GetAttributes(vtk.vtkDataObject.CELL).append(
        wdo_in.CellData['vertexIndices'], 'vertexIndices')
    # all other attributes are left unscaled, not important for stl conversion

    # Construct Polygons (Cells)
    # wdo_out.Polygons    = wdo_in.Polygons[a_ix_point] => not supported by wrapper, so use native VTK
    vtkCellArray = vtk.vtkCellArray()
    for triangle_i in wdo_in.CellData['vertexIndices'].__array__():
        vtkCellArray.InsertNextCell(3, triangle_i)
    wdo_out.VTKObject.SetPolys(vtkCellArray)

    return wdo_out.VTKObject


def write_stl_file(poly_in, path_file, scale=1):

    if scale != 1:
        poly_in = copy_poly_minimal(poly_in, scale=scale)

    vtkSTLwriter = vtk.vtkSTLWriter()
    vtkSTLwriter.SetFileTypeToASCII()
    vtkSTLwriter.SetInputData(poly_in)
    vtkSTLwriter.SetFileName(str(path_file))

    return vtkSTLwriter.Write()

def write_vtp_file(polyout, path_file):

    vtk_writer = vtk.vtkXMLPolyDataWriter()
    vtk_writer.SetDataMode(vtk.vtkXMLWriter.Ascii)
    vtk_writer.SetByteOrderToBigEndian()
    vtk_writer.SetFileName(str(path_file))
    vtk_writer.SetInputData(polyout)

    return vtk_writer.Write()


def read_stl_file(p_stl):

    reader = vtk.vtkSTLReader()
    reader.SetFileName(str(p_stl))
    reader.Update()
    poly = reader.GetOutput()

    return poly


def set_attributetype(field_type):

    if field_type == "CELL":
        attributeType = vtk.vtkDataObject.CELL
    elif field_type == "POINT":
        attributeType = vtk.vtkDataObject.POINT
    elif field_type == "ROW":
        attributeType = vtk.vtkDataObject.ROW
    else:
        raise RuntimeError(
            "Unsupported field attributeType".format(field_type))

    return attributeType


def _create_id_array(dataobject, attributeType):
    """Returns a VTKArray or VTKCompositeDataArray for the ids"""
    if not dataobject:
        raise RuntimeError("dataobject cannot be None")
    if dataobject.IsA("vtkCompositeDataSet"):
        ids = []
        for ds in dataobject:
            ids.append(_create_id_array(ds, attributeType))
        return dsa.VTKCompositeDataArray(ids)
    else:
        return dsa.VTKArray(
            np.arange(dataobject.GetNumberOfElements(attributeType)))


def maskarray_is_valid(maskArray):
    """Validates that the maskArray is either a VTKArray or a
    VTKCompositeDataArrays or a NoneArray other returns false."""
    return maskArray is dsa.NoneArray or \
        isinstance(maskArray, dsa.VTKArray) or \
        isinstance(maskArray, dsa.VTKCompositeDataArray)


def compute(wdo_in, expression, ns=None):
    #  build the locals environment used to eval the expression.
    mylocals = {}
    if ns:
        mylocals.update(ns)

    try:
        # the points are added in case selections is based on the points
        mylocals["points"] = wdo_in.Points
    except AttributeError:
        pass

    return eval(expression, globals(), mylocals)  # boolean mask based on query

def get_array(poly, arrayName, preference='cell'):

    return pv.get_array(pv.wrap(poly), arrayName, preference, err=False)

def get_arrays(attribs):
    """Returns a 'dict' referring to arrays in dsa.DataSetAttributes
    """
    arrays = {}
    for key in attribs.keys():
        # varname = paraview.make_name_valid(key)
        arrays[key] = attribs[key]


def get_point_array(poly, l_dim=None):
    """ get back point array = shape(nb_cells, connnectivity, 3) #output will be transformed to zyx format
    if l_dim is filled in, the coordinates get transformed into pixel coordinates (eg l_dim = [1e-6,0.129e-6,0.129e-6])
    these coordinates are indexes, because flooring is applied (e.g. all pixels with z-location between 0 and 1e-6 will get z=0)
    """
    try:
        pvw = pv.wrap(poly)
    except:
        pvw = poly

    conn = pvw.extract_cells(0).n_points
    a_points = np.zeros((pvw.n_cells, conn, 3))
    for i in range(pvw.n_cells):
        points_cell = pvw.extract_cells(i).points
        for j in range(conn):
            a_points[i, j, :] = points_cell[j]

    a_points = a_points[:, :, [2, 1, 0]]  # from xyz to zyx order
    if l_dim:
        # turns metric coordinates into pixel coordinates
        a_points = np.floor(a_points / np.array(l_dim)).astype(int)

    return a_points


def get_data_array(poly, field_type="CELL", attribute='parentIndex', verbose=True):
    """ get back a point or cell data array """
    if field_type == "CELL":
        data_array = dsa.WrapDataObject(poly).CellData[attribute]
    elif field_type == "POINT":
        data_array = dsa.WrapDataObject(poly).PointData[attribute]

    if isinstance(data_array, vtk.numpy_interface.dataset_adapter.VTKNoneArray):
        if verbose:
            print("{} is no attribute in this vtk object (field_type={})".format(
                attribute, field_type))
        return None

    if data_array.__str__().startswith('vtkString'):
        data_array = pv.convert_string_array(data_array)

    return data_array

def addArray(poly, data, arrayName, fieldType='POINT'):
    '''
    - also works for replacing an array
    - You can also pass singular numeric as well as string values
    - You can pass a list, but it must be of correct length !'''

    poly = pv.wrap(poly)

    if isinstance(data, str):
        nbElements = poly.GetNumberOfPoints(
        ) if fieldType == 'POINT' else poly.GetNumberOfCells()
        data = np.array([data] * nbElements)

    if fieldType == 'POINT':
        if 'point_data' in dir(poly):
            poly.point_data.set_array(data, arrayName)
        else:
            poly.point_arrays.append(data, arrayName)
    elif fieldType == 'CELL':
        if 'cell_data' in dir(poly):
            poly.cell_data.set_array(data, arrayName)
        else:
            poly.cell_arrays.append(data, arrayName)

    return poly


def add_array(poly, a_added, name_array, field_type="CELL", dtype='int'):
    """ Deprecated : use addArray instead
    use this to append a data array. This also works for replacing an array !"""

    # if a_added.dtype.type is np.str_:
    # if dtype =='str':
    # if name_array=='cell_lineage_name':
    #     print(f"{name_array}, {type(a_added[0])}")
    if isinstance(a_added[0], bytes) or (a_added.dtype.type is np.str_):
        a_added = pv.convert_string_array(a_added)
        a_added.SetName(name_array)
        if field_type == "CELL":
            poly.GetCellData().AddArray(a_added)
        elif field_type == "POINT":
            poly.GetPointData().AddArray(a_added)

    else:
        wdo = dsa.WrapDataObject(poly)
        if field_type == "CELL":
            wdo.GetAttributes(vtk.vtkDataObject.CELL).append(
                a_added, name_array)
        elif field_type == "POINT":
            wdo.GetAttributes(vtk.vtkDataObject.POINT).append(
                a_added, name_array)

    return poly


def add_array_with_mapper(poly, name_array_source, name_array_dest, mapper={}, field_type="CELL", default=999):
    """ takes an array (name_array_source) and maps  these values to a new array (name_array_dest) using a dict as mapper
    You also use this to replace an array (source and destination can be the same)"""
    vectorize = False
    a_source = get_data_array(
        poly, field_type=field_type, attribute=name_array_source)
    if vectorize:
        a_dest = np.vectorize(mapper.get)(a_source, default)
    else:
        a_dest = np.array([mapper.get(i, i)
                           for i in a_source.flatten()]).reshape(a_source.shape)
    poly = add_array(poly, a_dest, name_array_dest, field_type=field_type)

    return a_dest


def extractSelectionVtp(poly, queryValue, queryArrayName='parentIndexCanonical'):
    poly = pv.wrap(poly)
    queryArray = pv.get_array(poly, queryArrayName, preference='cell', err=False)
    indSel = np.where(queryArray == queryValue)[0]
    gridSubset = poly.extract_cells(indSel)
    
    return gridSubset.extract_surface()


def extract_selection_vtp(poly_in, query=None, l_cell_idx=None, field_type="CELL", inverse_selection=False, verbose=False):
    """DEPRECATED : use extractSelectionVtp instead !
    Returns a vtk polydata object as a subselection of an input polydata object
    Some words about indexes : 
    Cells
        - CellId              = index (of CellData and Polys (=connectivity and offset arrays)) = PK
        - vtkOriginalCellIds  = index of original vtp (never changed, even after consecutive selections)
        - SelectionCellIds    = index of previous selection(changes every selection step, allows you to go 1 level up)
        - vertexIndices       = related point indices (=redundant = same as connectivity) = FK
    Points  
        - PointId             = index (of PointData and Points) = PK
        - vtkOriginalPointIds = index of original vtp (never changed, even after consecutive selections) 
        - SelectionPointIds   = index of previous selection(changes every selection step, allows you to go 1 level up)

    naming chosen as to comply with the paraview naming

    """
    a_ix_cell = np.array([])
    a_ix_point = np.array([])
    wdo_in = dsa.WrapDataObject(poly_in)
    wdo_out = dsa.WrapDataObject(vtk.vtkPolyData())

    if query:
        attributeType = set_attributetype(field_type)
        if verbose:
            print(wdo_in.GetAttributes(attributeType))
        # d_attr_data = get_arrays(inputs[0].GetAttributes(attributeType))
        attribs = wdo_in.GetAttributes(attributeType)

        # dict : key = 'attributename' , value = array of data
        d_attr_data = {key: attribs[key] for key in attribs.keys()}
        # add "id" array if the query string refers to id.
        if not "id" in d_attr_data.keys() and re.search(r'\bid\b', query):
            d_attr_data["id"] = _create_id_array(wdo_in, attributeType)
        try:
            # SELECTION :returns boolean mask
            maskArray = compute(wdo_in, query, ns=d_attr_data)
        except:
            print ("Error: Failed to evaluate Expression '%s'. "
                   "The following exception stack should provide additional developer "
                   "specific information. This typically implies a malformed "
                   "expression. Verify that the expression is valid.\n", query)
            raise

        if not maskarray_is_valid(maskArray):
            raise RuntimeError("Expression '%s' did not produce a valid mask array. The value "
                               "produced is of the type '{0}'. This typically implies a malformed "
                               "expression. Verify that the expression is valid. {1}".format(query, type(maskArray)))

        if inverse_selection:
            maskArray = algos.logical_not(maskArray)

        # returns indices of boolean mask
        nonzero_indices = algos.flatnonzero(maskArray)

        if field_type == "CELL":
            a_ix_cell = np.array(nonzero_indices)
        else:
            print("only cell based selections are supported right now.")
            return
    else:
        a_ix_cell = np.array(l_cell_idx)

    if not isinstance(a_ix_cell, np.ndarray) or a_ix_cell.size == 0:
        print('warning : nothing selected.  None value will be returned')
        return None

    if field_type == "CELL":

        # STEP1 : Replace CellData
        nb_arrays = wdo_in.GetCellData().GetNumberOfArrays()
        if verbose:
            print("{0} arrays are present in {1}Data".format(
                nb_arrays, field_type))

        for i in range(nb_arrays):
            vtk_array = wdo_in.GetAttributes(
                vtk.vtkDataObject.CELL).GetArray(i)
            attr_name = wdo_in.GetAttributes(
                vtk.vtkDataObject.CELL).GetArrayName(i)
            if attr_name == "SelectionCellIds":
                continue

            if a_ix_cell.size == 0:
                vtk_array_select = np.array([])
            else:
                if isinstance(vtk_array.GetValue(0), str):
                    # indexing not possible on vtkStringArray
                    vtk_array_select = np.array(
                        [vtk_array.GetValue(i) for i in a_ix_cell])
                else:
                    vtk_array_select = vtk_array[a_ix_cell]

            if attr_name == "vertexIndices":
                # not stored in output_poly, only used further down
                a_vtkOriginalPointIds = vtk_array_select.__array__()
                continue

            if verbose:
                print("{0}),{1},{2} ==> {3}".format(
                    i, attr_name, vtk_array.size, vtk_array_select.size))

            # wdo_out.GetAttributes(vtk.vtkDataObject.CELL).append(vtk_array_select,attr_name)
            if isinstance(vtk_array_select[0], str):
                add_array(wdo_out.VTKObject, vtk_array_select,
                          attr_name, field_type="CELL", dtype='str')
            else:
                wdo_out.GetAttributes(vtk.vtkDataObject.CELL).append(
                    vtk_array_select, attr_name)
                if verbose:
                    print(wdo_out.GetAttributes(vtk.vtkDataObject.CELL)[
                          attr_name], "compared to input : \n", wdo_in.GetAttributes(vtk.vtkDataObject.CELL)[attr_name])

        # backup selectionIds to easily refer to the selection 1 level up
        wdo_out.GetAttributes(vtk.vtkDataObject.CELL).append(
            a_ix_cell, "SelectionCellIds")
        # at first selection, this column is newly added
        if isinstance(wdo_out.CellData['vtkOriginalCellIds'], dsa.VTKNoneArray):
            wdo_out.GetAttributes(vtk.vtkDataObject.CELL).append(
                a_ix_cell, "vtkOriginalCellIds")

        # STEP2 : Get points to be selected based on the cell selection
        # unique gives 1D SORTED ascending array
        a_ix_point = np.unique(a_vtkOriginalPointIds)
        d_oldPointId_newPointID = {
            old: new for new, old in enumerate(a_ix_point)}

        # STEP3 : Copy PointData
        nb_arrays = wdo_in.GetPointData().GetNumberOfArrays()
        if verbose:
            print("{0} arrays are present in CellData".format(nb_arrays))
        for i in range(nb_arrays):
            vtk_array = wdo_in.GetAttributes(
                vtk.vtkDataObject.POINT).GetArray(i)
            attr_name = wdo_in.GetAttributes(
                vtk.vtkDataObject.POINT).GetArrayName(i)
            if attr_name == "SelectionPointIds":
                continue

            if a_ix_point.size == 0:
                vtk_array_select = np.array([])
            else:
                vtk_array_select = vtk_array[a_ix_point]
            if verbose:
                print("{0}),{1},{2} ==> {3}".format(
                    i, attr_name, vtk_array.size, vtk_array_select.size))

            wdo_out.GetAttributes(vtk.vtkDataObject.POINT).append(
                vtk_array_select, attr_name)

        wdo_out.GetAttributes(vtk.vtkDataObject.POINT).append(
            a_ix_point, "SelectionPointIds")  # backup original ids as extra column
        # at first selection, this column is newly added
        if isinstance(wdo_out.PointData['vtkOriginalPointIds'], dsa.VTKNoneArray):
            wdo_out.GetAttributes(vtk.vtkDataObject.POINT).append(
                a_ix_point, "vtkOriginalPointIds")

        # STEP4: Copy Points
        if a_ix_point.size != 0:
            wdo_out.Points = wdo_in.Points[a_ix_point]

        # STEP5: Construct Polygons (Cells)
        # wdo_out.Polygons    = wdo_in.Polygons[a_ix_point] => not supported by wrapper, so use native VTK
        vtkCellArray = vtk.vtkCellArray()
        vertexIndices_new = []
        for p1, p2, p3 in a_vtkOriginalPointIds:
            l_new_triangle = [d_oldPointId_newPointID[p1],
                              d_oldPointId_newPointID[p2], d_oldPointId_newPointID[p3]]
            vtkCellArray.InsertNextCell(3, l_new_triangle)
            vertexIndices_new.append(l_new_triangle)

        wdo_out.VTKObject.SetPolys(vtkCellArray)

        # STEP6: update CellData vertexIndices
        wdo_out.GetAttributes(vtk.vtkDataObject.CELL).append(
            np.array(vertexIndices_new), "vertexIndices")

    if a_ix_point.size != 0:
        return wdo_out.VTKObject
    else:
        return None


def get_aggregate_data(poly, a_cell_id, l_aggregate_columns, func="sum"):
    wdo = dsa.WrapDataObject(poly)
    l_output = []

    if func == "sum":
        for ix, column in enumerate(l_aggregate_columns):
            l_output.append(
                np.sum(wdo.CellData[l_aggregate_columns[ix]][a_cell_id].__array__()))
    else:
        print('func not supported')

    return l_output


def get_point_data_of_triangles(poly, a_cell_id, return_points=False):
    """ Given a list of triangle indices, return centroid and normal
    """
    wdo = dsa.WrapDataObject(poly)
    a_vertexIndices = wdo.CellData['vertexIndices'].__array__()[a_cell_id]
    a_points = wdo.PointData['x'].__array__()[list(set(a_vertexIndices.flatten()))]
    centroid, normal = trimesh.points.plane_fit(a_points)

    return points if return_points else None, centroid, normal


# def get_centroid_of_triangles(poly, a_cell_id):
#     """ Given a list of triangle indices, return the centroid point coordinates (3,)"""
#     wdo = dsa.WrapDataObject(poly)
#     a_vertexIndices = wdo.CellData['vertexIndices'].__array__()[a_cell_id]
#     return np.mean(wdo.PointData['x'].__array__()[list(set(a_vertexIndices.flatten()))], axis=0)


def extract_parentID_selection_from_VTP(poly_in, verbose=False):
    d_parentID_VTP = {}
    a_parent_id = np.unique(dsa.WrapDataObject(
        poly_in).CellData['parentIndex'].__array__())
    for parent_id in a_parent_id:
        if verbose:
            print(
                '--processing cell with parentID {0}'.format(parent_id), flush=True)
        poly_out = extract_selection_vtp(
            poly_in, query="parentIndex == {0}".format(parent_id), field_type="CELL")
        d_parentID_VTP[parent_id] = poly_out

    return d_parentID_VTP


def enrich_embryo_with_contactID(poly_in, d_parentID_map=None):
    """
    d_parentID maps the standard parentID into a canonical parent ID

    """
    a_parentIndex = dsa.WrapDataObject(
        poly_in).CellData['parentIndex'].__array__()
    a_contactIndex = dsa.WrapDataObject(
        poly_in).CellData['contact_index'].__array__()
    a_contact_id = np.array([a_parentIndex[i] for i in a_contactIndex])
    # contactindex=0 means no contact => contact_id = parent_id in that case
    a_contact_id = np.where(a_contactIndex == 0, a_parentIndex, a_contact_id)
    poly_out = add_array(poly_in, a_contact_id, "contactId", field_type="CELL")

    if d_parentID_map:
        a_parentID_canonical = np.array(
            [d_parentID_map[x] for x in a_parentIndex])
        a_contactID_canonical = np.array(
            [d_parentID_map[x] for x in a_contact_id])
        poly_out = add_array(poly_out, a_parentID_canonical,
                             "parentId_canonical", field_type="CELL")
        poly_out = add_array(poly_out, a_contactID_canonical,
                             "contactId_canonical", field_type="CELL")

    return poly_out


def get_parentIDs(poly_in):
    a_parentIndex = dsa.WrapDataObject(
        poly_in).CellData['parentIndex'].__array__()
    return np.sort(np.unique(a_parentIndex))


def get_unique_values(poly_in, attrib='parentIndex'):
    a_attrib = dsa.WrapDataObject(poly_in).CellData[attrib].__array__()
    return np.sort(np.unique(a_attrib))


def convert_to_line_mesh(poly_in):
    """returns same poly mesh, but with cell_type = lines instead of triangles"""
    extractEdges = vtk.vtkExtractEdges()
    extractEdges.SetInputData(poly_in)
    extractEdges.Update()
    poly_line = extractEdges.GetOutput()
    wdo_line = dsa.WrapDataObject(poly_line)
    # a_edges =  poly_line.GetLines()  #vtkCellArray of lines
    return poly_line


def get_outside_points(poly_in):
    """returns outside points of a triangulated mesh. 
    These are points associated with lines that are only part of one triangle"""

    # step1 : build ds that counts occurence of lines (=2-sets) in triangles
    from collections import defaultdict
    d_line_count = defaultdict(int)
    wdo_in = dsa.WrapDataObject(poly_in)
    # =the connectivity array
    a_connectivity = wdo_in.CellData['vertexIndices'].__array__()
    for triangle_i in a_connectivity:
        triangle_i.sort()
        line1 = [triangle_i[0], triangle_i[1]]
        line2 = [triangle_i[0], triangle_i[2]]
        line3 = [triangle_i[1], triangle_i[2]]
        for line in [line1, line2, line3]:
            d_line_count[tuple(line)] += 1

    # step2 : get indices of outside points
    s_outside_points = set()
    for line_i, count in d_line_count.items():
        if count == 1:
            for point_i in line_i:
                s_outside_points.add(point_i)

    # step3 : get xyz of outside points + poly
    a_points_xyz = wdo_in.Points.__array__()[np.array(list(s_outside_points))]
    wdo_border_points = dsa.WrapDataObject(vtk.vtkPolyData())
    wdo_border_points.SetPoints(a_points_xyz)

    return a_points_xyz, wdo_border_points.VTKObject


def construct_plane(point_plane, normal_plane):
    vtk_plane = vtk.vtkPlaneSource()
    vtk_plane.SetCenter(point_plane[0], point_plane[1], point_plane[2])
    vtk_plane.SetNormal(normal_plane[0], normal_plane[1], normal_plane[2])
    vtk_plane.Update()
    return vtk_plane.GetOutput()


def scale_poly(poly_in, scale=1.e6):
    '''the shape of the poly is scaled, all other data remains unaffected'''
    wdo_in = dsa.WrapDataObject(poly_in)
    a_points_xyz = wdo_in.Points.__array__()
    wdo_in.SetPoints(a_points_xyz * scale)

    return wdo_in.VTKObject


def poly_to_trimesh(poly_in, scale=1, process=False):
    """convert a vtk poly to a trimesh object.  Beware, trimesh will reindex the points if process=True"""
    wdo_in = dsa.WrapDataObject(poly_in)
    a_polygons = wdo_in.GetPolygons()
    connectivity = a_polygons[0]
    return trimesh.Trimesh(vertices=wdo_in.Points * scale, faces=a_polygons.reshape(-1, connectivity + 1)[:,1:], process=process)


def list_attributes(poly, repr=True):
    """ returns the attribute names in a list (point and cell attributes)"""
    wdo = dsa.WrapDataObject(poly)
    l_point_attr = [wdo.GetAttributes(vtk.vtkDataObject.POINT).GetArrayName(
        i) for i in range(wdo.GetPointData().GetNumberOfArrays())]
    l_cell_attr = [wdo.GetAttributes(vtk.vtkDataObject.CELL).GetArrayName(
        i) for i in range(wdo.GetCellData().GetNumberOfArrays())]

    if repr:
        print('POINT attributes -> \n', l_point_attr)
        print('CELL attributes -> \n', l_cell_attr)

    return l_point_attr, l_cell_attr


def list_unique_values(poly, attribute='cellName', field_type="CELL", repr=True):
    """ returns the attribute names in a list (point and cell attributes)"""
    wdo = dsa.WrapDataObject(poly)
    data_array = get_data_array(
        poly, field_type=field_type, attribute=attribute)
    if data_array is None:
        return []
    else:
        a_unique = np.unique(data_array)
        # print(f"Unique values in {attribute} are {a_unique}")
        return a_unique


def get_mapper(poly, name_array_key, name_array_value, field_type="CELL"):
    """finds the all combinations between two arrays and provides a mapper between them"""
    a_key = get_data_array(poly, field_type=field_type,
                           attribute=name_array_key)
    if a_key is None:
        return None

    a_value = get_data_array(poly, field_type=field_type,
                             attribute=name_array_value)
    if a_value is None:
        return None

    s_pairs = set([(i, j) for i, j in zip(a_key, a_value)])
    mapper = {i: j for i, j in s_pairs}

    return mapper


def create_poly_from_df(df_input, type='spheres', return_poly=True, output_file=None, add_xyzr_extra=True, verbose=False):
    """ a dataframe with 
    spheres :
        x,y,z of center, radius should be provided.  Additional columns will be added as cell data arrays
        e.g :   x   y   z   radius  spID    clID    ceID
    lines : 
        x1,y1,z1,x2,y2,z2
    int is the default datatype for added cell arrays.
    an output file location is mandatory because a temporary vtp file must be written to disk, but if not given the paraview VTK object without extra data will be returned
    output = VTK poly
    """
    df_input.reset_index(inplace=True)
    df_cell_data = {}

    if type == 'lines':
        ix_extra_data = 6
        connectivity = 2  # pv.Lines.lines (= a line mesh)
    elif type == 'spheres':
        ix_extra_data = 4
        connectivity = 3  # pv.Sphere.faces (=a triangulated mesh)
    elif type == 'vertices':
        ix_extra_data = 3
        connectivity = 1  
    if add_xyzr_extra:
        ix_extra_data = 0

    # 1 - creation of the elementary objects (spheres, lines)
    if type == 'vertices':
        #vtp_points  = np.array(df_input.iloc[:,0:3])
        vtp_points = np.transpose(np.vstack(
            [np.array(df_input['x']), np.array(df_input['y']), np.array(df_input['z'])]))
        for i in range(ix_extra_data, len(df_input.columns)):
            df_cell_data[df_input.columns[i]] = np.array(df_input.iloc[:, i])

    else:
        blocks = pv.MultiBlock()
        for row_i in df_input.iterrows():
            if type == 'spheres':
                blocks.append(pv.Sphere(radius=row_i[1].radius, center=(row_i[1].x, row_i[1].y, row_i[1].z), direction=(
                    0, 0, 1), theta_resolution=10, phi_resolution=10, start_theta=0, end_theta=360, start_phi=0, end_phi=180))
            elif type == 'lines':
                blocks.append(pv.Line(pointa=(row_i[1].x1, row_i[1].y1, row_i[1].z1), pointb=(
                    row_i[1].x2, row_i[1].y2, row_i[1].z2)))

        # 2 - creating 1 vtp file
        # (WATCH OUT, the following destroys the sphere data (updates face information), so we take a copy
        blocks_copy = blocks.copy(deep=True)
        vtp_points = np.zeros((0, 3), dtype='float64')
        vtp_mesh_components = np.zeros((0,), dtype='int64')
        for extra_data_column in df_input.columns[ix_extra_data:]:
            df_cell_data[extra_data_column] = np.zeros((0,), dtype='int64')

        for ix_block, block_i in enumerate(blocks_copy):
            connectivity_i = block_i.lines if type == 'lines' else block_i.faces
            if verbose:
                print('the number of points in this block = ', len(block_i.points))
            if verbose:
                print('the number of mesh components (triangles, lines..) in this block (sphere, or line or..) = ', len(
                    connectivity_i) / (connectivity + 1))
            mesh_components = connectivity_i.reshape((-1, connectivity + 1))
            # we need to adjust the points indices in  the mesh_components
            mesh_components[:, 1:] = mesh_components[:, 1:] + len(vtp_points)

            for extra_data, value in df_cell_data.items():
                # the data we will add to Cell  (for visualisation and selection in paraview)
                value_append = np.full(
                    (len(mesh_components),), df_input.at[ix_block, extra_data])
                df_cell_data[extra_data] = np.concatenate(
                    (value, value_append))
            vtp_points = np.concatenate((vtp_points, block_i.points))
            vtp_mesh_components = np.concatenate(
                (vtp_mesh_components, mesh_components.reshape(-1,)))

    #classpyvista.PolyData(var_inp=None, faces=None, n_faces=None, lines=None, n_lines=None, deep=False)
    if connectivity == 1:
        pv_mesh = pv.PolyData(vtp_points)
    elif connectivity == 2:
        # pyvista bug , turns it into verts instead of lines (e.g 6 verts(=points) instead of 3 lines)
        pv_mesh = pv.PolyData(vtp_points, lines=vtp_mesh_components)
        #pv_mesh = pv.PolyData(vtp_points, vtp_mesh_components)
    else:
        # faces= does not work for some reason
        pv_mesh = pv.PolyData(vtp_points, vtp_mesh_components)

    if output_file:
        pv_mesh.save(output_file, binary=False)
    else:
        return pv_mesh

    # add the extra cell data arrays
    poly_mesh = read_vtp_file(output_file)
    field_type = "POINT" if type == 'vertices' else "CELL"

    for extra_data_column in df_input.columns[ix_extra_data:]:
        poly_mesh = add_array(
            poly_mesh, df_cell_data[extra_data_column], extra_data_column, field_type=field_type, dtype='int')
    pv_mesh_added = pv.wrap(poly_mesh)
    pv_mesh_added.save(output_file, binary=False)
    if verbose:
        print('Final output vtp file is written to ', output_file)

    return poly_mesh if return_poly else None


def df_stl_to_vtm(df_input, f_out=None, verbose=True):
    """ given a df with a column f_mesh (pointing to stl-files), construct 1 vtm-file and enrich with the columns from the df"""
    blocks = pv.MultiBlock()
    for ix, row_i in df_input.iterrows():
        if not row_i.f_mesh:
            continue
        # print(row_i.f_mesh, type(row_i.f_mesh))
        poly = pv.PolyData(read_stl_file(row_i.f_mesh))
        if poly.GetNumberOfCells() == 0:
            print(f"Error while writing stl to vtm : {Path(row_i.f_mesh).name} has no cells !  skipping...")
            continue

        # add data arrays
        for attr_name_i, data_i in row_i.items():
            if attr_name_i in ['f_mesh']:  # skip
                continue
            if data_i == 'NA':
                data_i = -1
            if isinstance(data_i, str):
                a_data = np.chararray((poly.GetNumberOfCells(),), itemsize=10)
            else:
                a_data = np.zeros((poly.GetNumberOfCells(),))
            a_data.fill(data_i)
            poly = add_array(poly, a_data, attr_name_i,
                             field_type='CELL')  # ,dtype='int')
        blocks.append(poly)

    if f_out is None:  # default name and location
        f_out = (Path(df_input['f_mesh'].iloc[0]).parent / 'embryo.vtm')

    blocks.save(f_out, binary=False)
    if verbose:
        print(f"VTM file is written to {f_out}")

    return


def get_surface_from_tet_mesh_trimesh(a_tet, a_points,
                                      manifold=True, filter_spikes=True,
                                      mapper_indices=False,
                                      remove_unreferenced_vertices=False):
    """ Given a set of tetrahedrons, give back a trimesh mesh.
    If manifold=False all the faces will be used, otherwise only the unique faces at the outside of the mesh are used
    Given that trimesh will reindex the points, a map is also returned for easy traceback
    remove_unreferenced_vertices: compacts the nodes in the mesh, but keep in mind this reindexes all faces"""
    a_tet = np.unique(
        a_tet, axis=0)  # filter out possible duplicate tetrahedrons (cc are not in a set..)

    if len(a_tet.shape) == 1:  # in case of single cc
        a_tet = a_tet.reshape(1, -1)

    if filter_spikes and manifold:
        a_unique_points = np.where(np.bincount(a_tet.flatten()) == 1)[0]
        a_tet = a_tet[np.invert(np.isin(a_tet, a_unique_points).any(axis=1))]

    cell_faces = np.concatenate(
        [a_tet[:, [i, j, k]] for i, j, k in combinations([0, 1, 2, 3], 3)], axis=0)

    # if filter_spikes and manifold:
    #     #filtering spikes
    #     a_unique_points = np.where(np.bincount(a_tet.flatten()) ==1 )[0]
    #     cell_faces = cell_faces[np.invert(np.isin(cell_faces, a_unique_points).any(axis=1))]
    #     #some internal faces are now exposed => these must be added extra
    #     a_tet_affected = np.where(np.sum(np.isin(a_tet, a_unique_points), axis=1)==1)
    #     if len(a_tet_affected)>0:
    #         a_faces_new = a_tet[a_tet_affected][np.invert(np.isin(a_tet[a_tet_affected], a_unique_points))].reshape(-1, 3)
    #         cell_faces = np.concatenate([cell_faces, a_faces_new])

    if not manifold:
        s_unique_faces = {frozenset(i) for i in cell_faces}
    else:
        s_unique_faces = set([])
        for face_i in cell_faces:
            s_face = frozenset(face_i)
            if s_face in s_unique_faces:
                # only works because a face can only appear twice at max
                s_unique_faces.remove(s_face)
            else:
                s_unique_faces.add(s_face)
    a_faces = np.array([list(i) for i in s_unique_faces])

    if len(a_faces) == 0:  # filtering spikes can remove all faces
        return None, None
    # process True will eliminate some faces..
    mesh = trimesh.Trimesh(vertices=a_points, faces=a_faces, process=False)
    if remove_unreferenced_vertices:
        mesh.remove_unreferenced_vertices()  #  reindexes faces

    if mapper_indices:
        # l_point_ix = []
        # for point_ix in a_faces.flatten():
        #     if point_ix not in l_point_ix:
        #         l_point_ix.append(point_ix)
        l_point_ix = [  set(a_faces.flatten())]
        d_tmhix_pointix = {ix: pointix for ix,
                           pointix in enumerate(l_point_ix)}
    else:
        d_tmhix_pointix = {}

    return mesh, d_tmhix_pointix

# def get_surface_from_tet_mesh_trimesh(a_tet, a_points, manifold=True, mapper_indices=False):
#     """ Given a set of tetrahedrons, give back a trimesh mesh.
#     If manifold=False all the faces will be used, otherwise only the unique faces at the outside of the mesh are used
#     Given that trimesh will reindex the points, a map is also returned for easy traceback"""
#     a_tet = np.unique(a_tet, axis=0) # filter out possible duplicate tetrahedrons (cc are not in a set..)

#     if len(a_tet.shape)==1: # in case of single cc
#         a_tet = a_tet.reshape(1,-1)
#     cell_faces = np.concatenate([a_tet[:, [i, j, k]] for i, j, k in combinations([0, 1, 2, 3],3)], axis=0)

#     if not manifold:
#         s_unique_faces = {frozenset(i) for i in cell_faces}
#     else:
#         s_unique_faces = set([])
#         for face_i in cell_faces:
#             s_face = frozenset(face_i)
#             if s_face in s_unique_faces:
#                 s_unique_faces.remove(s_face) # only works because a face can only appear twice at max
#             else:
#                 s_unique_faces.add(s_face)
#     a_faces = np.array([list(i) for i in s_unique_faces])
#     mesh = trimesh.Trimesh(vertices=a_points, faces=a_faces)

#     if mapper_indices:
#         l_point_ix = []
#         for point_ix in a_faces.flatten():
#             if point_ix not in l_point_ix:
#                 l_point_ix.append(point_ix)
#         d_tmhix_pointix = {ix: pointix for ix, pointix in enumerate(l_point_ix)}
#     else:
#         d_tmhix_pointix = {}

#     return mesh, d_tmhix_pointix


def write_meshes_from_simplices(d_ce_cc, a_tri, a_points, f_out, df_clusters=None, manifold=True, flipZX=False, repair=True, smoothing=None, write_vtm=True, mesh_key='cc_seed'):
    """ d_ce_cc : link from the seedcc to the cluster of cc's
        a_tri : the simplices of the cc (= indices of a_points)
        df_clusters : if this is provided, you can add additional data columns, a column 'cc_seed' must exist in this df
        f_out : path of the resulting vtm file.  a folder containing subparts will be created also
        manifold : get the manifold, else the tetrahedral mesh will be written 
        flipZX : sometimes needed to match with the input image when visualising
        BEWARE : trimesh REINDEXES the points, so original a_point indices are lost . Use dd_cl_tmhix_pointix , or reconstruct using x,y,z position! 
        mesh_key : the column name containing the keys for the meshes (must match keys in d_ce_cc)"""
    d_cell_mesh = {}
    p_out = f_out.parent / f_out.stem
    shutil.rmtree(p_out, ignore_errors=True)
    (p_out / "stl").mkdir(parents=True, exist_ok=True)

    if flipZX:
        a_points = a_points[:, [2, 1, 0]]

    if df_clusters is None:
        df_clusters = pd.DataFrame(
            {mesh_key: list(d_ce_cc.keys()), 'f_mesh': ""})

    for ix, cl_i in enumerate(df_clusters[mesh_key]):
        cc_i = d_ce_cc.get(cl_i)
        if cc_i is None:
            print(f"Error cl_i {cl_i} is present in the dataframe ({mesh_key}), but has no circumspheres (not a key in d_ce_cc).  skipping..")
            continue
        cell_mesh, _ = get_surface_from_tet_mesh_trimesh(
            a_tri[cc_i], a_points, manifold=manifold, filter_spikes=True)
        if cell_mesh is None:
            p_mesh = ""  # no stl written, all faces filtered out
        else:
            p_mesh= p_out / "stl" / f"{str(ix).zfill(3)}_{cl_i}.stl"
            cell_mesh.export(p_mesh, file_type='stl')
            if repair:

                cell_mesh = repair_triangle_soup(p_stl_in=p_mesh, p_stl_out=p_mesh.parent / (p_mesh.stem + "_repaired.stl"), verbose=False)
            d_cell_mesh[cl_i]  = cell_mesh
            if smoothing:
                smooth_surface_mesh(
                    cell_mesh, iterations=smoothing['iterations'], lamb=smoothing['lambda'])
                cell_mesh.export(p_mesh, file_type='stl')
        df_clusters.loc[df_clusters[mesh_key] == cl_i, 'f_mesh'] = str(p_mesh)

    df_clusters.to_csv(p_out / "df_clusters.csv", index=False)

    if write_vtm:
        df_stl_to_vtm(df_clusters, f_out=f_out)

    return df_clusters, d_cell_mesh


def write_meshes_timelapse(d_stack_ds, df_mesh, p_meshes, f_meshes=None, mode='all', level='blocks', scaling_ZYX=None, repair=True, verbose=False):
    """ Write meshes for a whole timelapse, with extra data columns
    level : when 'blocks'  meshes represent blocks, when 'cell' meshes represent cells"""
    if mode == 'hierarch':
        if 'trimmed' not in df_mesh:
            df_mesh['trimmed'] = int(-1)
        sel_columns = ['index', 'cc_seed', 'lbl_hierarch', 'lbl_hierarch_paraview', 'seed_code',
                       'fclust', 'bl_prev_link', 'lbl_hierarch', 'lbl_cell', 'trimmed', 'seed_merge_iterZ', 'r']
    elif mode == 'all':
        sel_columns = list(df_mesh.columns)
    else:
        sel_columns = ['index', 'cc_seed', 'lbl_tmin', 'lbl_t', 'lbl_tplus', 'lbl_tmin_paraview', 'lbl_t_paraview',
                       'lbl_tplus_paraview', 'emitter_code', 'conflicts', 'pdf0', 'pdf1', 'pdf2', 'pdf3', 'pdf4', 'lineage_lbl']

    if scaling_ZYX is not None:
        print(f"Scaling points to {scaling_ZYX}")

    df_mesh['f_mesh'] = 'NA'
    for ix_time, stack_ds_i in d_stack_ds.items():
        if len(df_mesh[df_mesh['t'] == ix_time]) == 0:
            continue
        else:
            if verbose:
                print(f"Writing meshes for stack_ix {ix_time}...", flush=True)
            else:
                print(f"{ix_time}|", flush=True, end="")
            if scaling_ZYX:
                a_points = stack_ds_i['a_points'] if scaling_ZYX is None else np.array(
                    [i * scaling for i, scaling in zip(stack_ds_i['a_points'].T, scaling_ZYX)]).T
            else:
                a_points = stack_ds_i['a_points']

            f_out_meshes = (p_meshes / f"embryo_t{str(ix_time).zfill(2)}.vtm") if f_meshes is None else (p_meshes / f"{f_meshes}_ixt{str(ix_time).zfill(2)}.vtm")
            a_points = stack_ds_i['a_points'] if scaling_ZYX is None else np.array(
                [i * scaling for i, scaling in zip(stack_ds_i['a_points'].T, scaling_ZYX)]).T
            df_clusters = write_meshes_from_simplices(d_ce_cc=stack_ds_i['d_ce_cc'] if level == 'cells' else stack_ds_i['d_bl_cc'],
                                                      a_tri=stack_ds_i['a_tri'],
                                                      a_points=a_points,
                                                      f_out=f_out_meshes,
                                                      df_clusters=df_mesh[df_mesh['t'] == ix_time][sel_columns],
                                                      flipZX=True,
                                                      repair=repair)

    df_mesh.loc[df_clusters.index, 'f_mesh'] = df_clusters['f_mesh']
    print(f"\n... meshes written to {p_meshes}")
    return df_clusters


def mesh_OK(tmh_mesh):
    return tmh_mesh.is_watertight and tmh_mesh.is_volume


def examine_mesh(tmh_mesh, load_in_tmh=False, log_text=""):
    if load_in_tmh:
        tmh_mesh = trimesh.load_mesh(tmh_mesh, process=False)
    print(f"Examining mesh {log_text}--------------------:")
    print(f"Watertight ? {'Yes' if tmh_mesh.is_watertight else 'No'}")
    print(f"Volume ? {'Yes' if tmh_mesh.is_volume else 'No'} -> size = {tmh_mesh.volume}")
    broken_faces = trimesh.repair.broken_faces(tmh_mesh)
    print(f"Broken faces ? {'No' if len(broken_faces)==0 else 'Yes'} -> broken faces = {broken_faces}")
    print(f"Number of vertices : {tmh_mesh.vertices.shape}. Number of faces: {tmh_mesh.faces.shape[0]}")

    return


def repair_triangle_soup_deprecated(p_mesh, tmh_mesh=None, suffix_repair='', verbose=False):
    """
    Try to reconstruct a valid manifold from a triangle soup, eliminating duplicate vertices, fixing normals, windings..
    the result should be a watertight surface mesh with a well defined volume
    The input triangle soup must be provided as an STL-file, if suffix_repair='' the repair will be performed in place
    """
    
    try:
        p_mesh_original = p_mesh
        if suffix_repair:
            p_mesh = p_mesh.parent / (p_mesh.stem + "_repaired.stl")
        _meshfix.clean_from_file(str(p_mesh_original), str(p_mesh))
        if not tmh_mesh:
            tmh_mesh = trimesh.load_mesh(p_mesh, process=False)
        tmh_mesh.process(validate=False)
        trimesh.repair.fix_normals(tmh_mesh)
        # tmh_mesh.process(validate=False)
        tmh_mesh.export(p_mesh, file_type='stl')
        if verbose:
            if not mesh_OK(tmh_mesh):
                print(f"Repair on {p_mesh.name} failed", flush=True, end="")
                examine_mesh(tmh_mesh, load_in_tmh=False,
                             log_text="Failed repair attempt")
            else:
                if verbose:
                    print(f' {p_mesh.name}-> repaired|'.format(), flush=True, end="")
    except:
        traceback.print_exc()
        print(f'Error : something went wrong with repairing {p_mesh}')

    return p_mesh, tmh_mesh


def repair_triangle_soup(tmh_mesh=None, p_stl_in=None, p_stl_out=None, verbose=False):
    """
    Try to reconstruct a valid manifold from a triangle soup, using a series of repair operations
    -> eliminating duplicate vertices, fixing normals, windings..
    the result should be a watertight surface mesh with a well defined volume
    Can be done in memory or via disk : Either input a trimesh or a path to an STl file
    """
    
    try:
        if p_stl_in:
            _meshfix.clean_from_file(str(p_stl_in), str(p_stl_out))
            tmh_mesh = trimesh.load_mesh(p_stl_out, process=False)
        else:
            tmh_mesh = pymeshfix_repair_steps(tmh=tmh_mesh)

        tmh_mesh.process(validate=False)
        trimesh.repair.fix_normals(tmh_mesh)
        # tmh_mesh.process(validate=False)
        tmh_mesh.remove_unreferenced_vertices()

        if p_stl_out:
            tmh_mesh.export(p_stl_out, file_type='stl')

        if verbose:
            if not mesh_OK(tmh_mesh):
                print(f"Repair on {p_stl_out.name} failed", flush=True, end="")
                examine_mesh(tmh_mesh, load_in_tmh=False,
                             log_text="Failed repair attempt")
            else:
                print(f' {p_stl_out.name}-> repaired|'.format(), flush=True, end="")

    except:
        traceback.print_exc()
        print(f'Error : something went wrong with repairing {p_stl_in if p_stl_in else tmh_mesh}')

    return tmh_mesh


def repair_tmh_mesh(tmh_mesh, verbose=False):
    try:
        if tmh_mesh.volume < 0:
            if verbose:
                print(" -> process and fix winding")
            tmh_mesh.process(validate=False)
            trimesh.repair.fix_winding(tmh_mesh)
            if verbose:
                print(f"Watertight ? {tmh_mesh.is_watertight}")
                print(f"Volume ? {tmh_mesh.is_volume} (size = {tmh_mesh.volume})")
    except:
        print('something went wrong with trimesh repair on {0}. continue')

    return


# def remesh_stl_deprecated(p_stl, tmh=None, in_place=False,
#                nb_nodes=1200, repair=True, verbose=False):
#     """ returnes a vtk triangulated mesh in VTK format"""

#     if tmh is None:
#         mesh = pv.read(p_stl)
#     else:
#         mesh = pv.wrap(tmh)
#     clus = clustering.Clustering(mesh)
#     clus.subdivide(3)  # triangulation
#     clus.cluster(nb_nodes)
#     remesh_poly_out = clus.create_mesh()

#     if in_place and p_stl:
#         write_stl_file(remesh_poly_out, p_stl)
#         if verbose:
#             print(f"Remeshed stl written to {p_stl}")
#     if repair:
#         p_mesh, tmh_mesh = repair_triangle_soup(
#             p_stl, suffix_repair='', verbose=False)
#         # _meshfix.clean_from_file(str(p_stl), str(p_stl))
#     return tmh_mesh


def _get_nb_nodes_cell_mesh(poly_mesh, surface_to_nodes_factor=2, min_nb_nodes=100, max_nb_nodes=1200):

    nb_nodes = int(poly_mesh.area * surface_to_nodes_factor)
    # if nb_nodes < 10:
    #     print(f" -> <WARNING> Skipping cell because the number of determined nodes is too small.")

    return np.clip(nb_nodes, min_nb_nodes, max_nb_nodes)



def remesh_mesh(tmh=None, p_stl_in=None, p_stl_out=None,
               nb_nodes=-1, repair=True, verbose=False):
    """ input either a trimesh or a path to an stl-file
    if p_stl_out is filled a stl-file is written"""

    if tmh is None:
        mesh = pv.read(p_stl_in)
    else:
        mesh = pv.wrap(tmh)

    clus = clustering.Clustering(mesh)
    clus.subdivide(3)  # triangulation
    if nb_nodes == -1:
        clus.cluster(_get_nb_nodes_cell_mesh(mesh))
    else:
        clus.cluster(nb_nodes)
    tmh_remesh = poly_to_trimesh(clus.create_mesh())

    if repair:
        tmh_remesh = repair_triangle_soup(tmh_mesh=tmh_remesh, verbose=False)
        # _meshfix.clean_from_file(str(p_stl), str(p_stl))

    if p_stl_out:
        tmh_remesh.export(p_stl_out, file_type='stl')
        if verbose:
            print(f"Remeshed stl written to {p_stl_out}")

    return tmh_remesh


def smooth_surface_mesh(tmh, method='mut_dif_laplacian', l_freeze_verts=[], iterations=100, lamb=0.5):
    """smoothing inplace
     """
    if l_freeze_verts:
        a_freeze_verts = tmh.vertices[l_freeze_verts]
    tmh_centroid = tmh.centroid
    tmh.vertices = tmh.vertices - tmh_centroid

    if method == 'laplacian':
        filter_laplacian(tmh, iterations=iterations, lamb=lamb)
    elif method == 'humphrey':
        filter_humphrey(tmh, iterations=iterations, alpha=0.1, beta=0.5)
    elif method == 'mut_dif_laplacian':
        filter_mut_dif_laplacian(tmh, iterations=iterations, lamb=lamb)
    elif method == 'taubin':
        filter_taubin(tmh, iterations=iterations, lamb=lamb, nu=0.1)

    tmh.vertices = tmh.vertices + tmh_centroid

    if l_freeze_verts:
        tmh.vertices[l_freeze_verts] = a_freeze_verts

    return


def create_dummy_poly(p_out, dummy_position=[10.e-6, 10.e-6, 10.e-6]):
    # mesh = trimesh.Trimesh(vertices=[[0, 0, 0], [0, 0, 1], [0, 1, 0]],
    #                        faces=[[0, 1, 2]])
    mesh = trimesh.Trimesh(vertices=[dummy_position, dummy_position, dummy_position],
                           faces=[[0, 1, 2]])
    mesh.export(p_out / 'dummy.stl', file_type='stl')

    return read_stl_file(p_out / 'dummy.stl')


def map_nodes_to_triangle(poly_in):
    ''' this will map a nodeindex to a random triangle index (in practice the one with the highest triangle index).
    In other words, you get a 1:1 mapping which can then be used to transfer triangle attributes to point attributes '''
    wdo_in = dsa.WrapDataObject(poly_in)
    a_connectivity = wdo_in.CellData['vertexIndices'].__array__()
    l_d_node_triangle = []
    for i in range(3):
        l_d_node_triangle.append({k: v for k, v in zip(
            np.sort(a_connectivity.T[i]), np.argsort(a_connectivity.T[i]))})
    d_node_triangle = {**l_d_node_triangle[0], **l_d_node_triangle[1], **l_d_node_triangle[2]}

    return d_node_triangle


def extract_data_from_vtp(p_vtp, field_type="POINT", l_attrib_sel=['myosin_raw', 'x', 'parentIndex']):

    a_poly = read_vtp_file(p_vtp)
    # wdo_in = dsa.WrapDataObject(a_poly)
    l_point_attr, l_cell_attr = list_attributes(a_poly, repr=False)
    l_attr = l_point_attr if field_type == "POINT" else l_cell_attr
    l_data = []
    column_labels = []

    for attrib_i in l_attrib_sel:
        if attrib_i not in l_attr:
            print(f"{attrib_i} not in attributes. skip", flush=True, end='')
            continue
        a_attrib = get_data_array(
            a_poly, field_type=field_type, attribute=attrib_i)
        if len(a_attrib.shape) > 1:
            for ix, i in enumerate(a_attrib.T):
                l_data.append(i)
                if attrib_i == "x":
                    column_labels.append(['x', 'y', 'z'][ix])
                else:
                    column_labels.append(f"{attrib_i}_{ix}")
        else:
            l_data.append(a_attrib)
            column_labels.append(attrib_i)
    return pd.DataFrame(np.stack(l_data).T, columns=column_labels)


def extract_data_from_vtp_folder(ipt_folder, l_attrib_sel, regex_files, field_type='POINT', add_index=True, add_tc_label=True):
    '''
    e.g.
    ipt_folder =  Path('/home/wth/Downloads/testinfra/OUTPUT/wt/wtAgg/selected_embryo/')
    l_attrib_sel = ['myosin_raw', 'x', 'parentIndex']
    regex_files = 'selected_embryo_R06_t*.vtp
    add_index = the original index reflects the index within each vtp
    '''
    l_df = []
    d_vtp_t = {}
    for ix, vtp_i in enumerate(sorted(ipt_folder.rglob(regex_files))):
        print(f"{ix}|", end="", flush=True)
        df_vtp = extract_data_from_vtp(vtp_i, field_type, l_attrib_sel)

        t_search = re.findall(".*_t(\d\d).vtp", str(vtp_i))
        d_vtp_t[vtp_i] = int(t_search[0]) if len(t_search) else ix + 1
        df_vtp['t'] = d_vtp_t[vtp_i]

        if add_tc_label:
            df_vtp['tc_label_vtp'] = 'tc label not found in path'
            for part_i in vtp_i.parts:
                query = re.findall(".*\d\d\d\d", part_i)
                if query:
                    df_vtp['tc_label_vtp'] = part_i

        l_df.append(df_vtp)

    df_data = pd.concat(l_df)

    if add_index:
        df_data.reset_index(inplace=True)
        df_data.rename({'index': 'triangleIdx' if field_type == 'CELL' else 'pointIdx'}, axis=1, inplace=True)

    return df_data, d_vtp_t


def extract_cells(poly_in, output_folder, write_vtp_files=True, write_stl_files=False, stl_scaling=1.0e6, verbose=False):
    d_parentID_VTP = extract_parentID_selection_from_VTP(poly_in, verbose=False)

    d_cellName_poly = {}

    for parent_ID_i, vtp_i in d_parentID_VTP.items():
        l_cell_name = list_unique_values(
            vtp_i, attribute='cellName', field_type="CELL", repr=True)
        if l_cell_name:
            cell_name = l_cell_name[0]
        else:
            # fallback naming via parentid
            cell_name = f"cell_parent{str(parent_ID_i).zfill(3)}"
        if output_folder:
            if write_vtp_files:
                write_vtp_file(vtp_i, output_folder / f"{cell_name}.vtp")
                if verbose:
                    print(f"VTP file written to output {cell_name}.vtp", flush=True)
            if write_stl_files:
                try:
                    write_stl_file(vtp_i, output_folder / f"{cell_name}.stl", scale=stl_scaling)
                    if verbose:
                        print(f"STL file written to output {cell_name}.stl", flush=True)
                except:
                    print(traceback.print_exc())
        else:
            d_cellName_poly[cell_name] = vtp_i

    if not output_folder:
        return d_cellName_poly
    else:
        return


def pymeshfix_repair(tmh=None, vertices=None, faces=None):
    """ if a tmh is given, a repaired trimesh will be outputted.
    Alternatively, pass vertices and faces and get back a repaired VTK poly
    Update : this in memory repair does not seem to work. Weird given that _clean_from_file
    does work.  Use pymeshfix_repair_steps instead."""
    if tmh:
        meshfix = pymeshfix.MeshFix(tmh.vertices, tmh.faces)
    else:
        meshfix = pymeshfix.MeshFix(vertices, faces)

    meshfix.repair()
    
    if tmh:
        tmh.vertices = meshfix.v
        tmh.faces = meshfix.f
        return tmh
    else:
        return meshfix.mesh


def pymeshfix_repair_steps(tmh=None, vertices=None, faces=None):
    """ The standard in-memory repair  (pymeshfix_repair) does not work often (filters out all but a few faces.
    Thereforethis method was constructed that only performs the repair steps that seem to work (trial and error)
    It accesses the Cython modules directly
    https://pymeshfix.pyvista.org/_autosummary/pymeshfix.PyTMesh.html"""

    meshfix = _meshfix.PyTMesh()
    if tmh:
        meshfix.load_array(tmh.vertices, tmh.faces)
    else:
        meshfix.load_array(vertices, faces)

    #***Remove all but the largest isolated component from the mesh before beginning the repair process
    # meshfix.remove_smallest_components() # destroys mesh !

    # ***this works, no problems
    meshfix.fill_small_boundaries() 

    # ***Attempts to join nearby open components.
    # works, but prohibitivly long runtime 
    # meshfix.join_closest_components()
    

    vclean, fclean = meshfix.return_arrays()
    if tmh:
        tmh.vertices = vclean
        tmh.faces = fclean
        return tmh
    else:
        return meshfix.mesh


def tmh_to_vtp(tmh_in, connectivity=3):
    a_connectivity = np.zeros((tmh_in.faces.shape[0],), dtype=int)
    a_connectivity.fill(connectivity)
    a_faces = np.vstack([a_connectivity, np.array(tmh_in.faces.T)]).T
    return pv.PolyData(tmh_in.vertices, a_faces)


def agg_stl_into_tmh(p_stl_folder, pid_from_filename=True):
    """ given a folder of stl files, a combined trimesh will be constructed, together with an array of parent ids that can be used later on to enrich the POINT data"""
    l_pid = []
    l_tmh = []
    for ix, mesh_i in enumerate(p_stl_folder.rglob("*.stl")):
        # print(ix, Path(mesh_i).stem)
        pid = int(re.findall('[0-9]+',Path(mesh_i).stem)[0]) if pid_from_filename else ix
        tmh = trimesh.load_mesh(mesh_i, process=False) 
        a_pid = np.zeros((tmh.vertices.shape[0],),dtype=int)
        a_pid.fill(pid)
        l_pid.append(a_pid)
        l_tmh.append(tmh)
    
    return concatenate(l_tmh), np.concatenate(l_pid, axis=0)


def getContactId(poly, contactRange = 600e-9, columnName='parentIndexCanonical'):
    ''' contactRange should be set like 2 * h_eff (600nm)
    Keep in mind : pyvista's cell_data_to_point_data() maps via interpolation. 
    If interpolation is not ok, you can use a mapper (map nodes to triangle), which randomly assigns to a triangle in case of ambiguity '''
    poly = pv.wrap(poly)
    kdt = KDTree(poly.points)
    parentIdxNodes = pv.get_array(poly.cell_data_to_point_data(), columnName, preference='point', err=False)
    contactParentIdx = []
    contactId = []
    for ixRow, row in enumerate(kdt.query_ball_tree(kdt, r=contactRange)):
        contactParentIdxRow = parentIdxNodes[row]
        parentIdSelf = parentIdxNodes[ixRow]
        checkOther = contactParentIdxRow != parentIdSelf
        if not np.any(checkOther):
            contactId.append(parentIdSelf)
        else:
            contactId.append(contactParentIdxRow[checkOther][0])  # pick first contact
        contactParentIdx.append(contactParentIdxRow)
        
    return contactId


def getTriangles(poly):
    return pv.wrap(poly).faces.reshape((-1, 4))[:, 1:4]


def mapNodeDataToCellData(poly, column='contactIdCanonical', enrichVtp=True):
    ''' maps node data to cell data, taking the most occuring value (NOT via interpolation)
    for interpolation use pyvista point_to_cell_data()'''
    poly = pv.wrap(poly)
    dataPoints = pv.get_array(poly, column, preference='point', err=False)
    triangles = getTriangles(poly)
    dataCellsTri = np.array([dataPoints[i] for i in triangles.flatten()]).reshape(triangles.shape)
    dataCells = np.median(dataCellsTri,axis=1)  # pick most occuring
    if enrichVtp:
        addArray(poly, dataCells, column, fieldType='CELL')
    else:
        return dataCells


