'''
Created on 28 Mar 2019

@author: wimth
'''

from param_XML import Param_xml
import numpy as np
from helper_functions import slice_tif, convert_16bit_to_RGB, rescale_validation_df
from datamodel import Stack, Featuretracker, Cell4D, Label, Param
from io import StringIO
import pandas as pd
import os
import shutil
import time
import sys
from skimage.exposure import adjust_gamma
from fileHandler import FileHandler
import logging


def read_parms():

    param_xml = Param_xml.get_param_xml(
        sys.argv, l_main_keys=['body'], verbose=False)
    param_xml.add_prms_to_dict(l_keys=['RAM'])
    param_xml.add_prms_to_dict(l_keys=['spheresDT'])

    prm = param_xml.prm
    prm['l_stack_raw'] = param_xml.get_value(
        'l_stack_nb', ['body', 'preprocessing', 'flowcontrol'], use_main_keys=False)

    prm['use_exterior_overlay'] = True
    prm['validate_cells'] = True if prm['validation_file'] else False
    prm['SHOW_CELLS_ONLY'] = False if prm['validate_cells'] else True

    prm['offset_input_img'] = prm['nb_stack_prepro'] - \
        prm['nb_stack_input'] if prm['offset_input_img'] else 0
    prm['offset_exterior_mask'] = prm['nb_stack_prepro'] - \
        prm['nb_stack_input'] if prm['offset_exterior_mask'] else 0
    # if 'membranes_annotated' in prm['membrane_file']:
    #   prm['l_stack_nb'] = list (range(1, len(prm['l_stack_raw']) + 1))

    prm['verbose'] = False
    prm['DBG'] = False


    return param_xml


def spheres_DT(param_xml, fh):

    def initialize_class_variables():

        if prm['verbose']:
            print(
                "#initialize_class_variables>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

        Param(param_xml, filehandler=fh)
        Stack.l_RGBA_cellview = []
        Label.create_indicator_labels()

        return

    def append_summary_of_stack_txt_repr(stack):
        if not stack:
            return
        if prm['verbose']:
            print("###saving a txt-summary of the stack repr (append)")

        output = StringIO()
        stack.repr(detail_cells=True, output=output)

        fh.d_save_info['f_name'] = 'debug_logging_repr'
        fh.save_data(output.getvalue(), file_ext='txt', verbose=False)

        return

    def append_summary_of_4Dcell_txt_repr():
        if prm['verbose']:
            print("###saving a txt-summary of the 4D repr (append)")

        output = StringIO()
        for cell4D in Cell4D.l_cell4D:
            cell4D.repr(output=output)

        fh.d_save_info['f_name'] = 'summary'
        fh.save_data(output.getvalue(), file_ext='txt', verbose=False)

        return

    def append_labelling_chronology_to_txt_log():
        if prm['verbose']:
            print("###saving the labelling chronology to the txt summary (append)")

        output = StringIO()
        for label, l_labelling_log in feat_tracker.d_label_events.items():
            print('', file=output)
            print(
                'Start labelling_log for ',
                label,
                ' **********',
                file=output)
            for ix, event in enumerate(l_labelling_log):
                print(str(ix + 1), '->', event, file=output)

        fh.d_save_info['f_name'] = 'summary'
        fh.save_data(output.getvalue(), file_ext='txt', verbose=False)

        return

    def add_exterior_mask():
        if not prm['use_exterior_overlay']:
            return
        if prm['verbose']:
            print("###add a mask to shield off the exterior")
        # fh.d_load_info['load_dir'] = prm['DIR_EXTERIOR_MASK'] if prm['DIR_EXTERIOR_MASK'] else os.path.join(fh.get_root_save_location(),'002_preprocessing')

        if prm['exterior_mask_file']:
            fh.d_load_info['f_name'] = prm['exterior_mask_file']
        else:
            fh.d_load_info['f_name'] = os.path.join(
                fh.get_root_save_location(), '002_preprocessing', 'exterior_mask.tif')

        if os.path.exists(fh.d_load_info['f_name']):
            stack.add_exterior_mask(
                slice_tif(
                    filehandler=fh,
                    ix_slice_time=[
                        nb_stack -
                        1 +
                        prm['offset_exterior_mask'],
                        nb_stack +
                        prm['offset_exterior_mask']],
                    verbose=False),
                mask=True)
        else:
            print(
                'No exterior mask used.  File does not exist: ',
                fh.d_load_info['f_name'])

        return

    def create_stack():
        if prm['verbose']:
            print(
                "##create a stack ----------------------------------------------------------------------------timestep{0}".format(nb_stack))
        img = slice_tif(
            na_tif=a_4D_membrane,
            ix_slice_time=[
                nb_stack -
                1 +
                prm['offset_input_img'],
                nb_stack +
                prm['offset_input_img']],
            verbose=False)
        img_bin = np.zeros(img.shape, 'bool')
        img_bin[img > 0] = True
        stack = Stack(
            img=img_bin,
            timestep=nb_stack,
            feat_tracker=feat_tracker,
            name=fh.d_load_info['f_name'])

        return stack

    def read_validation_nuclei():
        if not prm['validate_cells']:
            return None
        if prm['verbose']:
            print("#load the Cell nuclei for validation")
        df_val = pd.read_csv(prm['validation_file'], delimiter='\t')

        ufl_rescaled = True if (
            prm.get('img_raw_file_rescaled') and prm['rescale_validation_file']) else False
        if ufl_rescaled:
            df_val = rescale_validation_df(
                df_val,
                old_res=prm['scaling_ZYX'],
                new_res=prm['scaling_ZYX_rescaled'])
        else:
            df_val["x"] = pd.to_numeric(df_val["x"], downcast='integer')
            df_val["y"] = pd.to_numeric(df_val["y"], downcast='integer')
            df_val["z"] = pd.to_numeric(df_val["z"], downcast='integer')

        return df_val

    def finish_last_stack(stack):
        if prm['verbose']:
            print('##finishing last stack')

        if prm['validate_cells']:
            stack.validate_clusters(df_nuclei, verbose=prm['verbose'])
        feat_tracker.update_features_v2(stack, verbose=prm['verbose'])

        stack.construct_clusterview(
            fh, temp_folder=True, verbose=prm['verbose'])

        stack.update_lineage_names_cell_division(verbose=prm['verbose'])
        stack.write_excel_summary_3D(fh, verbose=prm['verbose'])
        if prm['validate_cells']:
            stack.write_excel_validation(
                fh, print_total=True, verbose=prm['verbose'])

        if prm['DBG']:
            append_summary_of_stack_txt_repr(stack)

        return

    def get_extra_channel():

        fh.d_load_info['f_name'] = str(
            param_xml.get_file_info(
                field='img_raw_file'))
        fh.d_save_info['pre_f_name'] = ''
        try:
            a_extra_channel = convert_16bit_to_RGB(
                fh.load_tif(), clip_not_scale=True)
        except BaseException:
            return None

        if len(a_extra_channel.shape) == 4:  # turn stack into timelapse
            z, y, x, rgba = a_extra_channel.shape
            a_extra_channel = a_extra_channel.reshape((1, z, y, x, rgba))

        if len(prm['l_stack_raw']) == 0:
            a_slice_tif = slice_tif(na_tif=a_extra_channel,
                                    ix_slice_time=[prm['l_stack_nb'][0] - 1,
                                                   prm['l_stack_nb'][-1]],
                                    verbose=False,
                                    RGB=True,
                                    trim_1D=False)
        else:
            a_slice_tif = slice_tif(
                na_tif=a_extra_channel,
                ix_slice_time=[
                    i - 1 for i in prm['l_stack_raw']],
                verbose=False,
                RGB=True,
                trim_1D=False)

        make_brighter = True  # causes memory crash for big timelapses,so go stack by stack
        if make_brighter:
            for ix, i in enumerate(a_slice_tif):
                a_slice_tif[ix, :] = adjust_gamma(i, gamma=0.5)

        return a_slice_tif

    def load_membrane_data():

        # fh.d_load_info['load_dir'] = prm['DIR_MEMBRANE'] if prm['DIR_MEMBRANE'] else os.path.join(fh.get_root_save_location(),'002_preprocessing')
        # fh.d_load_info['f_name'] = prm['membrane_file'] if prm['membrane_file'] else  os.path.join(fh.get_root_save_location(),'002_preprocessing','membranes.tif')
        fh.d_load_info['f_name'] = prm['membrane_file'] if prm['membrane_file'] else param_xml.get_value(
            'file_membranes', l_path_keys=['body', 'preprocessing', 'paths', 'output_folder'], use_main_keys=False)
        a_4D = fh.load_tif(verbose=prm['verbose'])
        if len(a_4D.shape) == 3:
            a_4D = a_4D.reshape(1, *a_4D.shape)

        return a_4D

    def remove_temp_files():
        fh.extend_save_info(
            extra_dir_1='003_spheresDT',
            extra_dir_2='temp',
            from_root=True)
        shutil.rmtree(fh.get_save_location())

        return

    ########################################################################
    tic = time.time()
    prm = param_xml.prm

    # fill in general load and save info
    fh.extend_save_info(extra_dir_1='003_spheresDT', from_root=True)
    fh.clear_save_dir()
    logging.basicConfig(
        filename=prm['output_folder'] /
        'chronology.log',
        level=logging.DEBUG,
        format='%(asctime)s %(message)s',
        datefmt='%m/%d/%Y %I:%M:%S %p')
    logging.info('Start')

    initialize_class_variables()

    a_4D_membrane = load_membrane_data()
    Param.set_img_dimensions(a_4D_membrane)

    nb_inconsistencies = 0

    df_nuclei = read_validation_nuclei()

    feat_tracker = Featuretracker(nb_timesteps=a_4D_membrane.shape[0])

    if prm['l_stack_nb'] is None or len(prm['l_stack_nb']) == 0:
        prm['l_stack_nb'] = range(1, len(a_4D_membrane) + 1)

    for nb_stack in prm['l_stack_nb']:
        print("Processing stack {}".format(nb_stack))
        logging.info("Processing stack {}".format(nb_stack))
        # read/initialize---------------------
        start_time = time.time()
        fh.d_save_info['nb_stack'] = nb_stack
        stack = create_stack()
        prev_stack = Stack.prev_stack
        add_exterior_mask()
        if Param.nb_cells_first_stack:
            Param.set_radii_thresholds(logging)
        stack.add_distance_transform(verbose=prm['verbose'])
        ic_GT_corr = True if (
            (not prm['l_stack_nb_GT'] or (
                nb_stack +
                prm['validation_start_time'] -
                1) in prm['l_stack_nb_GT'])) else False

        # SPHERES/CLUSTERS---------------------
        # seeding
        if ic_GT_corr:
            stack.seed_with_validation_file(
                df_nuclei,
                prm['validation_start_time'],
                verbose=prm['verbose'])
        elif prev_stack:
            stack.seed_with_cells_from_prev_stack(verbose=prm['verbose'])

        # clustering
        stack.put_spheres_in_distance_transform(verbose=prm['verbose'])

        # stack.reset_seeds(func=prm['reset_seeds'])

        # labeling
        if (not prev_stack) or (not prm['label_passing']):
            stack.label_clusters_of_first_stack(
                ic_GT_corr, verbose=prm['verbose'])
        else:
            stack.label_clusters_via_prev_stack_information(
                verbose=prm['verbose'])

        # filtering
        stack.filter_clusters(ic_GT_corr, verbose=prm['verbose'])
        # Cluster.print_stats("after filtering")

        # CELLS---------------------------------
        stack.create_cells_based_on_cluster_labels(verbose=prm['verbose'])

        if prev_stack:
            if ic_GT_corr:
                stack.create_cells_through_cell_divisions_GT_corr(
                    verbose=prm['verbose'])
                # Cluster.print_stats("after GT cell division creation")
            else:
                stack.create_cells_through_cell_divisions(
                    verbose=prm['verbose'])

        stack.filter_cells(verbose=prm['verbose'])

        # CELL4D--------------------------------
        stack.create_cell4D(verbose=prm['verbose'])
        stack.update_cell4D(verbose=prm['verbose'])
        stack.filter_cell4D(verbose=prm['verbose'])

        # Finalize previous stack-------------------------------
        if prev_stack:
            # check datamodel before writing output
            nb_inconsistencies += prev_stack.check_consistency_datamodel(
                verbose=prm['verbose'])

            # validation
            if prm['validate_cells']:
                prev_stack.validate_clusters(
                    df_nuclei, prm['validation_start_time'], verbose=prm['verbose'])
                prev_stack.write_excel_validation(fh, verbose=prm['verbose'])

            # determine the lineage_names for outputting
            prev_stack.update_lineage_names_cell_division(
                verbose=prm['verbose'])

            # output
            # prev_stack.construct_RGBA_clusterview(fh,temp_folder=True,
            #     show_cells_only=prm['SHOW_CELLS_ONLY'],df_validation = df_nuclei,
            #     validation_start_time = prm['validation_start_time'],
            #     verbose=prm['verbose'])
            prev_stack.construct_clusterview(
                fh, temp_folder=True, verbose=prm['verbose'])
            prev_stack.write_excel_summary_3D(fh, verbose=prm['verbose'])
            feat_tracker.update_features_v2(prev_stack, verbose=prm['verbose'])
            if prm['DBG']:
                append_summary_of_stack_txt_repr(prev_stack)
        else:
            stack.write_excel_summary_3D(
                fh, initialize=True, verbose=prm['verbose'])

        if prm['verbose']:
            print('elapsed time for stack =', time.time() - start_time)

        # CHECKS
        Stack.check_memory_allocation(verbose=prm['verbose'])
    # <<<end of stack loop

    finish_last_stack(Stack.curr_stack)

    if prm['DBG']:
        append_summary_of_4Dcell_txt_repr()
        append_labelling_chronology_to_txt_log()
    feat_tracker.write_excel_summary_4D_v2(fh)
    # Stack.save_4D_RGBA_clusterview(fh,nb_channels=1,extra_channel=get_extra_channel())
    Stack.agg_temp_viz_files(fh, search_string='raw_clusterview')
    Stack.agg_temp_viz_files(fh, search_string='labelview')
    # Stack.save_4D_clusterview(fh)

    Stack.init_class_variables()
    if prm['DBG']:
        print('{0} datamodel inconsistencies found across all stacks'.format(
            nb_inconsistencies))

    remove_temp_files()

    toc = time.time()
    if prm['verbose']:
        print('runtime_spheresDT = ', toc - tic)

    return


if __name__ == '__main__':
    param_xml = read_parms()
    fh = FileHandler.init_fh(param_xml.prm)
    spheres_DT(param_xml, fh)
