"""
Created on 08 January 2021
post processing of CGAL skin meshing : convert off-files into stl, remesh and repair
@author: wimth
"""


import os
import sys
import trimesh
from pymeshfix import _meshfix
from pyacvd import clustering
import pyvista
from VTK_utils import write_stl_file
from param_XML import Param_xml
import time
import numpy as np
from joblib import Parallel, delayed


def read_parms():
    param_xml = Param_xml.get_param_xml(
        sys.argv, l_main_keys=['body'], verbose=False)
    param_xml.add_prms_to_dict(l_keys=['sphere_meshing'])
    param_xml.add_prms_to_dict(l_keys=['RAM'])
    param_xml.prm['min_nb_nodes'] = 100
    param_xml.prm['max_nb_nodes'] = 1200
    param_xml.prm['remesh'] = True
    return param_xml.prm


def _get_nb_nodes(prm, surface_area):

    if prm['nb_nodes_cell'] == -1:
        nb_nodes = int(surface_area * prm['surface_to_nodes_factor'])
        if nb_nodes < 10:
            return None
    else:
        nb_nodes = prm['nb_nodes_cell']

    return np.clip(nb_nodes, prm['min_nb_nodes'], prm['max_nb_nodes'])


def repair_and_remesh_off_mesh(path, mesh_ctr=None, verbose=False):

    sysout = ' post processing mesh : {}'.format(path.name)
    f_out = prm['output_folder_meshes'] / (path.stem + ".stl")

    # convert off to stl
    d_path_mesh[path] = trimesh.load_mesh(str(path), process=False)
    surface_area = d_path_mesh[path].area
    sysout += f" -> converted to stl"

    # remesh
    mesh = pyvista.wrap(d_path_mesh[path])  # needs pyvista 0.27.x
    if prm['remesh']:
        clus = clustering.Clustering(mesh)  # wrap gives VTK polydata
        clus.subdivide(3)

        nb_nodes = _get_nb_nodes(prm, surface_area)
        if nb_nodes is None:
            print(
                f" -> <ERROR> Skipping cell {path.stem} because the number of determined nodes is too small. Check this mesh")
            # continue
            return

        clus.cluster(nb_nodes)
        mesh = clus.create_mesh()
        sysout += f" -> remeshed (#nodes={nb_nodes if nb_nodes != prm['min_nb_nodes'] else str(prm['min_nb_nodes']) + '!'})"

    write_stl_file(mesh, f_out)

    # repair
    try:
        _meshfix.clean_from_file(str(f_out),
                                 str(f_out))   # repairing, just in case
        sysout += ' -> repaired'.format(path.name)

        # in very rare ocassions the mesh normals are pointing inwards (spot by
        # negative volume)
        tmh_mesh = trimesh.load_mesh(str(f_out), process=False)
        if tmh_mesh.volume < 0:
            sysout += " -> fix inversion"
            trimesh.repair.fix_inversion(tmh_mesh)
            tmh_mesh.export(str(f_out), file_type='stl')
    except BaseException:
        print(
            'something went wrong with _meshfix on {0}. continue'.format(f_name))

    if verbose:
        if mesh_ctr:
            sysout += f"(#mesh={mesh_ctr})"
        print(sysout, flush=True)

    return


if __name__ == '__main__':
    tic = time.time()
    prm = read_parms()

    d_path_mesh = {}
    for f in prm['output_folder_meshes'].glob(
            '*.stl'):  # clear outputfolder stl's
        os.remove(str(f))

    Parallel(n_jobs=prm.get('nb_cores', 1))(delayed(repair_and_remesh_off_mesh)(path, mesh_ctr=ix + 1, verbose=True)
                                            for ix, path in enumerate(prm['output_folder_meshes'].glob('*.off')))

    toc = time.time()
    print('runtime_sphere_meshing = ', toc - tic)
