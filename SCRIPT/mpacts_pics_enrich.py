'''
Created on 29 dec 2020
enrich mpacts-pics segmentation with contact info and cell id's
@author: wimth
'''
import numpy as np
import pandas as pd
import sys
import os
import re
import matplotlib
import matplotlib.pyplot as plt
from param_XML import Param_xml
import traceback

from id_cells import identify
from VTK_utils import (read_vtp_file, write_vtp_file,add_array,get_data_array,extract_parentID_selection_from_VTP,poly_to_trimesh, add_array_with_mapper, map_nodes_to_triangle)

def read_parms():
    verbose = True

    param_xml = Param_xml.get_param_xml(
        sys.argv, l_main_keys=['body'], verbose=False)
    param_xml.add_prms_to_dict(l_keys=['mpacts_pics_enrich'])
    param_xml.add_prms_to_dict(l_keys=['RAM'])

    prm = param_xml.prm
    prm['zres'], prm['yres'], prm['xres'] = param_xml.get_file_info(
        field='scaling_ZYX')

    prm['nb_stack'] = param_xml.get_value(
        'nb_stack_prepro', ['RAM', 'nb_stack'])
    prm['verbose'] = verbose
    
    return param_xml, prm


def _store_ts(param_xml, prm):
    param_xml.store_value(s_parm='timestamp_mesh', value_to_store=prm['timestamp'],
                          l_path_keys=["body", "RAM"], persist=True, verbose=False)
    return


def load_lineage_data(prm, verbose=False):
    # read cell names
    df_cell_names = pd.read_csv(prm['input_file_lineage'], delimiter="\t")

    if 'time' in df_cell_names:
        df_cell_names = df_cell_names[df_cell_names.time == prm['nb_stack']]
    if 't' in df_cell_names:
        df_cell_names = df_cell_names[df_cell_names.t == prm['nb_stack']]

    df_cell_names['x'] = prm['xres'] * 1.e-6 * df_cell_names['x']
    df_cell_names['y'] = prm['yres'] * 1.e-6 * df_cell_names['y']
    df_cell_names['z'] = prm['zres'] * 1.e-6 * df_cell_names['z']

    return df_cell_names


def get_canonical_parentid(cell_name_search, df_canonical_parentid):
    cond = df_canonical_parentid['cell'] == cell_name_search
    if cell_name_search == 'empty':
        return 997
    if df_canonical_parentid[cond].index.size == 0:
        return 998
    else:
        return df_canonical_parentid[cond].index[0]


def name_cells_using_lineage(
        poly, df_cell_names, df_canonical_parentid, default_name='parentID', verbose=False):
    """ provide a mapping between parentid and a cell name based on a named lineage tsv file"""
    d_parentid_VTP = extract_parentID_selection_from_VTP(
        poly_embryo, verbose=verbose)
    if default_name == 'parentID':
        d_parentid_cellname = {k: "{}{}".format(
            default_name, str(k).zfill(3)) for k in d_parentid_VTP}
    else:
        d_parentid_cellname = {k: default_name for k in d_parentid_VTP}
    if verbose:
        print(
            f"{prm['input_file_lineage']} used as lineage file =>  df_cell_names = \n{df_cell_names}")

    # from IPython import embed;embed()
    s_canonical_names = set(df_canonical_parentid.cell).difference({'empty'})
    for parentid_i, vtp_i in d_parentid_VTP.items():
        tmh_i = poly_to_trimesh(vtp_i)
        for ix, row_i in enumerate(df_cell_names.iterrows()):
            point_xyz = row_i[1]['x'], row_i[1]['y'], row_i[1]['z']
            # must have shape (1,3)
            if tmh_i.contains(np.array([list(point_xyz)])):
                if verbose:
                    print('mesh {} contains point ix{} = {}'.format(
                        parentid_i, ix, point_xyz))
                if not d_parentid_cellname.get(parentid_i):
                    # by default copy name
                    d_parentid_cellname[parentid_i] = row_i[1]['cell']
                else:
                    if not d_parentid_cellname[parentid_i] in s_canonical_names:
                        d_parentid_cellname[parentid_i] = row_i[1]['cell']
                    else:
                        print(
                            f"Warning : {row_i[1]['cell']} was not used in mesh naming ,because it collided with {d_parentid_cellname[parentid_i] }")

    print(d_parentid_cellname)

    return d_parentid_cellname


def write_mapper_cellname_csv(d_cellname_canonical_parentid):
    """ write a csv to log the mapping from cell name to parentid and canonical parentid"""
    df_parentid_cellname = pd.DataFrame.from_dict(
        d_parentid_cellname, orient='index')
    df_parentid_cellname.reset_index(level=0, inplace=True)
    df_parentid_cellname.columns = ['parentid', 'cell_name']

    df_cellname_canonical_parentid = pd.DataFrame.from_dict(
        d_cellname_canonical_parentid, orient='index')
    df_cellname_canonical_parentid.reset_index(level=0, inplace=True)
    df_cellname_canonical_parentid.columns = [
        'cell_name', 'canonical_parentid']

    df_merge = pd.merge(df_parentid_cellname, df_cellname_canonical_parentid,
                        how='outer', left_on='cell_name', right_on='cell_name', indicator=True)
    df_merge.to_csv(
        str(prm['output_folder'] / "d_parentid_cellname.csv"), header=True, index=False)

    return


def _select_input_file(prm, verbose=False):
    '''Select the embryo to be enriched '''
    vtp_i = None
    if prm['input_file']:
        input_file_vtp = prm['input_folder'] / prm['input_file']
    else:
        input_file_vtp = ""
        l_files = [(vtp_i, int(re.findall(r'\d+', vtp_i.stem)[0]))
                   for vtp_i in prm['input_folder'].glob('Seeding_cells*[0-9]*.vtp')]
        # sorted on file number ascending
        for t_vtp in sorted(l_files, key=lambda t: t[1]):
            vtp_i, nb_vtp = t_vtp
            if prm['input_file_nb']:
                if prm['input_file_nb'] == int(nb_vtp):
                    input_file_vtp = vtp_i
                    break
        if not vtp_i:
            raise NameError(
                f"No vtp input file could be selected ! check {prm['input_folder']} ")
        input_file_vtp = vtp_i

    if verbose:
        print('vtp file {0} will be used for enriching'.format(input_file_vtp)) if input_file_vtp else print(
            'no vtp file found ! at {0}'.format(input_file_vtp))

    return input_file_vtp


def _map_parentIndex(prm):
    '''
     naming_strategy=
        'lineager'
        'micsla':strategy of michiel(needs debugging);
        'fallback'=just use naming 'parentXX';
    Ultimately this should be replaced with automatic naming
    '''
    naming_strategy = prm.get('naming_strategy', 'lineager')

    if naming_strategy == 'lineager' and not os.path.exists(
            prm['input_file_lineage']):
        naming_strategy = 'fallback'

    df_canonical_parentid = pd.DataFrame({})

    if prm['verbose']:
        print(f'Automated naming via {naming_strategy} method:')

    if naming_strategy == 'micsla':
        a_centroids = np.genfromtxt(prm['input_file_centroids'], delimiter=',')
        err, l_names = identify(a_centroids)
        d_parentid_cellname = {ix: x for ix, x in enumerate(l_names)}
        print("\nFound lineage match for {0} cells with score {1}".format(
            len(l_names), err))

    elif naming_strategy == 'fallback':
        a_parentIndex = get_data_array(
            poly_embryo, field_type="CELL", attribute='parentIndex')
        d_parentid_cellname = {i: ("parent" + str(i).zfill(3))
                               for i in np.unique(a_parentIndex)}
    elif naming_strategy == 'lineager':
        df_cell_names = load_lineage_data(prm, verbose=False)
        df_canonical_parentid = pd.read_csv(
            prm['input_file_canonical_tree'], delimiter="\t")
        d_parentid_cellname = name_cells_using_lineage(
            poly_embryo, df_cell_names, df_canonical_parentid, prm['default_name'], False)

    return d_parentid_cellname, df_canonical_parentid


def _add_parentid(poly_embryo, d_parentid_cellname):

    add_array_with_mapper(poly_embryo, 'parentIndex', 'cellName',
                          d_parentid_cellname, default='Unidentified')

    return


def _add_timestamp(poly_embryo, timestamp):

    a_template = get_data_array(
        poly_embryo,
        field_type="CELL",
        attribute='parentIndex')
    a_ts = np.zeros_like(a_template, dtype=float)
    a_ts.fill(timestamp)
    add_array(poly_embryo, a_ts, 'ts_mesh', dtype=float)

    return


def _add_filter_analyse(poly_embryo):

    a_template = get_data_array(
        poly_embryo,
        field_type="CELL",
        attribute='parentIndex')
    a_filter_analyse = np.zeros_like(a_template, dtype=int)
    add_array(poly_embryo, a_filter_analyse, 'filter_analyse', dtype=int)

    return


def _add_canonical_parentid(poly_embryo, df_canonical_parentid):
    if df_canonical_parentid.empty:
        return

    d_cellname_canonical_parentid = {k: get_canonical_parentid(
        k, df_canonical_parentid) for k in d_parentid_cellname.values()}
    add_array_with_mapper(poly_embryo, 'cellName', 'parentIndexCanonical',
                          d_cellname_canonical_parentid, default=999)
    d_parentid_canonical = {
        i: d_cellname_canonical_parentid[j] for i, j in d_parentid_cellname.items()}
    write_mapper_cellname_csv(d_cellname_canonical_parentid)

    return d_parentid_canonical


def _add_contactid(poly_embryo):
    a_parentIndex = get_data_array(
        poly_embryo, field_type="CELL", attribute='parentIndex')
    a_contactIndex = get_data_array(
        poly_embryo, field_type="CELL", attribute='contact_index')
    a_contact_id = np.array([a_parentIndex[i] for i in a_contactIndex])
    # contactindex=0 means no contact => contact_id = parent_id in that case
    a_contact_id = np.where(
        np.isin(a_contactIndex, [0, -1]), a_parentIndex, a_contact_id)
    poly_embryo = add_array(poly_embryo, a_contact_id,
                            "contactid", field_type="CELL")

    return


def _add_lineageid(poly_embryo, input_file_lineageId):
    ''' enrich embryo VTP with lineageid arrays '''
    if not input_file_lineageId:
        return

    df_cell = pd.read_csv(input_file_lineageId)
    nb_levels = 5
    l_parentid = []
    l_l_levels = []
    for ix_row, row_i in df_cell.iterrows():
        l_name_lineage = re.findall("\\d", str(row_i.name_lineage))
        l_name_lineage_levels = []
        for ix_level in range(0, nb_levels):
            l_name_lineage_levels.append(
                l_name_lineage[ix_level] if ix_level < len(l_name_lineage) else '0')
        l_l_levels.append(l_name_lineage_levels)
        l_parentid.append(row_i.parentIndex)

    a_levels = np.transpose(np.char.array(l_l_levels))
    for ix_level in range(0, nb_levels):
        a_lineage_cum = a_lineage_cum + \
            a_levels[ix_level] if ix_level > 0 else a_levels[ix_level]
        add_array_with_mapper(poly_embryo, 'parentIndex', f'lineageid{ix_level}', {
                              k: int(v) for k, v in zip(l_parentid, a_lineage_cum)})

    return


def _add_canonical_contactid(poly_embryo, d_parentid_canonical):
    if not d_parentid_canonical:
        return

    add_array_with_mapper(poly_embryo, 'contactid',
                          'contactidCanonical', d_parentid_canonical)

    return


def _add_node_to_triangle_link(poly_embryo):

    d_node_triangle = map_nodes_to_triangle(poly_embryo)
    add_array(
        poly_embryo,
        np.array(
            list(
                d_node_triangle.values())),
        'triangleIdx',
        field_type="POINT",
        dtype='int')

    return


if __name__ == '__main__':
    matplotlib.use('Agg')
    try:
        param_xml, prm = read_parms()
        _store_ts(param_xml, prm)
        prm['output_folder'].mkdir(parents=True, exist_ok=True)

        input_file_vtp = _select_input_file(prm)
        poly_embryo = read_vtp_file(input_file_vtp)

        d_parentid_cellname, df_canonical_parentid = _map_parentIndex(prm)

        _add_parentid(poly_embryo, d_parentid_cellname)
        d_parentid_canonical = _add_canonical_parentid(
            poly_embryo, df_canonical_parentid)
        _add_contactid(poly_embryo)
        _add_canonical_contactid(poly_embryo, d_parentid_canonical)
        _add_lineageid(poly_embryo, prm['input_file_lineageId'])
        _add_timestamp(poly_embryo, prm['timestamp'])
        _add_filter_analyse(poly_embryo)
        _add_node_to_triangle_link(poly_embryo)

        if 'enriched' in input_file_vtp.stem:
            p_out = input_file_vtp
        else:
            p_out = str(prm['output_folder'] /
                        (input_file_vtp.stem + "_enriched.vtp"))
        write_vtp_file(poly_embryo, p_out)
        if prm['verbose']:
            print("output written : {0}".format(p_out))
    except Exception as e:
        print(f"{e}", flush=True)
        traceback.print_exc()
        exit(1)
