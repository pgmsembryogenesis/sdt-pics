"""
Created on 8 Aug 2019
- composes a csv of spheres that can be used as the input for the meshing (skin surface meshing)
- option to shrink spheres to remove all overlap

@author: wimth
"""

from param_XML import Param_xml
from fileHandler import FileHandler
import re
import sys
import math
from collections import namedtuple
from pathlib import Path
import numpy as np
import pandas as pd
from helper_functions import get_3D_sphere_coordinates, get_stack
from scipy.spatial import distance
from tifffile import TiffFile, imsave
import traceback
import logging


def read_parms():
    param_xml = Param_xml.get_param_xml(
        sys.argv, l_main_keys=['body'], verbose=False)
    param_xml.add_prms_to_dict(l_keys=['RAM'])
    param_xml.add_prms_to_dict(l_keys=['mpacts_input_generator'])
    prm = param_xml.prm

    prm['file_spheres'] = param_xml.get_value(
        'file_spheres', ['mpacts_input_generator', 'paths', 'output_folder'])

    prm['validation_file'] = param_xml.get_value(
        'validation_file', l_path_keys=['spheresDT', 'paths'])

    prm['membrane_file'] = param_xml.get_value(
        'membrane_file', l_path_keys=['spheresDT', 'paths'])
    if not prm['membrane_file']:
        prm['membrane_file'] = param_xml.get_value(
            'file_membranes', l_path_keys=[
                'preprocessing', 'paths', 'output_folder'])

    prm['img_raw'] = param_xml.get_file_info(field='img_raw_file')
    series = TiffFile(prm['img_raw']).series[0]
    prm['zmax'] = series.shape[series.axes.rfind('Z')]
    prm['ymax'] = series.shape[series.axes.rfind('Y')]
    prm['xmax'] = series.shape[series.axes.rfind('X')]
    prm['zres'], prm['yres'], prm['xres'] = param_xml.get_file_info(
        field='scaling_ZYX')

    return param_xml


def mpacts_input_generator(param_xml, fh):

    def get_spheres(s_spheres):
        # return should look like this ['1, [11, 243, 153], 53.0', '16, [14,
        # 288, 164], 34.0']
        l_matches = p.findall(s_spheres)
        return l_matches

    def get_sphere_info(s_sphere):
        # return should look like ['1', '11', '243', '153', '53', '0']
        # print(s_sphere)
        l_matches = p2.findall(s_sphere)
        if len(l_matches) > 5:
            radius = float(l_matches[4]) + (float("0." + str(l_matches[5])))
        else:
            radius = float(l_matches[4])
        return [float(i)
                for i in [l_matches[1], l_matches[2], l_matches[3], radius]]

    def write_csv_output():

        ls_columns = ["cell_label",  # this array determines the order of the columns (besides the names)
                      "x",
                      "y",
                      "z",
                      "radius",
                      "x_micron",
                      "y_micron",
                      "z_micron",
                      "radius_micron"]

        d_df = {}  # the dict that will be used to create the dataframe

        # initialize the ls_columns
        l_cell_label = []
        l_x = []
        l_y = []
        l_z = []
        l_radius = []
        l_x_micron = []
        l_y_micron = []
        l_z_micron = []
        l_radius_micron = []

        if not prm['shrink_spheres']:
            for cell_label, l_nt_spheres in d_cellID_l_nt_spheres.items():
                for nt_sphere in l_nt_spheres:
                    l_cell_label.append(cell_label)
                    l_x.append(int(nt_sphere.x))
                    l_y.append(int(nt_sphere.y))
                    l_z.append(int(nt_sphere.z))
                    l_radius.append(int(nt_sphere.radius))
                    l_x_micron.append(int(nt_sphere.x) * prm['xres'])
                    l_y_micron.append(int(nt_sphere.y) * prm['yres'])
                    l_z_micron.append(int(nt_sphere.z) * prm['zres'])
                    l_radius_micron.append(int(nt_sphere.radius) * prm['xres'])

        else:
            for idS_i, idMesh_i in sorted(
                    Sphere.d_idS_mesh_key.items(), key=lambda kv: kv[1]):
                sphere = Sphere.d_idS_sph[idS_i]

                l_cell_label.append(idMesh_i)
                l_x.append(int(sphere.zyx[2]))
                l_y.append(int(sphere.zyx[1]))
                l_z.append(int(sphere.zyx[0]))
                l_radius.append(float(sphere.radius))
                l_x_micron.append(int(sphere.zyx[2]) * prm['xres'])
                l_y_micron.append(int(sphere.zyx[1]) * prm['yres'])
                l_z_micron.append(int(sphere.zyx[0]) * prm['zres'])
                l_radius_micron.append(float(sphere.radius) * prm['xres'])

        d_df[ls_columns[0]] = l_cell_label
        d_df[ls_columns[1]] = l_x
        d_df[ls_columns[2]] = l_y
        d_df[ls_columns[3]] = l_z
        d_df[ls_columns[4]] = l_radius
        d_df[ls_columns[5]] = l_x_micron
        d_df[ls_columns[6]] = l_y_micron
        d_df[ls_columns[7]] = l_z_micron
        d_df[ls_columns[8]] = l_radius_micron

        # output_path = os.path.join(fh.get_save_location(),'spheres_for_meshing.csv')
        prm['file_spheres'].parent.mkdir(parents=True, exist_ok=True)
        df_spheres = pd.DataFrame(d_df)
        df_spheres.sort_values(by=['cell_label', 'radius'], ascending=[
                               True, False], inplace=True)

        if prm['nb_spheres_per_cell'] > 0:  # select a subset of the biggest spheres
            df_spheres = df_spheres.groupby(['cell_label']).head(
                prm['nb_spheres_per_cell']).reset_index(drop=True)

        df_spheres.to_csv(str(prm['file_spheres']), index=False)
        # fh.store_f_name('spheres_for_meshing',output_path)

        # extra csv for CGAL meshing
        d_label_id = {j: i for i, j in enumerate(
            sorted(list(set(l_cell_label))))}
        # cell label will be interpreted as a float in CGAL meshing (should
        # match parentId)
        d_df[ls_columns[0]] = [d_label_id[i] for i in l_cell_label]
        del d_df[ls_columns[1]]
        del d_df[ls_columns[2]]
        del d_df[ls_columns[3]]
        del d_df[ls_columns[4]]

        df_spheres = pd.DataFrame(d_df)
        if prm['nb_spheres_per_cell'] > 0:  # select a subset of the biggest spheres
            df_spheres = df_spheres.groupby(['cell_label']).head(
                prm['nb_spheres_per_cell']).reset_index(drop=True)
        df_spheres.to_csv(prm['file_spheres'].parent /
                          "spheres_for_meshing_CGAL.csv", index=False)

        if l_cluster_label_processed:
            # remove duplicates and preserve order
            l_cluster_label = list(dict.fromkeys(l_cluster_label_processed))
            df_d_parentid_label = pd.DataFrame({'parentid': [i for i in range(
                len(l_cluster_label))], 'cluster_label': l_cluster_label})
            df_d_parentid_label.to_csv(
                prm['file_spheres'].parent / "d_parentid_label.csv", index=False)

        print('mpacts output written as', str(prm['file_spheres']))

    class Sphere:
        idS = 0
        d_idS_sph = {}
        d_idS_mesh_key = {}
        d_idS_label = {}

        prm = param_xml.prm
        a_labelview = np.zeros(
            (prm['zmax'], prm['ymax'], prm['xmax']), dtype='int')
        # spheres will be sequentially added to these views
        a_sphereview = np.zeros(
            (prm['zmax'], prm['ymax'], prm['xmax']), dtype='int')

        def __init__(self, center_zyx, radius, mesh_key, label):
            Sphere.idS += 1
            self.zyx = center_zyx
            self.radius = radius
            self.mesh_key = mesh_key
            self.label = label
            self.idS = Sphere.idS
            self.l_pixels = None
            self.update_l_pixels()
            Sphere.d_idS_sph[self.idS] = self
            Sphere.d_idS_mesh_key[self.idS] = mesh_key
            Sphere.d_idS_label[self.idS] = label

        def update_l_pixels(self):
            if self.radius > 0:
                self.l_pixels = get_3D_sphere_coordinates(a_coordZYX=self.zyx, radius=self.radius,
                                                          limit_shape=(prm['zmax'], prm['ymax'], prm['xmax']), xy_vs_z=xy_vs_z)

            return

        def update_views(self, verbose=False):
            # only now the current sphere is added to the views
            Sphere.a_sphereview[self.l_pixels] = 0
            Sphere.a_labelview[self.l_pixels] = 0
            if verbose:
                print('__number of pixels overlap sphere ({1}) before = {0}'.format(
                    len(self.l_pixels[0]), self.mesh_key))

            self.update_l_pixels()
            # only now the current sphere is added to the views
            Sphere.a_sphereview[self.l_pixels] = self.idS
            Sphere.a_labelview[self.l_pixels] = self.label
            if verbose:
                print('__number of pixels overlap sphere ({1}) after = {0}'.format(
                    len(self.l_pixels[0]), self.mesh_key))

            return

        def get_sphere_of_max_overlap(self, verbose=False):
            '''
            the sphere ID of the sphere that maximally overlaps with the current sphere is returned
            '''

            bincount = np.bincount(
                Sphere.a_sphereview[self.l_pixels].flatten())
            # not interested in overlap with zero region, so set zero count
            # equal to zero
            bincount[0] = 0
            sph_overlap_max = Sphere.d_idS_sph[np.argmax(bincount)]
            while sph_overlap_max.mesh_key == self.mesh_key:
                bincount[sph_overlap_max.idS] = 0
                sph_overlap_max = Sphere.d_idS_sph[np.argmax(bincount)]
            # if len(bincount) > self.idS:
            #     bincount[self.idS] = 0  # not interested in overlap with self

            if verbose:
                print("---> OVERLAPPING SPHERES DETECTED => nbpix overlap=",
                      np.max(bincount))

            # sanity test
            if sph_overlap_max.mesh_key == self.mesh_key:
                print("WARNING : The two spheres belong to the same mesh {0} !  not good !".format(
                    self.mesh_key))
            if not sph_overlap_max:
                print(
                    'no overlapping sphere from another label found, something is wrong')

            return sph_overlap_max

        def shrink_and_move_spheres(self, overlap_sphere, verbose=False):
            def shrink_radii():
                sum_radii = overlap_sphere.radius + self.radius
                dist = self.calc_dist_spheres(overlap_sphere)
                length_overlap = max(sum_radii - dist, 0)
                # shrinkage radii will account for half the overlap, the other
                # accounted will be covered by translation
                radius_corr = length_overlap / 4

                if verbose:
                    print("-->they overlap with =", length_overlap,
                          "the distance between them=", dist, "")

                if length_overlap == 0:
                    length_overlap = 1
                    # we apply some shrinking to solve rounding errors that can
                    # cause looping
                    radius_corr = 0.25

                self.radius = self.radius - radius_corr
                overlap_sphere.radius = overlap_sphere.radius - radius_corr

                return length_overlap, dist, self.radius

            def set_new_centers(length_overlap, dist, verbose=False):
                '''
                the centers are pushed in opposite directions.  it will land midway the current radius.  so the shrunken and translated sphere will be completely
                contained in the current sphere and pushed towards its own cluster.
                '''
                scale_factor = length_overlap / \
                    (dist * 4)  # dist = normalize vector; centers only need to shift with 1/4 ot the overlap (translation accounts for half of overlap, rest = shrinking)
                # translation_distance = length_overlap / 4
                l_translation = [(i - j) * scale_factor for i,
                                 j in zip(overlap_sphere.zyx, self.zyx)]
                self.zyx = [math.floor(i - j)
                            for i, j in zip(self.zyx, l_translation)]
                overlap_sphere.zyx = [math.floor(
                    i + j) for i, j in zip(overlap_sphere.zyx, l_translation)]

                # sanity checks (temp)
                new_dist = self.calc_dist_spheres(overlap_sphere)
                new_sum_radii = overlap_sphere.radius + self.radius
                new_radius_overlap = new_sum_radii - new_dist
                if verbose:
                    print('test : the new distance between these cells is {0} instead of the old distance {1}.'.format(
                        new_dist, dist))
                if verbose:
                    print('test : the new length_overlap is now sum_radii{0} - new_dist{1} = {2}'.format(
                        new_sum_radii, new_dist, new_radius_overlap))
                if new_radius_overlap < 1e-5:
                    if verbose:
                        print("test : OK no overlap left :-)")

                return

            if verbose:
                print('_INITIAL : overlap_sphere:', overlap_sphere.repr())
            if verbose:
                print('_INITIAL : self:', self.repr())

            length_overlap, dist, radius = shrink_radii()

            if (length_overlap > 0 and dist > 0):
                set_new_centers(length_overlap, dist)  # CORE
                if verbose:
                    print('_UPDATED : overlap_sphere:', overlap_sphere.repr())
                if verbose:
                    print('_UPDATED : self:', self.repr())
                overlap_sphere.update_views(verbose=False)

            return

        def overlap_with_other_cell(self):
            '''
            the cluster label of the sphere that maximally overlaps with the current sphere is returned
            '''
            bincount = np.bincount(Sphere.a_labelview[self.l_pixels].flatten())
            # not interested in overlap with zero region, so set zero count
            # equal to zero
            bincount[0] = 0
            if len(bincount) > self.label:
                # not interested in overlap with own cell
                bincount[self.label] = 0

            # 1 pixel overlap is ignored
            bincount[bincount <= prm['overlap_tolerance']] = 0
            return True if np.argmax(bincount) > 0 else False

        def calc_dist_spheres(self, sphere2):
            a = (self.zyx[0] * xy_vs_z, self.zyx[1], self.zyx[2])
            b = (sphere2.zyx[0] * xy_vs_z, sphere2.zyx[1], sphere2.zyx[2])
            dst = distance.euclidean(a, b)

            return dst

        def repr(self):
            if self.l_pixels:
                return "zyx:{0},radius:{1},mesh_key:{2},idS:{3},nb_pixels:{4}".format(
                    self.zyx, self.radius, self.mesh_key, self.idS, len(self.l_pixels[0]))
            else:
                return "zyx:{0},radius:{1},mesh_key:{2},idS:{3},nb_pixels:EMPTY".format(
                    self.zyx, self.radius, self.mesh_key, self.idS)

    # PART 1 : make a csv of spheres per cell used in meshing
    # -------------------------------------------------------

    prm = param_xml.prm
    # suffix = '_rescaled' if param_xml.file_rescaled() else ''
    # param_xml.store_value(s_parm=f'RAW_IMG_DIM_TZYX{suffix}',value_to_store=list(img_raw.shape)),l_path_keys=["body","RAM"],persist=True,verbose=True)
    # _,prm['zmax'],prm['ymax'],prm['xmax'] = param_xml.get_file_info(field='RAW_IMG_DIM_TZYX')
    LABEL_EXTERIOR = 1
    LABEL_FILTERED = 9998
    LABEL_VALIDATION = 9999
    SKIP_LABEL = [LABEL_FILTERED, LABEL_VALIDATION, LABEL_EXTERIOR]
    # SKIP_LABEL = [LABEL_VALIDATION, LABEL_EXTERIOR]
    xy_vs_z = prm['zres'] / prm['xres']

    # READ IN EXCEL------------------------------------------
    df_ipt = pd.read_csv(str(prm['input_file_csv']), delimiter=',')
    df_ipt['validation_error'] = df_ipt['validation_error'].astype('str')

    # STEP 1 BUILD UP DATASTRUCTURES FROM EXCEL INPUT---------------------
    p = re.compile(r"(?:\()(.+?)(?:\))")
    p2 = re.compile(r"\d+")

    l_cluster_label_processed = []
    if not prm['shrink_spheres']:

        d_cellID_l_nt_spheres = {}  # ix = mesh key
        nt_Sphere = namedtuple('nt_Sphere', ['z', 'y', 'x', 'radius'])

        for _, row_cluster in df_ipt[df_ipt.stack_nb ==
                                     prm['nb_stack']].iterrows():
            if row_cluster.cluster_label in SKIP_LABEL:
                continue
            else:
                l_cluster_label_processed.append(row_cluster.cluster_label)
            mesh_key = "{0}_{1}_t{2}".format("{:02d}".format(
                row_cluster.cluster_label), row_cluster.color, row_cluster.stack_nb)
            print(mesh_key)
            if row_cluster.validation_error != 'nan':
                print(row_cluster.validation_error)  # ter info
            l_spheres = get_spheres(row_cluster['spheres(ctr,centre,radius)'])
            l_nt_spheres = []
            for sphere in l_spheres:
                z, y, x, radius = get_sphere_info(sphere)
                nt_sphere = nt_Sphere(z=z, y=y, x=x, radius=radius)
                l_nt_spheres.append(nt_sphere)

            if l_nt_spheres:
                l_nt_spheres_old = d_cellID_l_nt_spheres.get(mesh_key)
                l_nt_spheres_new = l_nt_spheres_old + \
                    l_nt_spheres if l_nt_spheres_old else l_nt_spheres
                d_cellID_l_nt_spheres[mesh_key] = l_nt_spheres_new

    else:
        dd = {}  # to avoid looping if the same combination keeps popping up

        for _, row_cluster in df_ipt[df_ipt.stack_nb ==
                                     prm['nb_stack']].iterrows():
            if row_cluster.cluster_label in SKIP_LABEL:
                continue
            else:
                l_cluster_label_processed.append(row_cluster.cluster_label)

            mesh_key = "{0}_{1}_t{2}".format("{:02d}".format(
                row_cluster.cluster_label), row_cluster.color, row_cluster.stack_nb)
            print('processing cluster :', mesh_key, flush=True)
            if row_cluster.validation_error != 'nan':
                print(row_cluster.validation_error)  # ter info

            l_spheres = get_spheres(row_cluster['spheres(ctr,centre,radius)'])

            for sphere_i in l_spheres:
                z, y, x, radius = get_sphere_info(sphere_i)
                sph_curr = Sphere([int(z), int(y), int(x)], radius, mesh_key, int(
                    row_cluster.cluster_label))

                while (sph_curr.overlap_with_other_cell()):

                    sph_overlap_max = sph_curr.get_sphere_of_max_overlap()

                    dd.setdefault(sph_curr.idS, []).append(sph_overlap_max.idS)

                    sph_curr.shrink_and_move_spheres(
                        sph_overlap_max, verbose=False)

                    sph_curr.update_l_pixels()
                    if not sph_curr.l_pixels:
                        break

                z, y, x = sph_curr.zyx
                if sph_curr.l_pixels or z < 0:
                    sph_curr.update_views(verbose=False)
                else:
                    print('sphere{} is skipped, no pixels left or negative z coordinate'.format(
                        sph_curr.repr()))

    write_csv_output()

    # PART 2 : make the pixel input for mpacts-PiCS
    # ---------------------------------------------

    ix_stack = prm['nb_stack_prepro'] - \
        1 if str(prm['nb_stack_prepro']).isnumeric() else 0

    img_raw = get_stack(prm['img_raw'], ix_stack)
    print(f"Pixel_selector_type = {prm['pixel_selector_type']}")

    if prm['pixel_selector_type'] == 'preprocessed':
        a_frangi = get_stack(prm['input_file_frangi'], ix_stack)
        a_threshold = get_stack(prm['input_file_threshold'], ix_stack)
        a_membrane = get_stack(prm['membrane_file'], ix_stack)
        a_mpacts_pixel_selector = np.where(a_threshold, a_frangi, 0)
        max_stack = np.max(a_mpacts_pixel_selector)
        for ix_z, a_z in enumerate(a_mpacts_pixel_selector):
            a_mpacts_pixel_selector[ix_z] = np.where(
                a_membrane[ix_z] > 0, max_stack, a_z)

    elif prm['pixel_selector_type'] == 'membrane':
        a_mpacts_pixel_selector = get_stack(prm['membrane_file'], ix_stack)

    elif prm['pixel_selector_type'] == 'frangi':
        a_frangi = get_stack(prm['input_file_frangi'], ix_stack)
        a_threshold = get_stack(prm['input_file_threshold'], ix_stack)
        a_mpacts_pixel_selector = np.where(a_threshold, a_frangi, 0)

    else:  # fallback to 'raw' image
        z, y, x = img_raw.shape
        a_img_flat = img_raw.flatten()

        # this sets the downsampling of the image (1 pixel per kernel_length is
        # selected)
        kernel_length = 10
        a_kernel = np.zeros((kernel_length,))
        a_kernel[0] = 1

        a_padding = np.zeros(
            (kernel_length - (a_img_flat.shape[0] % kernel_length),))
        a_img_flat = np.concatenate((a_img_flat, a_padding))

        a_mpacts_pixel_selector = (a_img_flat.reshape(
            (-1, kernel_length)) * a_kernel).flatten()
        a_mpacts_pixel_selector = a_mpacts_pixel_selector[:-a_padding.shape[0]]
        a_mpacts_pixel_selector = a_mpacts_pixel_selector.reshape((z, y, x))

        img_raw.reshape((z, y, x))

    imsave(str(prm['output_folder'] / "mpacts_pixel_selector.tif"),
           a_mpacts_pixel_selector)
    imsave(str(prm['output_folder'] / "raw_img_selected_stack.tif"), img_raw)

    # PART 3 : select part of validation (will be used for automated cell naming)
    # ------------------------------------------------------
    if isinstance(prm['validation_file'], Path):
        df_val = pd.read_csv(prm['validation_file'], delimiter="\t")
        cond = df_val['time'] == prm['nb_stack_input']
        df_val = df_val[cond]
        df_val.to_csv(prm['output_folder'] /
                      "lineage_named.tsv", sep='\t', index=False)

        select = df_val['cell'].duplicated()  # boolean mask
        if select.any():
            print(
                f"<DATA INCONSISTENCY>! - This(these) cellname(s) appear more than once ! => {df_val[select]['cell']}. Fix the lineage file !")

    # PART 4 : select part of the lineage report for lineageId
    # ------------------------------------------------------
    if isinstance(prm['input_file_lineageId'], Path):
        df_cells = pd.read_csv(prm['input_file_lineageId'])
        df_cell = df_cells[df_cells['stack_nb'] == prm['nb_stack_input']]
        df_cell.to_csv(prm['output_folder'] / "lineageId.csv", index=False)
    return


if __name__ == '__main__':
    try:
        param_xml = read_parms()
        fh = FileHandler.init_fh(param_xml.prm)
        mpacts_input_generator(param_xml, fh)
    except Exception as e:
        logging.error(traceback.format_exc())
        sys.exit(1)
