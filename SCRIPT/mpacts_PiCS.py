'''
Created on 15 May 2019
- adaption mpacts_Celegans.py = original script wim
- python 2.7 +  unix (binaries don't work under python 3, no binaries for windows)

@author: wimth,barts
'''

# IMPORT
from PIL import Image
import numpy as np
import vtk
# import VTK_utils
from VTK_utils import read_vtp_file, extract_selection_vtp, write_vtp_file, get_data_array, add_array_with_mapper
import vtk.numpy_interface.dataset_adapter as dsa
import matplotlib.pyplot as plt
from scipy import ndimage, signal
import os
import shutil
import glob
import math
import sys
import re
import xmltodict
from tifffile import TiffFile, imshow, imsave
from scipy.ndimage import zoom
from param_XML import Param_xml
import logging
import warnings

# IMPORT MPACTS
import mpacts.core.simulation as sim
from mpacts.core.valueproperties import Variable, VariableFunction
from mpacts.core.units import unit_registry as u
import mpacts.core.command as cmd
import mpacts.geometrygenerators.trianglegeometries as trigeo
from mpacts.particles.particlecontainer import ParticleContainer
import mpacts.particles as prt
from mpacts.contact.models.collision.linearforce.linearforce_fl_matrix import FlatLayerAdhesiveTensionFprimIntMatrix
import mpacts.commands.geometry.orientation as orient
import mpacts.geometrygenerators.trianglegeometries as trigeo
import mpacts.geometrygenerators.pointgenerators as poi
from scipy.ndimage import gaussian_filter
import mpacts.geometrygenerators.transformations as transfo
from mpacts.contact.models.continuum.immersedboundary import GridToCell
from mpacts.commands.onarrays.setvalue import SetValueCommand
import DEMutilities.enumerate_properties as ep
import DEMutilities.relaxcontacts as relax
from mpacts.core.arrays import create_array
# from mpacts.core.arrays.ArrayManager import get_array
import mpacts.core.arrays as arr
from mpacts.particles.specialcases import DeformableCell
import mpacts.contact.detectors.multigrid as cdm
from random import random
import mpacts.contact.matrix.conjugategradient as cg
import mpacts.commands.monitors.progress as pp
import mpacts.tools.random_seed as RS
import mpacts.geometrygenerators.polyhedron as polyhedron
from mpacts.io.datasave import DataSaveCommand
import mpacts.io.datareader as read
from mpacts.io.savefiles import save_stl
from mpacts.io.filereaders import read_stl
import mpacts.core.configuration as conf
from mpacts.tools.setcontactdatastorage import SetContactDataStorage
import mpacts.io.vtk_writer as vtkWriter

import mpacts.contact.models.misc.contactcounter as counter
from mpacts.contact.detectors.reuse_existing import ReuseExistingContactDetector
from mpacts.commands.geometry.nodenormals import ComputeNodeNormalsCommand
import convexhull
# from embryosim.esimutils import mpacts_util
# ReuseExistingContactDetector

# remeshing
from mpacts.remeshing import TriangularMeshRemeshingCommand
import mpacts.core.command as cmds
import mpacts.remeshing.particle_decorators as pd
import mpacts.remeshing.particle_managers as pm
import mpacts.remeshing.particles as pt
import mpacts.remeshing.stages as st
from pymeshfix import _meshfix


class VolumeLossPrinter(pp.PrinterBase):
    def __init__(self, pc):
        self.pc = pc

    def PrintProgress(self):
        retstr = 'dvol: '
        retstr += '%3.2f' % ((np.sum(self.pc['volume'][:]) - np.sum(
            self.pc['volume_eq'][:])) / np.sum(self.pc['volume_eq'][:]) * 100)
        retstr += r'%'

        return retstr


def pre_filter(image, sigma, dz_dx_ratio):
    '''
    Gaussian smooth filter with standard deviation sigma. Implementation is numpy so very fast
    '''

    if sigma == 0:
        return image
    image = np.array(image, dtype=float)
    image /= np.max(image)
    image *= 256

    # even smoothing in all directions, so correct for z
    l_sigma = np.array([sigma / dz_dx_ratio, sigma, sigma])

    return np.array(gaussian_filter(image, l_sigma), dtype=float) / \
        256. * np.sqrt(2 * np.pi * sigma**2)**3


def resample(data, to_zoom, dx, dy, dz):
    '''
    Resampling data with given 'zoom' ratios. Used to make it more evenly distributed across dimensions
    '''
    dx /= to_zoom[2]
    dy /= to_zoom[1]
    dz /= to_zoom[0]
    if to_zoom == (1.0, 1.0, 1.0):
        new_data = data
    else:
        # prefilter off. the spline filter messes up the image!
        new_data = zoom(data, to_zoom, order=1, prefilter=False)

    return new_data, dx, dy, dz


def filter_Signal(data, dx, dy, dz, param_xml, calibration_run_ic=False):
    '''
    Ibase : pixel_strength (float)  (array of float)
    xb : list of tuples with coordinates of selected pixels
    '''
    print(" - Filtering and compressing the signal...")

    data = np.array(data, dtype=float)

    # Select only a percentage of strongest pixels
    if pixel_selector_type == 'preprocessed':
        perc_pix_sel = param_xml.get_value('perc_pix_sel', ['parms_pix'])
    else:
        perc_pix_sel = 100
    nb_pix_sel = param_xml.get_value('nb_pix_sel', ['parms_pix'])
    n_keep = nb_pix_sel if nb_pix_sel > 0 else int(
        perc_pix_sel / 100. * data.size)
    n_keep = min(n_keep, np.count_nonzero(data))  # don't pick up zero pixels
    # xb=tuple of 3 arrays of pixel coordinates
    xb = np.unravel_index(data.flatten().argsort()[-n_keep:], data.shape)

    if param_xml.get_value('balance_z_signal', ['parms_pix']):
        mid_z = int(np.round(data.shape[0] / 2))
        print(' - rebalancing z-signal with z reference {0}'.format(mid_z))
        a_max = np.max(data, axis=(1, 2))
        a_max = a_max / a_max[mid_z]
        # rebalancing signal per slice
        data = data / a_max[:, np.newaxis, np.newaxis]

    Ibase = data[xb]

    # tuple of 3 arrays in micron (in x,y,z order)
    xb = tuple(l * r for l, r in zip((xb[2], xb[1], xb[0]), (dx, dy, dz)))
    xb = np.transpose(xb)  # array (count,3)
    xb = list(map(tuple, xb))  # list of 3D-tuples in micron (x,y,z)

    # Compress
    if pixel_selector_type == 'preprocessed':
        def compressor_signal(x):  # compresses data between interval [1,2]
            return np.exp(-((x_max - x) / ((x + 1.e-6) - x_min))) + 1.0
        Ibase = np.log10(Ibase)  # for a more even spread between min and max
        x_max = np.max(Ibase)
        x_min = np.min(Ibase)
        compressor_signal_vec = np.vectorize(compressor_signal)
        Ibase = compressor_signal_vec(Ibase)
    else:
        if pixel_selector_type in ['calibration', 'membrane']:
            Ibase[Ibase > 0] = 1

    return Ibase, xb


def extract_parentID_selection_from_VTP(input_file_vtp, verbose=False):
    '''
    temporary => copy paste from VTK_Utils
    '''
    d_parentID_VTP = {}
    poly_in = read_vtp_file(input_file_vtp)
    a_parent_id = np.unique(dsa.WrapDataObject(
        poly_in).CellData['parentIndex'].__array__())
    for parent_id in a_parent_id:
        if verbose:
            print('--processing cell with parentID {0}'.format(parent_id))
        poly_out = extract_selection_vtp(
            poly_in,
            query="parentIndex == {0}".format(parent_id),
            field_type="CELL")
        d_parentID_VTP[parent_id] = poly_out

    return d_parentID_VTP


def add_cell_to_particle_container(
        a_vertices, a_triangles, pc_cell, cd_kd, mysim):
    p = polyhedron.Polyhedron()
    p.vertices = list(map(tuple, a_vertices))
    # vils = connectivity , shape = (nb_triangles,3), contains point indices
    p.triangles = list(map(tuple, a_triangles))
    con = polyhedron.Connectivity(p)
    Cell = pc_cell.add_particle()

    #verts = transfo.translate(p.vertices, tuple(xcell[i]))
    Cell.nodes.add_and_set_particles(x=p.vertices, v=(0., 0., 0.)
                                     )

    Cell.triangles.add_and_set_particles(vertexIndices=p.triangles, F=(0, 0, 0), contact_area=0., layer_width=cd_kd
                                         )
    Cell.add_connectivity(
        mysim("loop_cmds/contact_cmds/CD_Springs_cells"),
        con.edgeCorners)

    return Cell


def print_simulation_parameters():
    try:
        print('Parameters used : baseVisco={0},pt_cortex={1},CM_cell_cell.w1={2}'.format(
            baseVisco.get_value(), pt_cortex.get_value(), CM_cell_cell.get('w1')))
        print("Internal_pressure  = {}".format(p_int.get_value()))
        print("kv_cell = {}".format(kv_cell.get_value()))
        print("cortical tension = {}".format(pt_cortex.get_value()))
        print("cell adhesion  = {}".format(adh_cell.get_value()))
        print("k_layer  = {}".format(k_layer.get_value()))
        print("image_force_ratio  = {}".format(image_force_ratio.get_value()))
    except Exception as e:
        pass

    return


def add_pixels_to_simulation():
    # cells('triangles').VTKWriterCmd(executeInterval=1, select_all=True, directory=str(output_folder))  #execute interval large (only 1 or 2 vtp snapshots of initial relaxation)
    # Adding the pixels : (This is much much faster as it does no dependency
    # checking...)
    Grid.resize(len(xb))
    Grid['x'].set_array(xb)
    Grid['r'].set_array([0 for _ in xb])
    Grid['Signal'].set_array(list(Ibase))
    Grid['m'].set_array([0. for _ in xb])
    Grid['h'].set_array([rval for _ in xb])
    print("<<Introduced signal grid>>. Total of {} pixels".format(Grid.size()))
    Grid.VTKWriterCmd(
        executeOnce=True,
        select_all=True,
        directory=str(output_folder))  # basegrid output

    return


def scale_hull(scale_factor, xyz_centroid_hull=[0, 0, 0]):
    """ scale using center of hull as the origin"""
    xbf = np.array(hull('controlPoints/xBF'))
    for xi in range(0, 3):
        xbf[:, xi] -= xyz_centroid_hull[xi]
    xbf *= scale_factor
    for xi in range(0, 3):
        xbf[:, xi] += xyz_centroid_hull[xi]
    hull("controlPoints")['xBF'].set_array(list(map(tuple, xbf)))
    return


def add_hull_to_simulation():
    ctrl, vil, xyz_centroid = convexhull.giftwrap(cloud=np.array(cells('nodes/x')) * 1e6,
                                                  ignore_half=True,
                                                  quantile_gut=0.4,  # original 0.4
                                                  divide_remove=5,  # 5 originally
                                                  seedlen=75,
                                                  debug=False)
    ctrl *= 1e-6
    xyz_centroid *= 1e-6
    vil = [el[::-1] for el in vil]  # Normals should point inwards
    thehull.controlPoints.add_and_set_particles(xBF=list(map(tuple, ctrl)))
    thehull.hull.add_and_set_particles(vertexIndices=list(
        map(tuple, vil)), layer_width=10 * erange.get_value())  # orig 5

    if param_xml.get_value('scale_hull_factor', ['process_flow']) != 1:
        # wth: loose fit for smoothing (in conjunction with larger repulsion
        # distance)
        scale_hull(
            param_xml.get_value(
                'scale_hull_factor',
                ['process_flow']),
            xyz_centroid)

    print("Introduced and positioned hull")
    hull.VTKWriterCmd(
        executeInterval=.05,
        select_all=True,
        directory=str(output_folder))

    return


def update_parms_simulation(phase=2):

    p_int_mult.set_value(
        1.e3 *
        param_xml.get_value(
            'p_int_{}'.format(phase),
            ['parms_cell_model']) /
        param_xml.get_value(
            'p_int',
            ['parms_cell_model']))
    kv_cell_mult.set_value(
        1.e3 *
        param_xml.get_value(
            'kv_cell_{}'.format(phase),
            ['parms_cell_model']) /
        param_xml.get_value(
            'kv_cell',
            ['parms_cell_model']))
    pt_cortex_mult.set_value(
        1.e-3 *
        param_xml.get_value(
            'pt_cortex_{}'.format(phase),
            ['parms_cell_model']) /
        param_xml.get_value(
            'pt_cortex',
            ['parms_cell_model']))
    adh_cell_mult.set_value(
        1e-1 *
        param_xml.get_value(
            'adh_cell_{}'.format(phase),
            ['parms_cell_model']) /
        param_xml.get_value(
            'adh_cell',
            ['parms_cell_model']))
    k_layer_mult.set_value(
        param_xml.get_value(
            'k_layer_{}'.format(phase),
            ['parms_cell_model']) /
        param_xml.get_value(
            'k_layer',
            ['parms_cell_model']))

    # if phase in [1]:
    #     k_layer_mult.set_value(1.e12)
    # else:
    #     k_layer_mult.set_value(1.e7)

    return


def get_latest_file():
    """ returns latest Seeding cells file path, and also number """
    l_files = sorted([(int(re.findall("\\d+", str(path.stem))[0]), path.name)
                     for path in output_folder.rglob("Seeding_cells_triangles_[0-9]*.vtp")])

    return l_files[-1][1], l_files[-1][0]


def log_chronology_txt(text=""):

    # l_files = sorted([ (int(re.findall("\d+",str(path.stem))[0]),path.name)  for path in output_folder.rglob("Seeding_cells_triangles_[0-9]*.vtp")])
    logging.info(
        f'{text}, simulation time = {mysim.get_time()}, latest file written = {get_latest_file()[0]}, ')

    return


def get_map_oldix_newix(type='triangles'):
    """ When exporting to vtp the dead nodes or triangles will be filtered out.
    We assume this leads to a simple shift in indices.
    Therefore we need to update the data-arrays that store indices
    This method provides a map for doing this"""

    a_parent = mysim(
        f'cells/{"triangles" if type =="triangles" else "nodes"}/parentIndex')
    a_alive = np.empty(len(a_parent), dtype=bool)

    for j in range(len(a_parent)):
        try:
            if a_parent[j] >= 0:
                a_alive[j] = True
        except RuntimeError:
            a_alive[j] = False

    d_oldix_newix = {
        i: j - 1 for i,
        j in zip(
            range(
                len(a_parent)),
            np.cumsum(a_alive))}
    logging.info(
        f'Mapping indices for {type}. Total nb of {type} = {len(a_parent)}, number of alive = {np.sum(a_alive)}, max index in remapper = {max(d_oldix_newix.values())}')

    return d_oldix_newix, a_alive


def remap_idx(field, d_oldix_newix):
    """ replaces arrays in simulation object via a mapper"""

    if "vertex" in field:
        l_field = np.array(mysim(field).tolist()).flatten().tolist()
    else:
        l_field = mysim(field).tolist()

    a_new = np.array([d_oldix_newix.get(i, i) for i in l_field])

    if "vertexIndices" in field:
        # must be list of 3-tuples of ints (not numpy.int)
        l_new = [(int(i), int(j), int(k)) for i, j, k in a_new.reshape(-1, 3)]
        mysim(field).set_array(l_new)
    else:
        mysim(field).set_array(a_new.tolist())

    return


def engage_cd_counter(mysim, cells, h_eff, CD_cell_cell):
    """Gives some extra arrays in output to investigate contacting triangles"""
    COUNTER = counter.ContactIndexKeeper(
        pc1=cells('triangles'),
        pc2=cells('triangles'),
        contact_range=h_eff)
    CD_COUNTER = ReuseExistingContactDetector("cell-Counter"  # This only exists so the contact indices initialize
                                              , mysim, cmodel=COUNTER, cd=CD_cell_cell
                                              )
    for i in range(len(cells('triangles/contact_index'))):
        try:
            # Default is 0 but this overlaps with triangle index 0
            mysim('cells/triangles/contact_index')[i] = -1
        except RuntimeError:
            pass


if __name__ == "__main__":
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        import time
        tic = time.time()

        param_xml = Param_xml.get_param_xml(
            sys.argv, l_main_keys=[
                'body', 'mpacts_PiCS'], verbose=False)
        docker_run_ic = param_xml.get_value('docker_run_ic', ['paths'])
        input_file_pixel_selector = param_xml.get_value(
            'input_file_pixel_selector', ['paths'], docker_run=docker_run_ic)
        pixel_selector_type = param_xml.get_value(
            'pixel_selector_type', ['process_flow'], docker_run=docker_run_ic)
        output_folder = param_xml.get_value(
            'output_folder', ['paths'], docker_run=docker_run_ic)

        # safety check to prevent disastrous delete
        if output_folder.exists() and '012_mpacts_pics' in output_folder.parts:
            shutil.rmtree(output_folder, ignore_errors=False, onerror=None)
            logging.info(f"{output_folder} is removed")
        output_folder.mkdir(parents=True, exist_ok=False)  #
        logging.basicConfig(
            filename=output_folder /
            'chronology.log',
            level=logging.DEBUG,
            format='%(asctime)s %(message)s',
            datefmt='%m/%d/%Y %I:%M:%S %p')
        logging.info('Start')

        if input_file_pixel_selector:
            print(str(input_file_pixel_selector), " is used to extract pixels")
            with TiffFile(input_file_pixel_selector) as tif:
                a_pixel_selector = tif.asarray()
            if len(a_pixel_selector.shape) == 4:
                a_pixel_selector = a_pixel_selector.reshape(
                    a_pixel_selector.shape[1:])

            calibration_run_ic = param_xml.get_value(
                'calibration_run_ic', ['process_flow'])

            # FILTER SIGNAL
            dz, dy, dx = [
                i * 1e-6 for i in param_xml.get_file_info(field='scaling_ZYX')]
            print(
                " - pixels converted to micron using dx=",
                dx,
                "dy=",
                dy,
                "dz=",
                dz)

            a_pixel_selector = pre_filter(
                a_pixel_selector,
                sigma=param_xml.get_value(
                    'sigma_smoothing',
                    ['parms_pix']),
                dz_dx_ratio=dz /
                dx)  # smoothing
            a_pixel_selector, dx, dy, dz = resample(
                a_pixel_selector, tuple(
                    param_xml.get_value(
                        'zoom_factors', ['parms_pix'])), dx, dy, dz)  # resampling
            Ibase, xb = filter_Signal(
                a_pixel_selector, dx, dy, dz, param_xml, calibration_run_ic)  # filtering + scaling

            (nz, ny, nx) = a_pixel_selector.shape
            print(
                " - before correction=%d, after=%d" %
                ((nz * (nx) * (ny)), Ibase.size))

        SetContactDataStorage("Vector")

        # SETTING UP SIMULATION------------------------------------------------
        time_mult = param_xml.get_value('simulation_time_1',
                                        ['parms_sim']) + param_xml.get_value('simulation_time_2',
                                                                             ['parms_sim']) + param_xml.get_value('simulation_time_3',
                                                                                                                  ['parms_sim'])
        dtg = 5e-3  # timestep
        mysim = sim.Simulation("Seeding", timestep=dtg)
        params = mysim('params')  # Creating lib with settings

        # calibrating the image force to the number of pixels
        if input_file_pixel_selector:
            image_force_ratio = param_xml.get_value(
                'image_force_ratio', ['parms_pix'])
            image_force_ratio_mult = Variable(
                "image_force_ratio_mult", params, value=1)
            if pixel_selector_type == 'raw':
                # How much does image contribute compared to mechanics
                image_force_ratio = Variable(
                    "image_force_ratio", params, value=image_force_ratio)
            else:
                image_force_ratio_scaled = image_force_ratio / \
                    np.average(Ibase)
                print(
                    " - The image_force_ratio from parameterfile = {0} and downscaled by the average pixel intensity to {1}".format(
                        image_force_ratio,
                        image_force_ratio_scaled))
                print(" - total force of the pixels now equals ",
                      np.sum(Ibase * image_force_ratio_scaled))
                image_force_ratio = VariableFunction(
                    "image_force_ratio",
                    params,
                    function="{}*$image_force_ratio_mult$".format(image_force_ratio_scaled))

        param_xml.l_main_keys.append('parms_cell_model')
        # Reference value for mechanical tension. This determines base
        # mechanical state. Best to keep unchanged:

        pt_cortex_mult = Variable(
            "pt_cortex_mult", params, value=1 * u("nN/um"))
        pt_cortex = VariableFunction(
            "pt_cortexn", params, function="{}*$pt_cortex_mult$".format(
                param_xml.get_value('pt_cortex')))
        #pt_cortex   = Variable( "pt_cortexn"      , params, value=  param_xml.get_value('pt_cortex')*u('nN/um'))
        function = '$pt_cortexn$'
        pt_cortex_f = VariableFunction(
            "pt_cortex_f", params, function=function)

        kv_cell_mult = Variable("kv_cell_mult", params, value=1 * u("kPa"))
        kv_cell = VariableFunction(
            "kv_celln", params, function="{}*$kv_cell_mult$".format(param_xml.get_value('kv_cell')))
        # kv_cell     = Variable( "kv_celln"        , params, value=
        # param_xml.get_value('kv_cell')*u("kPa"))       #Volume conservation.

        p_int_mult = Variable("p_int_mult", params, value=1 * u("kPa"))
        p_int = VariableFunction(
            "internal_pressure",
            params,
            function="{}*$p_int_mult$".format(
                param_xml.get_value('p_int')))
        # p_int     = Variable( "internal_pressure"        , params, value=
        # param_xml.get_value('internal_pressure')*u("kPa"))       #Balloon
        # force

        # Adhesive range (smaller = stiffer, larger = soft and smooth)
        h_eff = Variable(
            "h_eff",
            params,
            value=param_xml.get_value('h_eff') *
            u('nm'))
        h_eff_pair = Variable(
            "h_eff_pair",
            params,
            value=(
                param_xml.get_value('h_eff') /
                1.3) *
            u('nm'))  # Adhesive range (smaller = stiffer, larger = soft and smooth) previously 10
        # In the quadratic form, the maximal force will be at 1/sqrt(2)*h, so
        # roughly 0.7*h
        h_smooth = Variable(
            "smoothing_length",
            params,
            value=param_xml.get_value('h_smooth') *
            u('um'))  # Smoothing length of image pixels (big -> slow calculation)
        radius = Variable(
            "radius",
            params,
            value=param_xml.get_value('radius') *
            u('um'))  # Average cell radius
        max_r = Variable(
            "max_inv_curv",
            params,
            value=param_xml.get_value('max_r') *
            u('um'))
        # Tolerance for conjugate gradient solver (large=fast and imprecise)
        opt_limit = Variable(
            "cg_opt_limit",
            params,
            value=param_xml.get_value('opt_limit'))

        FrictionN = Variable("FrictionN", params, value=param_xml.get_value(
            'FrictionN') * u('kPa*s/um'))  # Normal contact friction
        FrictionT = Variable("FrictionT", params, value=param_xml.get_value(
            'FrictionT') * u('kPa*s/um'))  # Tangential contact friction
        viscosity = Variable("viscosity", params, value=param_xml.get_value(
            'viscosity') * u('kPa*s'))  # Diagonal (liquid) viscosity: high = stable
        baseVisco = Variable("baseVisco", params, value=param_xml.get_value(
            'baseVisco') * u('kPa*s'))  # In-plane cortex viscosity
        tc = Variable("tc", params, value=param_xml.get_value(
            'tc') * u('um'))  # Cortex thickness (typically 250 nm)
        # As inconsequential as parameters can be...
        Vpoiss = Variable(
            "Vpoiss",
            params,
            value=param_xml.get_value('Vpoiss'))

        function = str(param_xml.get_value('gamma_liq')) + \
            '*$viscosity$/$radius$'
        gamma_liq = VariableFunction("gamma_liquid", params, function=function)

        if input_file_pixel_selector:
            function = '$image_force_ratio$*$pt_cortexn$'
            # important NOTE - k_signal should in principle be scaled based on
            # the number of grid pixels.
            k_signal = VariableFunction('k_signal', params, function=function)

        function = str(param_xml.get_value('cd_kd')) + '*$h_eff$'
        cd_kd = VariableFunction('cd_kd', params, function=function)

        adh_cell_mult = Variable(
            "adh_cell_mult",
            params,
            value=1000 *
            u("nN/um"))  # 0.001
        # multiply so adh_cell expresses a ratio of adhesion and surface
        # tension.
        function = "{}*$adh_cell_mult$".format(
            param_xml.get_value('adh_cell')) + '*$pt_cortexn$'
        adh_cell = VariableFunction("adh_celln", params, function=function)

        # Adhesion energy cell hull
        adh_hull = Variable("adh_hull", params, value=0.00002)

        function = str(param_xml.get_value('ka')) + '*$pt_cortexn$'
        # prevents triangles from collapsing or blowing up too much (no effect
        # for small deformations, kicks in when triangles become too extreme)
        ka = VariableFunction("ka", params, function=function)

        function = '$baseVisco$'
        baseVisco_f = VariableFunction(
            "baseVisco_f", params, function=function)

        k_layer_mult = Variable("k_layer_mult", params, value=1)
        k_layer = VariableFunction(
            "k_layer", params, function="{}*$k_layer_mult$".format(param_xml.get_value('k_layer')))

        del param_xml.l_main_keys[-1]

        #------------------------------------------------------#
        # Creating the base for the Signal
        if input_file_pixel_selector:
            Grid = ParticleContainer("BaseGrid", prt.Sphere, mysim)
            Grid.create_array('Scalar', 'Signal')
            Grid.create_array("Scalar", "h")

        #------------------------------------------------------#
        # Creating the base for the cells
        cells = prt.ParticleContainer("cells", DeformableCell, mysim)
        create_array("Scalar", 'edge_length', cells("triangles"))
        create_array("Scalar", 'layer_width', cells("triangles"))
        create_array("Index", 'reject', cells("triangles"))
        create_array(
            "unsigned",
            'number_of_effective_contacts',
            cells("triangles"))
        create_array("int", 'contact_index', cells("triangles"))

        cells.DeformableCellGeometryCmds(
            maximal_radius=max_r, enforce_positive=False)

        cells.DeformableCellInternalMechanicsCmds(kv=kv_cell, internal_pressure=p_int, nu=Vpoiss, viscosity=baseVisco_f, surface_tension=pt_cortex_f, thickness=tc, indirect_surface_tension_model=False, ka=0., ka_compress=ka
                                                  )
        #------------------------------------------------------#
        # Adding contact mechanics and interpolation stuff
        CM_cell_cell = FlatLayerAdhesiveTensionFprimIntMatrix(pc1=cells('triangles'), pc2=cells('triangles'), w1=adh_cell, w2=adh_cell, gamma_normal=FrictionN, gamma_tangential=FrictionT, Fprim1=cells('triangles')['F'], Fprim2=cells('triangles')['F'], contact_range=h_eff, implicitness=1.0, reject_angle=np.pi / 2., k_layer=k_layer  # 1e3   #1e12 ##speedup repulsion  Layer stiffness.
                                                              )

        if input_file_pixel_selector:
            h0 = h_smooth.get_value()
            h = (h0, h0, h0)
            rval = min(h)
            CM_grid_cell = GridToCell(
                pc1=Grid, pc2=cells('nodes'), h=h, scale_factor=k_signal)

        #------------------------------------------------------#
        # Adding contact detectors

        CD_cell_cell = cdm.MultiGridContactDetector(
            "Cell-Cell", mysim, cmodel=CM_cell_cell, update_every=5, keep_distance=cd_kd)

        if input_file_pixel_selector:
            CD_cell_grid = cdm.MultiGridContactDetector("Cell-grid", mysim, cmodel=CM_grid_cell, update_every=25, keep_distance=1.799 * h0  # Beyond this value, the function is less than 1/10th of maximal value
                                                        )

        #------------------------------------------------------#
        # Adding viscous friction
        cells("nodes").FrictionOnAreaCmd(area=cells('nodes')['area'], gamma_normal=gamma_liq, gamma_tangential=gamma_liq
                                         )

        #------------------------------------------------------#
        # Adding a GC solver for the system
        CG = cg.DefaultConjugateGradientSolver(
            sim=mysim, tolerance=opt_limit, reset_x=False)

        #------------------------------------------------------#
        # Stability command for a viscous model
        # cells.MeshUniformizationCmds( relaxation_time = dtg*20, weight_with_area = False)

        #------------------------------------------------------#
        # adding cells
        input_file_vtp = param_xml.get_value(
            'input_file_vtp', ['paths'], docker_run=docker_run_ic)
        if input_file_vtp:
            print(
                "using vtp file as input for cells : {0}".format(input_file_vtp))
            d_parentID_VTP = extract_parentID_selection_from_VTP(
                str(input_file_vtp), verbose=False)
            for parentID, poly_cell in d_parentID_VTP.items():
                print(" - loading cell '{}' from vtp".format(parentID))
                wdo_cell = dsa.WrapDataObject(poly_cell)
                add_cell_to_particle_container(wdo_cell.Points, wdo_cell.GetAttributes(
                    vtk.vtkDataObject.CELL)['vertexIndices'], cells, cd_kd.get_value(), mysim)
        else:
            stlfiles = sorted(
                param_xml.get_value(
                    'input_folder_meshed_cells',
                    ['paths'],
                    docker_run=docker_run_ic).glob(
                    param_xml.get_value(
                        'regex_input_meshes',
                        ['paths'],
                        docker_run=docker_run_ic)))  # Cells are saved as a collection of names stl files, in a separate folder.
            SELECT_IDX_CELL = param_xml.get_value(
                'SELECT_IDX_CELL', ['process_flow'], docker_run=docker_run_ic)
            for i in range(len(stlfiles)):
                # if (len(SELECT_IDX_CELL)==0) or (i in SELECT_IDX_CELL):
                if SELECT_IDX_CELL is None or (i in SELECT_IDX_CELL):
                    print(" - loading cell '{}'".format(stlfiles[i]))
                    ctrl, vils = read_stl(str(stlfiles[i]))
                    # BART: Convert from micron to SI units.
                    xs = np.array(ctrl) * 1e-6
                    add_cell_to_particle_container(
                        xs, vils, cells, cd_kd.get_value(), mysim)

        print("Done adding cells. Total of {} nodes and {} triangles".format(
            cells('nodes').size(), cells('triangles').size()))
        if cells('nodes').size() == 0:
            print(f"<ERROR> No cells were found.  Please check input_folder_meshed_cells")
            exit(1)

        if param_xml.get_value('fit_hull', ['process_flow']):
            # Hull ------------------------------------------------------------
            # wth previously 250, but expanded in conjuction with scaling of
            # hull
            erange = Variable("contact_range", params, value=25 * u('nm'))
            hull = prt.ParticleContainer("hull", prt.RigidBody.compose(
                (prt.RigidTriangle, "hull", "controlPoints")), mysim)
            tension = create_array("Scalar", "tension", hull("hull"))
            Fprim = create_array("Vector", "F", hull("hull"))
            create_array("Scalar", "layer_width", hull("hull"))
            ca = create_array("Scalar", "contact_area", hull("hull"))
            create_array("Scalar", "r", hull)

            ComputeNodeNormalsCommand(
                "ComputeHullNodeNormals",
                mysim,
                pc=hull("hull"),
                nodes=hull("controlPoints"))
            hull("hull").TriangleNormalsAndAreaCmd(
                x=hull("controlPoints")['x'])
            SetValueCommand("ResetHullTension", mysim, value=0., array=tension)
            SetValueCommand("ResetHullContactArea", mysim, value=0., array=ca)
            SetValueCommand(
                "ResetHullFPrim", mysim, value=(
                    0., 0., 0.), array=Fprim)

            hull.SetContactMatrixDiagonalCmd(
                visc=1e8)  # hull may not migrate ;-)

            CM_cell_hull = FlatLayerAdhesiveTensionFprimIntMatrix(pc1=cells('triangles'), pc2=hull('hull'), w1=adh_hull, w2=0, gamma_normal=FrictionN.get_value() / 20, gamma_tangential=FrictionT.get_value() / 20, Fprim1=cells('triangles')['F'], Fprim2=hull('hull')['F'], contact_range=erange, implicitness=1.0, k_layer=1e12  # with : used to be 100e9  # flat layer adhesion (repulsion)
                                                                  )

            CD_cell_hull = cdm.MultiGridContactDetector("Cell-hull", mysim, cmodel=CM_cell_hull, update_every=25  # orig 5
                                                        , keep_distance=cd_kd.get_value())

            thehull = hull.add_particle()
            thehull.q = (1.0, 0, 0, 0)  # quaternions
            thehull.v = (0, 0, 0)  # did nothing
            # I have no idea why this is done
            thehull.r = radius.get_value() - 0.25 * erange.get_value()
        #------------------------------------------------------#
        # some printers
        printerl = [pp.DefaultProgressPrinter(mysim)  # simulation time, timestep, wall-time, and simulation-/wall-time fraction.
                    , VolumeLossPrinter(cells), pp.PropertyPrinter(CG('steps'), "CG ")
                    ]

        printer = pp.PrinterChain(printerl)
        pp.ProgressIndicator("PrintProgress", mysim, printer=printer, print_interval=3
                             )

        # Remeshing procedure -------------------------------------------------
        mysim.run_until(dtg)  # Needs some initialization to start properly
        # timelist = [dtg, 0.1 * time_mult, 0.3 * time_mult, 0.5 * time_mult, 0.7 * time_mult]
        remesh_every = param_xml.get_value('remesh_every', ['parms_sim'])
        if remesh_every:
            timelist = list(np.arange(0, time_mult, remesh_every))
        else:
            timelist = [dtg] + [time_mult *
                                i for i in param_xml.get_value('remesh_times', ['parms_sim'])]
        logging.info(
            f'remeshing at {timelist}, using simulation timesteps = {dtg} and time_mult={time_mult}')
        # 1) We need to set geometrical commands on the same frequency as
        # remeshing
        mysim("loop_cmds/pre_body_force_cmds/ExecuteOnce_ComputeTriangleIndexList_cells_triangles").set(
            gate=cmds.ExecuteAtGivenTimes(time_list=timelist))
        mysim("loop_cmds/pre_body_force_cmds/ExecuteOnce_SortTriangleIndexList_cells_triangles").set(
            gate=cmds.ExecuteAtGivenTimes(time_list=timelist))
        # 2) Compute triangle area before remeshing command
        cells("triangles").TriangleNormalsAndAreaCmd(
            parent=mysim("loop_cmds/remove_particles_cmds"), x=cells('nodes')['x'])
        # 3) Create node and triangle manager + decorators (in case ka = kd =
        # 0)
        node_manager = pm.NodeManager(
            cells("nodes"),
            particle_type=pd.TriangleIndices(
                pt.Node))
        triangle_manager = pm.TriangleManager(
            cells("triangles"), particle_type=pt.Triangle)
        # 4) Enable remeshing
        avgarea = 1.1 * np.sum(mysim("cells/area")) / \
            len(mysim("cells/triangles/area"))
        TriangularMeshRemeshingCommand("Remeshing", mysim, stages=[st.RemeshingStageSplitLargeTriangles,
                                                                   st.RemeshingStageFlipEdges,
                                                                   st.RemeshingStageDeleteSmallTriangles,
                                                                   st.RemeshingStageUpdateNodeFixedListContactList], node_manager=node_manager, triangle_manager=triangle_manager, triangle_contact_detectors=[mysim("loop_cmds/contact_cmds/CD_Springs_cells")], min_area=avgarea / 2, max_area=avgarea * 2, verbosity=0, gate=cmds.ExecuteAtGivenTimes(time_list=timelist)
                                       )

        # ------------------------------------------------------#
        # RUNNING SIMULATION
        # ------------------------------------------------------
        # set output frequency
        # cells('triangles').VTKWriterCmd(executeInterval=dtg, select_all=True, directory=str(output_folder))  #vtp files
        # mysim.run_until(mysim.get_time() + dtg)

        DataSaveCommand("SaveDataCommand", mysim, executeInterval=1.0, directory=str(
            output_folder))  # mpacts data files...  execute interval smaller : more snapshots)
        cells('triangles').VTKWriterCmd(
            executeInterval=.01,
            select_all=True,
            directory=str(output_folder))  # vtp files  0.5 prev

        # PHASEI------------
        if param_xml.get_value('fit_hull', ['process_flow']):
            add_hull_to_simulation()
        print(
            ">>>PhaseI:cell relaxation (without pixels); simulation time : {0}s".format(
                param_xml.get_value(
                    'simulation_time_1',
                    ['parms_sim'])))
        print_simulation_parameters()
        mysim.run_until(
            mysim.get_time() +
            param_xml.get_value(
                'simulation_time_1',
                ['parms_sim']))
        log_chronology_txt("After phaseI")

        cells('triangles').VTKWriterCmd(
            executeInterval=.01,
            select_all=True,
            directory=str(output_folder))  # vtp files

        if input_file_pixel_selector:
            add_pixels_to_simulation()
        # PHASEII--------------
        sim_time_2 = param_xml.get_value('simulation_time_2', ['parms_sim'])
        if sim_time_2 > 0:
            update_parms_simulation(phase=2)
            print(">>>PhaseII:cell expansion (with pixels); simulation time : {0}s".format(
                sim_time_2))
            print_simulation_parameters()
            mysim.run_until(
                mysim.get_time() +
                param_xml.get_value(
                    'simulation_time_2',
                    ['parms_sim']))
            log_chronology_txt("After phaseII")

        # PHASEIII-------------------
        sim_time_3 = param_xml.get_value('simulation_time_3', ['parms_sim'])
        if sim_time_3 > 0:
            update_parms_simulation(phase=3)
            # Grid['Signal'].set_array([0 for _ in list(Ibase)]) # switching of
            # signal
            if param_xml.get_value('fit_hull', ['process_flow']):
                # mysim.remove_child(hull)  #problem later on : Multigrid still
                # active
                scale_hull(0)  # effectively shortcutting the hull

            print(
                ">>>PhaseIII:Relaxation with pixels; simulation time : {0}s".format(sim_time_3))
            print_simulation_parameters()
            cells('triangles').VTKWriterCmd(
                executeInterval=.01,
                select_all=True,
                directory=str(output_folder))  # vtp files
            mysim.run_until(
                mysim.get_time() +
                param_xml.get_value(
                    'simulation_time_3',
                    ['parms_sim']))
            log_chronology_txt("After phaseIII")

        # additional short simulation to get contact information after the last
        # remeshn
        # Must be after last remesh
        engage_cd_counter(mysim, cells, h_eff, CD_cell_cell)
        mysim.run_until(mysim.get_time() + dtg)
        vtkWriter.VTKSerialWriter(
            data=cells('triangles'),
            filename=str(
                output_folder /
                "Seeding"),
            select_all=True,
            start_index=get_latest_file()[1] +
            1).execute()
        log_chronology_txt("After final runtime : engaging contactdetection")

        # read in clean VTP to fix indices
        poly_embryo = read_vtp_file(output_folder / get_latest_file()[0])
        d_oldix_newix, _ = get_map_oldix_newix(type='triangles')
        add_array_with_mapper(
            poly_embryo,
            name_array_source='contact_index',
            name_array_dest='contact_index',
            mapper=d_oldix_newix,
            field_type="CELL")
        d_oldix_newix, _ = get_map_oldix_newix(type='nodes')
        add_array_with_mapper(
            poly_embryo,
            name_array_source='vertexIndices',
            name_array_dest='vertexIndices',
            mapper=d_oldix_newix,
            field_type="CELL")
        write_vtp_file(poly_embryo, output_folder / get_latest_file()[0])
        log_chronology_txt(
            "After final runtime : fixing indices due to remeshing => end of simulation")

        # save centroids
        np.savetxt(str(output_folder / "cell_centroids.csv"),
                   np.array(cells('x')), delimiter=",")
        logging.info(f'runtime_mpacts_pics = {time.time() -tic}')
