'''
Created on 19dec 2021
preprocessing run. Locating membranes in a microscopy time lapse
@author: wimth

'''
from helper_functions import get_dimensions, imsave_fiji
from joblib import Parallel, delayed
import multiprocessing
from tifffile import TiffFile, imsave, imread
from pathlib import Path
from preprocess import filter_rescale, filter_frangi, filter_reconstruction, filter_remove_small_objects, filter_blend_z_membrane, set_thresholds, smooth_exterior_mask, filter_skeletonize, get_exterior_mask
from param_XML import Param_xml
import sys
import copy
import re
import shutil
import os
import warnings
import pandas as pd
import numpy as np


def _preprocess_rescale(a_stack, param_xml, ix_stack):
    kwargs = {
        'curr_res': param_xml.prm['scaling_ZYX'],
        'target_res': param_xml.prm['scaling_ZYX_rescaled']
    }

    f_out = str(param_xml.prm['img_raw_file'].stem) +\
        f"_rescaled_t{str(ix_stack).zfill(3)}" + '.tif'
    p_out = param_xml.prm['temp_folder'] / "rescaled" / f_out

    return filter_rescale(a_stack, kwargs, p_out)


def _preprocess_frangi(a_stack, param_xml, ix_stack):

    scale_range = param_xml.get_value('scale_range', ['frangi'])
    if len(scale_range) == 1:
        # results in 1 sigma
        sigma_start, sigma_end = scale_range[0], scale_range[0] + 0.0001
    else:
        sigma_start, sigma_end = scale_range

    kwargs = {
        'sigmas': np.arange(sigma_start, sigma_end, param_xml.get_value('scale_step', ['frangi'])),
        'alpha': param_xml.get_value('alpha', ['frangi']),  # no effect in 2D !
        'beta': param_xml.get_value('beta', ['frangi']),  # used to be beta1
        'gamma': param_xml.get_value('gamma', ['frangi']),
        'black_ridges': param_xml.get_value('black_ridges', ['frangi'])
    }

    # kwargs = {
    #     'sigmas': param_xml.get_value('scale_range', ['frangi']),
    #     'scale_step': param_xml.get_value('scale_step', ['frangi']),
    #     'alpha': param_xml.get_value('alpha', ['frangi']),  # no effect in 2D !
    #     'beta': param_xml.get_value('beta', ['frangi']),  # used to be beta1
    #     'gamma': param_xml.get_value('gamma', ['frangi']),
    #     'black_ridges': param_xml.get_value('black_ridges', ['frangi'])
    # }

    p_out = (param_xml.prm['temp_folder'] / 'frangi'
             / (f'frangi_t{str(ix_stack).zfill(3)}' + '.tif'))

    return filter_frangi(a_stack, kwargs, True, p_out)


def _preprocess_set_thresholds(a_stack, param_xml, prm,
                               df_thresholds, ix_stack):
    kwargs = {
        'seed_thr_divide_factor': param_xml.get_value(
            'SEED_THR_DIVIDE_FACTOR', ['collect_stats']),
        'membrane_acceptance_level': param_xml.get_value(
            'MEMBRANE_ACCEPTANCE_LEVEL', ['collect_stats']),
        'no_signal_thresh': param_xml.get_value(
            'NO_SIGNAL_THRESH', ['collect_stats']),
        'z_range': prm['z_range']
    }

    return set_thresholds(a_stack, df_thresholds, **kwargs)


def _preprocess_threshold(a_stack, df_thresholds, param_xml, ix_stack):
    kwargs = {'method': 'dilation'}
    p_out = param_xml.prm['temp_folder'] / 'threshold'\
        / (f'threshold_t{str(ix_stack).zfill(3)}' + '.tif')

    return filter_reconstruction(a_stack, df_thresholds, kwargs, p_out)


def _preprocess_remove_small_objects(a_stack, param_xml, ix_stack):
    kwargs = {'min_size': param_xml.get_value(
        'min_size', ['RmSmObj3D']),
        'connectivity': param_xml.get_value(
        'connectivity', ['RmSmObj3D'])}

    p_out = None

    return filter_remove_small_objects(a_stack, kwargs, p_out)


def _preprocess_skeletonize(a_stack, param_xml, ix_stack):
    p_out = param_xml.prm['temp_folder'] / 'skeletonize' \
        / (f'membranes_t{str(ix_stack).zfill(3)}' + '.tif')
    return filter_skeletonize(a_stack, p_out)


def _preprocess_exterior_mask(a_stack, param_xml, ix_stack):
    p_out = param_xml.prm['temp_folder'] / 'exterior_mask' /\
        (f'exterior_mask_t{str(ix_stack).zfill(3)}' + '.tif')
    return get_exterior_mask(
        a_stack, p_out, param_xml.prm['build_exterior_mask_3D'])


def _preprocess_blend_z_membrane(a_membrane, a_stack,
                                 a_threshold, a_exterior_mask,
                                 param_xml, prm, df_thresholds, zoom, ix_stack):

    def set_z_range_loc_max():
        zoom_z = 1 if not zoom else zoom[0]
        z_range_loc_max = prm['z_range_loc_max']
        if z_range_loc_max:
            if prm['z_range_loc_max'] == 'auto':
                narrow_range_by = 1  # in micron
                if prm['scaling_ZYX_rescaled']:
                    scaling_ZYX = prm['scaling_ZYX_rescaled']
                    zdim = prm['Zdim_rescale']
                else:
                    scaling_ZYX = prm['scaling_ZYX']
                    zdim = prm['Zdim_rescale']
                z_range_narrow = (narrow_range_by / scaling_ZYX[0])
                z_range = prm.get('z_range')
                if not z_range:
                    z_range = [1, zdim]
                z_range_loc_max = [
                    int(z_range[0] + z_range_narrow), int(z_range[1] - z_range_narrow)]
            else:
                z_range_loc_max = [int(i) for i in z_range_loc_max.split(";")]
                z_range_loc_max = [int(i * zoom_z) for i in z_range_loc_max]
        return z_range_loc_max

    z_range_loc_max = set_z_range_loc_max()
    kwargs = {'scaling_ZYX': param_xml.get_file_info(
        field='scaling_ZYX'),
        'sigma_ddz_smoothing': param_xml.get_value(
        'sigma_ddz_smoothing', ['blend_z_membrane']),
        'min_dist_peak_membrane': param_xml.get_value(
        'min_dist_peak_membrane', ['blend_z_membrane']),
        'min_dist_between_peaks': param_xml.get_value(
        'min_dist_between_peaks', ['blend_z_membrane']),
        'peak_thresh_rel': param_xml.get_value(
        'peak_thresh_rel', ['blend_z_membrane']),
        'peak_thresh_rel_slice': param_xml.get_value(
        'peak_thresh_rel_slice', ['blend_z_membrane']),
        'SN_thresh': param_xml.get_value(
        'SN_thresh', ['blend_z_membrane']),
        'z_metric_mode': param_xml.get_value(
        'z_metric_mode', ['blend_z_membrane']),
        'z_range_loc_max': z_range_loc_max,
        'verbose': prm['ic_agg_zlog']
    }
    kwargs['p_out_folder'] = param_xml.prm['temp_folder'] \
        / 'z_membrane' / 'log' / \
        f'z_membrane_intermediate_steps_{str(ix_stack).zfill(2)}' if kwargs[
        'verbose'] else None

    p_out = param_xml.prm['temp_folder'] / 'z_membrane' \
        / (f'membranes_t{str(ix_stack).zfill(3)}' + '.tif')

    return filter_blend_z_membrane(a_membrane, a_stack,
                                   a_threshold, a_exterior_mask,
                                   kwargs, df_thresholds, p_out)


def _log_output(df_thresholds, d_log, param_xml, ix_stack):

    if df_thresholds is None:
        return

    if d_log is not None:
        df_thresholds.drop(list(d_log.keys()), axis=1,
                           inplace=True, errors='ignore')
        df_thresholds = pd.concat(
            [df_thresholds.reset_index(), pd.DataFrame(d_log)], axis=1)

    f_out = param_xml.prm['temp_folder'] / 'log' / \
        f"df_thresholds_t{str(ix_stack).zfill(3)}.csv"

    f_out.parent.mkdir(parents=True, exist_ok=True)

    df_thresholds.to_csv(f_out, index=False)

    return


def preprocess_stack(a_stack, param_xml, df_thresholds, ix_stack):
    """ each filter will be fed the output of the previous step.
    stacks needed for later steps will be kept in memory"""
    prm = param_xml.prm
    d_log, zoom, a_stack_rescaled = None, None, None
    for ix_filter, filter_i in enumerate(prm['l_filters']):
        # print(filter_i, '-----------------------------')

        if ix_filter == 0 and 'rescale' not in prm['l_filters']:
            a_stack_raw = copy.deepcopy(a_stack)

        if filter_i == 'rescale':
            a_stack, zoom = _preprocess_rescale(
                a_stack, param_xml, ix_stack)
            a_stack_rescaled = copy.deepcopy(
                a_stack) if 'z_membrane' in prm['l_filters'] else None
            prm['Zdim_rescale'], prm['Ydim_rescale'], prm['Xdim_rescale'] = a_stack_rescaled.shape
            if prm.get('z_range'):
                prm['z_range'] = [int((i - 0.5) * zoom[0])
                                  for i in prm['z_range']]
            # if prm.get('z_range_loc_max'):
            #     prm['z_range_loc_max'] = [int(i * zoom[0])
            #                               for i in prm['z_range_loc_max']]

        elif filter_i == 'frangi':
            a_stack = _preprocess_frangi(a_stack, param_xml, ix_stack)

        elif filter_i == 'threshold':
            df_thresholds = _preprocess_set_thresholds(
                a_stack, param_xml, prm, df_thresholds, ix_stack)
            a_stack = _preprocess_threshold(
                a_stack, df_thresholds, param_xml, ix_stack)
            a_threshold = copy.deepcopy(
                a_stack) if 'z_membrane' in prm['l_filters'] else None

        elif filter_i == 'remove_small_objects':
            a_stack = _preprocess_remove_small_objects(
                a_stack, param_xml, ix_stack)

        elif filter_i == 'skeletonize':
            a_stack = _preprocess_skeletonize(a_stack, param_xml, ix_stack)
            a_membrane = copy.deepcopy(
                a_stack) if 'z_membrane' in prm['l_filters'] else None

        elif filter_i == 'exterior_mask':
            if prm.get('switch_to_annotated_membrane'):
                prm['Tdim'], prm['Zdim'], prm['Ydim'], prm['Xdim'] = get_dimensions(
                    prm['annotated_membrane_file'])
                a_stack = _read_stack_img(
                    prm['annotated_membrane_file'], prm['Zdim'], ix_stack + 1)
            a_stack = _preprocess_exterior_mask(
                a_stack, param_xml, ix_stack)

        elif filter_i == 'z_membrane':
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                a_stack, d_log = _preprocess_blend_z_membrane(
                    a_membrane, a_stack_raw if a_stack_rescaled is None else a_stack_rescaled, a_threshold,
                    a_stack, param_xml, prm, df_thresholds, zoom, ix_stack)
    _log_output(df_thresholds, d_log, param_xml, ix_stack)

    print(f"{ix_stack}|", end="", flush=True)

    return a_stack_rescaled.shape if a_stack_rescaled is not None else a_stack.shape


def _set_up_run_annotated_membrane(prm):
    # prm['img_raw_file'] = prm['annotated_membrane_file']
    # print(f"img raw file set to {prm['img_raw_file']}")
    if os.path.exists(prm['output_folder'] / 'frangi.tif'):
        prm['scaling_ZYX_rescaled'] = None
        prm['l_filters'] = ['exterior_mask']
        prm['img_raw_file'] = prm['annotated_membrane_file']
        print('img raw file swithed to annotated')
    else:
        prm['switch_to_annotated_membrane'] = True

    return


def _read_parms(p_xml):
    # print(p_xml)
    param_xml = Param_xml.get_param_xml(
        p_xml, l_main_keys=['body'], verbose=False)
    param_xml.add_prms_to_dict(l_keys=['RAM'])
    param_xml.add_prms_to_dict(l_keys=['preprocessing', 'flowcontrol'])
    param_xml.add_prms_to_dict(l_keys=['preprocessing', 'paths'])

    param_xml.l_main_keys = ['body', 'preprocessing', 'filter_parms']
    prm = param_xml.prm

    prm['z_range'] = param_xml.get_value('z_range', ['collect_stats'])
    # if not prm['z_range']:
    #     prm['z_range'] = range(0, prm['Zdim'])

    prm['z_range_loc_max'] = param_xml.get_value(
        'z_range_loc_max', ['blend_z_membrane'])
    prm['build_exterior_mask_3D'] = param_xml.get_value(
        'build_exterior_mask_3D', ['exterior_mask'])
    prm['smooth_exterior_mask'] = param_xml.get_value(
        'smooth_exterior_mask', ['exterior_mask'])
    if prm['nb_cores'] < 1:
        prm['nb_cores'] = multiprocessing.cpu_count()
    prm['output_folder'] = prm['root_output_folder'] / '002_preprocessing'
    prm['temp_folder'] = prm['output_folder'] / 'temp'

    if prm['annotated_membrane_file']:
        _set_up_run_annotated_membrane(prm)

    try:
        prm['Tdim'], prm['Zdim'], prm['Ydim'], prm['Xdim'] = get_dimensions(
            prm['img_raw_file'])
    except Exception as e:
        exit(1)

    if not prm['l_stack_nb']:
        prm['l_stack_nb'] = [i + 1 for i in range(prm['Tdim'])]

    return param_xml


def aggregate_temp_files(p_folder, p_out, append=False,
                         smooth_exterior=False, agg_zlog=False, clean_up=False):
    print('\nAggregating data...\n', flush=True, end="")
    (p_out if not agg_zlog else p_out / "zlog").mkdir(parents=True, exist_ok=True)
    # concatenate images
    l_img = []
    stem_old = ""
    for p_img in sorted(p_folder.rglob('*.tif')):
        query_stem = re.findall('([^/]*)_t[0-9]*.tif', str(p_img.name))
        if not query_stem:
            continue
        stem_new = query_stem[0]
        if append:
            data = imread(p_img)
            imsave(str(p_out / (stem_new + '.tif')),
                   data.reshape((1, *data.shape)), append=True)
        else:
            if stem_new != stem_old and len(l_img) > 0:
                a_stack = np.stack(l_img)
                if stem_old == 'exterior_mask' and smooth_exterior:
                    kwargs = {'weight_center': 3,
                              'weight_top_bottom': 2, 'weight_left_right': 1}
                    a_stack = smooth_exterior_mask(
                        a_stack, p_out=None, **kwargs).astype(np.uint8)
                imsave_fiji(p_out / (stem_old + '.tif'), a_stack)
                print(f"{stem_old}|", flush=True, end="")
                l_img = []
                a_img = None
            with TiffFile(p_img) as tif:
                l_img.append(tif.asarray())
                print(f"|", flush=True, end="")
        stem_old = stem_new
    if l_img:
        agg_file_out = p_out / (stem_old + '.tif')
        imsave_fiji(agg_file_out, np.stack(l_img))

    if agg_zlog:
        l_regex_zlog = [
            '*3_ddz_extended_mask.tif',
            '*5_ddzsmooth_peak_overlay.tif',
            '*7_extended a_peaks_extend_over_original.tif']
        for regex_i in l_regex_zlog:
            l_img = []
            for p_img in sorted(p_folder.rglob(regex_i)):
                with TiffFile(p_img) as tif:
                    l_img.append(tif.asarray())
            if l_img:
                imsave_fiji(p_out / 'zlog' / p_img.name, np.stack(l_img))

    l_df_threshold = []
    for p_file in sorted(p_folder.rglob('*threshold*.csv')):
        df_threshold = pd.read_csv(p_file)
        df_threshold.rename({'Unnamed: 0': 'z'}, inplace=True, axis=1)
        df_threshold['t'] = int(re.findall(
            '[^/]*_t([0-9]*).csv', str(Path(p_file).name))[0])
        l_df_threshold.append(df_threshold)
    if l_df_threshold:
        df_threshold = pd.concat(l_df_threshold)
        df_threshold.to_csv(p_out / "thresholds.csv", index=False)

    # clean up temp files
    if clean_up:
        shutil.rmtree(p_folder)

    print("")

    return


def _update_xml(param_xml, l_log):
    prm = param_xml.prm

    if (prm['scaling_ZYX_rescaled'] != prm['scaling_ZYX']) and \
            ('rescale' in prm['l_filters']):

        param_xml.store_value(
            s_parm='RAW_IMG_DIM_TZYX_rescaled',
            value_to_store=f"{len(l_log)}\
            ;" + ';'.join([str(i) for i in l_log[0]]),
            l_path_keys=["body", "RAM"],
            persist=True, verbose=False)

        param_xml.store_value(
            s_parm='img_raw_file_rescaled',
            value_to_store=str(
                prm['output_folder']
                / (str(prm['img_raw_file'].stem)
                   + '_rescaled' + '.tif')),
            l_path_keys=["body", "RAM", "paths"],
            persist=True, verbose=False)

    return


def _read_stack_img(p_file, Zdim, nb_stack):

    with warnings.catch_warnings():
        warnings.simplefilter("ignore")

        a_stack = imread(str(p_file), key=range(
            (nb_stack - 1) * Zdim, (nb_stack) * Zdim, 1))

    return a_stack


if __name__ == '__main__':
    param_xml = _read_parms(sys.argv[1])
    prm = param_xml.prm

    df_thresholds = pd.read_csv(
        prm['threshold_file']) if prm['threshold_file'] else None

    print(f'Extracting membrane from image\
        ({prm["nb_cores"]} cores)...', flush=True)
    l_log = Parallel(n_jobs=prm['nb_cores'])(delayed(preprocess_stack)(
        a_stack=_read_stack_img(prm['img_raw_file'], prm['Zdim'], nb_stack),
        param_xml=param_xml,
        df_thresholds=None if df_thresholds is None else
        df_thresholds[df_thresholds.t == nb_stack - 1],
        ix_stack=nb_stack - 1)
        for ix, nb_stack in enumerate(prm['l_stack_nb']))

    aggregate_temp_files(prm['temp_folder'], prm['output_folder'],
                         append=False, smooth_exterior=prm['smooth_exterior_mask'],
                         agg_zlog=prm['ic_agg_zlog'], clean_up=True)
    _update_xml(param_xml, l_log)
