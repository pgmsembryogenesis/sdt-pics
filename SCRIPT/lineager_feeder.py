'''
Created on 4 Apr 2019
interface for the Lineager application (WormDeLux)

lineage tree is built up using the
* cluster labels (own cluster_label and parent_label)
* cluster.division indication to mark cell division (marked 'NEW CELL')
* cell_rank : to mark the seed clusters (only seed clusters get an entry)

al this information comes from the cluster_summary report from spheresDT

example : a cell division in the cluster summary looks likes this
t   color cluster_label         parent_label
1	blue	    4	    seed	    99
2	blue	    4	    seed	    99	        DIVIDED
2	yellow+++	46	    seed	    4	        NEW CELL

@author: wimth
'''

from param_XML import Param_xml
import pandas as pd
from collections import namedtuple
import os
import sys
import re
from datamodel import Label
from helper_functions import slice_tif
from fileHandler import FileHandler
from os import path


def read_parms():

    param_xml = Param_xml.get_param_xml(
        sys.argv, l_main_keys=['body'], verbose=True)
    param_xml.add_prms_to_dict(l_keys=['RAM'])
    param_xml.add_prms_to_dict(l_keys=['lineager_feeder'])

    prm = param_xml.prm
    if prm['offset_lineage_files'] == -1:
        prm['l_stack_number'] = param_xml.get_value(
            'l_stack_number', ['preprocessing', 'flowcontrol'])
        if isinstance(prm['l_stack_number'], list):
            prm['offset_lineage_files'] = prm['l_stack_number'][0] - 1
        else:
            prm['offset_lineage_files'] = 0

    prm['convert_to_tsv'] = True


    return param_xml


def generate_lineager_input_files(param_xml, fh):

    def read_input_file():
        full_path = prm['INPUT_FILE'] if prm['INPUT_FILE'] else fh.get_f_name(
            'cluster_summary')
        df_ipt = pd.read_csv(full_path, delimiter=',')

        return df_ipt

    def write_lineage_file_prev():
        fh.d_save_info['f_name'] = 't{0}-nuclei'.format(
            str(nb_time_prev + prm['offset_lineage_files']).rjust(3, '0'))
        fh.save_data(
            d_label_lineagedata_prev,
            file_ext='lineage',
            verbose=False)
        return

    def write_lineageId_report():
        df_cells = df_ipt.groupby(by=['stack_nb', 'cluster_label'], sort=True).first()[
            'name_lineage'].reset_index()
        l_parentIndex = []
        parentIndex = 0
        old_stack_nb = 1
        for _, row_i in df_cells.iterrows():
            new_stack_nb = row_i.stack_nb
            if new_stack_nb != old_stack_nb:
                parentIndex = 0
                old_stack_nb = new_stack_nb
            l_parentIndex.append(parentIndex)
            parentIndex += 1
        df_cells['parentIndex'] = l_parentIndex
        df_cells = df_cells[~df_cells['cluster_label'].isin(['9998', '9999'])]

        p_output = prm['output_folder'] / 'lineageID_report.csv'
        if path.exists(p_output):
            os.remove(p_output)
        print(f"Saving csv file : {p_output}")
        df_cells.to_csv(p_output, index=False)

        return

    def convert_starrynite_to_tsv(p_starry, p_output=None):

        l_row = []
        for path_i in sorted(p_starry.glob("*nuclei")):
            time = int(re.findall("(\\d+)-nuclei", str(path_i))[0])
            df_starry = pd.read_csv(path_i, delimiter=',', header=None)
            for ix, row_i in df_starry.iterrows():
                row = pd.Series({'cell': row_i[9], 'time': time, 'x': int(row_i[5]), 'y': int(row_i[6]), 'z': int(row_i[7]), 'radius': row_i[8],
                                 'self': int(row_i[0]), 'parent': int(row_i[2]), 'child1': int(row_i[3]), 'child2': int(row_i[4])})
                l_row.append(row)

        df_tsv = pd.DataFrame(l_row)
        if p_output:
            if path.exists(p_output):
                os.remove(p_output)
            print(f"Saving tsv file : {p_output}")
            df_tsv.to_csv(p_output, sep='\t', index=False)

        return df_tsv

    def make_image_sequence():
        print('  - making image sequence', flush=True)

        # fh.d_load_info['load_dir'] = IMG_RAW_DIR
        fh.d_load_info['f_name'] = str(
            param_xml.get_file_info(
                field='img_raw_file'))  # prm['img_raw_file']

        a_raw = fh.load_tif(verbose=False)
        if len(a_raw.shape) == 3:
            z, _, _ = a_raw.shape
            t = 1
        else:
            t, z, _, _ = a_raw.shape

        fh.extend_save_info(
            extra_dir_1='Image_Sequence',
            reset_to_snapshot=True)

        for t_i in range(t):
            for z_i in range(z):
                a_slice = slice_tif(
                    a_raw,
                    ix_slice_time=[
                        t_i,
                        t_i + 1],
                    ix_slice_z=[
                        z_i,
                        z_i + 1],
                    verbose=False)
                fh.d_save_info['f_name'] = param_xml.get_file_info(
                    field='img_raw_file').stem + '_t' + str(t_i + 1).zfill(3) + '_z' + str(z_i + 1).zfill(3)
                fh.save_data(a_slice, verbose=False)
                # print('|',end='')

        fh.reset_save_info()

        return

    #MAIN#################################################################

    prm = param_xml.prm
    fh.extend_save_info(
        extra_dir_1='005_Lineager_feeder',
        from_root=True,
        take_snapshot_after=True)
    fh.extend_save_info(extra_dir_1='lineage_files')

    df_ipt = read_input_file()

    LineageData = namedtuple('LineageData',
                             ['id',
                              'unk',
                              'link_past',
                              'link_future_1',
                              'link_future_2',
                              'x',
                              'y',
                              'z',
                              'diameter',
                              'name'])
    d_label_lineagedata = {}
    d_label_lineagedata_prev = {}
    d_label_deltaZ = {}
    d_label_seeded = {}
    nb_time_prev = 0

    time_range = range(df_ipt['stack_nb'].min(), df_ipt['stack_nb'].max() + 1)
    for nb_time in time_range:
        for _, row_cluster in df_ipt[df_ipt.stack_nb == nb_time].iterrows():
            if row_cluster.cluster_label in [
                    Label.EXTERIOR_CTR, Label.VALIDATION_CTR, Label.UNASSIGNED_CTR, Label.FILTERED_CTR]:
                continue
            if row_cluster.cell_rank == 'seed':
                nt_lineage = LineageData(id=row_cluster.ctr_lineage,  # i use the same id's in every stack ! so not unique over stacks
                                         unk=1,
                                         link_past=row_cluster.ctr_lineage,
                                         link_future_1=row_cluster.ctr_lineage,
                                         link_future_2=-1,
                                         x=row_cluster.x,
                                         y=row_cluster.y,
                                         z=row_cluster.z,
                                         diameter=4,
                                         name='{0}({1}_{2})'.format(
                                             row_cluster.name_lineage, row_cluster.cluster_label, row_cluster.color)
                                         )

                if nb_time == 1:
                    nt_lineage = nt_lineage._replace(link_past=-1)

                if row_cluster.division in [
                        'NEW CELL', 'DIVIDED']:  # a new cell must correct 2 records, current and previous

                    if d_label_lineagedata_prev:
                        nt_lineage_prev = d_label_lineagedata_prev.get(
                            row_cluster.name_lineage[:-2])
                        if nt_lineage_prev:
                            # correct the past link of the current record
                            nt_lineage = nt_lineage._replace(
                                link_past=nt_lineage_prev.id)
                            if row_cluster.division == 'NEW CELL':
                                # add the second future link to the previous
                                # record
                                nt_lineage_prev = nt_lineage_prev._replace(
                                    link_future_2=row_cluster.ctr_lineage)
                            else:
                                # add the first future link to the previous
                                # record
                                nt_lineage_prev = nt_lineage_prev._replace(
                                    link_future_1=row_cluster.ctr_lineage)
                            d_label_lineagedata_prev[row_cluster.name_lineage[:-2]
                                                     ] = nt_lineage_prev

                # a new entry or overruling of the previous cluster entry
                d_label_lineagedata[row_cluster.name_lineage] = nt_lineage

                # if row_cluster.division=='NEW CELL':  # a new cell must correct 2 records, current and previous
                #     nt_lineage=nt_lineage._replace(link_past=row_cluster.label_parent) #correct the past link of the current record (=remove 'd2')
                #     if d_label_lineagedata_prev:
                #         nt_lineage_prev = d_label_lineagedata_prev.get(row_cluster.label_parent)
                #         if nt_lineage_prev:
                #             nt_lineage_prev=nt_lineage_prev._replace(link_future_2=row_cluster.cluster_label) #add the second future link to the previous record
                #             d_label_lineagedata_prev[row_cluster.label_parent]=nt_lineage_prev

                # d_label_lineagedata[row_cluster.cluster_label] = nt_lineage #
                # a new entry or overruling of the previous cluster entry

            else:
                continue
        print(f'|', end='', flush=True)

        if d_label_lineagedata_prev:
            write_lineage_file_prev()

        nb_time_prev = nb_time
        d_label_lineagedata_prev = d_label_lineagedata
        d_label_lineagedata = {}
        d_label_deltaZ = {}

    write_lineage_file_prev()
    if prm['convert_to_tsv']:  # id  unk link past   link future 1   link future 2   x   y   z   rad name => cell    time    x   y   z   radius  self    parent  child1  child2
        df_tsv = convert_starrynite_to_tsv(
            prm['output_folder'] / 'lineage_files',
            prm['output_folder'] / 'lineage_SDT')

    write_lineageId_report()

    if prm['MAKE_IMAGE_SEQUENCE']:
        make_image_sequence()


if __name__ == '__main__':
    param_xml = read_parms()
    fh = FileHandler.init_fh(param_xml.prm)
    generate_lineager_input_files(param_xml, fh)
