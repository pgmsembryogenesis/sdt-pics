'''
Created on 27 Mar 2019

@author: wimth
'''
import xmltodict
import os
import re
from pathlib import Path
import pathlib
import xml.etree.ElementTree as ET

class Param_xml():

    def __init__(self, file, l_main_keys):

        self.file = Path(file)
        self.l_main_keys = l_main_keys

        with open(file) as fd:
            # full dict unresolved parms (raw)
            self.d_prms = xmltodict.parse(fd.read())
        self.prm = {}  # dict for resolved parms

    def get_value(self, s_parm, l_path_keys=[], use_main_keys=True, lookup=None, dtype=None, all=None, docker_run=False):

        def resolve_path_name(value):
            if "<ROOT_OUTPUT>" in value:
                OUTPUT_FOLDER_ROOT = self.get_value("root_output_folder", l_path_keys=[
                                                    'body', 'RAM', 'paths'], use_main_keys=False, dtype='string')
                if OUTPUT_FOLDER_ROOT != "None":
                    value = value.replace(
                        '<ROOT_OUTPUT>', str(OUTPUT_FOLDER_ROOT))

            if "<SUB_FOLDER>" in value:
                OUTPUT_FOLDER_ROOT_SUB = self.get_value("root_output_subfolder", l_path_keys=[
                                                        'body', 'RAM', 'paths'], use_main_keys=False, dtype='string')
                if OUTPUT_FOLDER_ROOT_SUB != "None":
                    value = value.replace(
                        '<SUB_FOLDER>', OUTPUT_FOLDER_ROOT_SUB)

            if "<runID>" in value:
                runID = self.get_value("runID", l_path_keys=[
                                       'body', 'LOG'], use_main_keys=False, dtype='string')
                if runID != "None":
                    value = value.replace('<runID>', runID)

            if value.startswith('..'):
                # location of this script
                value = value.replace(
                    '..', str(Path(os.path.realpath(__file__)).parents[1]))

            if value.startswith('.'):
                value = value.replace(
                    '..', str(Path(os.path.realpath(__file__)).parents[0]))

            return os.path.normpath(value)

        def fill_in_root_folder(value):
            OUTPUT_FOLDER_ROOT = self.get_value("root_output_folder", l_path_keys=[
                                                'body', 'RAM', 'paths'], use_main_keys=False, dtype='string')
            value = value.replace('<ROOT_OUTPUT>/', '')

            if "<SUB_FOLDER>" in value:
                OUTPUT_FOLDER_ROOT_SUB = self.get_value("root_output_subfolder", l_path_keys=[
                                                        'body', 'RAM', 'paths'], use_main_keys=False, dtype='string')
                value = value.replace('<SUB_FOLDER>/', '')
                if OUTPUT_FOLDER_ROOT_SUB != "None":
                    return os.path.join(OUTPUT_FOLDER_ROOT, OUTPUT_FOLDER_ROOT_SUB, value)

            return os.path.join(OUTPUT_FOLDER_ROOT, value)

        def compose_file_path():
            file_nm = d_param_sub[s_parm]['@value']
            s_folder = str(l_path_keys[-1])
            folder_path = self.get_value(
                s_folder, l_path_keys=l_path_keys[:-1], use_main_keys=False)

            if not isinstance(folder_path, Path):
                folder_path = Path(folder_path)

            return folder_path / file_nm

        s_parm = s_parm.strip()
        if isinstance(l_path_keys, str):
            l_path_keys = [l_path_keys]

        if use_main_keys:
            l_path_keys = self.l_main_keys + l_path_keys

        d_param_sub = self.retrieve_element(l_path_keys)

        if dtype == 'tree': #returns the dictionary of the tree at that level
            return d_param_sub[s_parm]

        # 'file' triggers recursion to retrieve folder name ! dtype path is implicit
        elif s_parm.startswith('file'):
            file_path = str(compose_file_path())

            if docker_run:
                file_path = file_path.replace(self.d_prms["body"]["RAM"]["paths"]["script_folder"]
                                              ["@value"], self.d_prms["body"]["RAM"]["paths"]["docker_script_folder"]["default"])
                file_path = file_path.replace(self.d_prms["body"]["RAM"]["paths"]["data_folder"]
                                              ["@value"], self.d_prms["body"]["RAM"]["paths"]["docker_data_folder"]["default"])

            return Path(file_path)

        else:
            value = d_param_sub[s_parm]['@value']
            if value == 'link':
                print('temp : stop using link , convert to +')
            if value == "default":
                value = d_param_sub[s_parm]['default']

        if value and "+" in value:  # + triggers recursion !
            l_path_keys = value.split('+')
            return self.get_value(l_path_keys[-1], l_path_keys=l_path_keys[:-1], use_main_keys=False, lookup=lookup, docker_run=docker_run)

        if lookup:                                                                  # look up a value
            value = d_param_sub[s_parm].get(lookup)
            if not value:
                value = d_param_sub[s_parm].get('default')
            if not value:
                value = d_param_sub[s_parm].get('@value')  # just as backup

        dtype = d_param_sub[s_parm].get('dtype', dtype)  # apply dtype
        if isinstance(dtype, dict):
            # in the rare cases we have updated dtype, we retrieve the update here (the old entry can be retrieved with '#text')
            dtype = dtype['@value']

        ic_list = True if (value and ';' in value) else False

         # if value == '' or value is None:
         #    return value

        if value in ['', 'nan', 'blanc', None]:
            return None

        if not dtype or dtype == 'string':
            # if dtype is not mentioned in param file, string is assumed
            value = value.split(';') if ic_list else str(value)
        if dtype == 'float':
            value = list(map(float, value.split(';'))
                         ) if ic_list else float(value)
        if dtype == 'int':
            if value is not None:
                value = list(map(int, value.split(';'))
                             ) if ic_list else int(float(value))
        if dtype == 'list;int':
            if value == 'all':
                pass
            else:
                try:
                    if ':' in value:
                        start, end = value.split(':')
                        value = list(range(int(start), int(end) + 1))
                    else:
                        value = list(map(int, value.split(';'))) if ic_list else [
                            int(float(value))]
                except:
                    value = []
        if dtype == 'list;float':
            if value == 'all':
                pass
            else:
                try:
                    value = list(map(float, value.split(';'))
                                 ) if ic_list else [float(value)]
                except:
                    value = []
        if dtype == 'list;str':
            value = list(map(str, value.replace('\n', '').split(';'))
                         ) if ic_list else [str(value)]

        if dtype == 'list;path':
            value = list(map(Path, value.replace('\n', '').split(';'))
                         ) if ic_list else [Path(value)]
        if dtype == 'list;2tuple':
            value = list(map(str, value.replace('\n', '').split(';'))
                         ) if ic_list else [str(value)]
            value = [i for i in zip(value[::2], value[1::2])]
        if dtype == 'bool':
            if "false" in value.lower() or value == '0':
                value = False
            else:
                value = True
        if dtype == 'tuple;int':
            value = tuple(map(int, value.split(';')))
        if dtype == 'tuple;float':
            value = tuple(map(float, value.split(';')))
        if dtype == 'tuple':
            value = tuple(value.split(';'))
        if dtype == 'dict;list;str':
            value = list(map(str, value.replace('\n', '').split(';'))) 
            value = {k: v.split(',') for k, v in zip(value[::2], value[1::2])}
            if not value:
                value = {}
        if dtype == 'dict;float':
            value = list(map(str, value.replace('\n', '').split(';'))) 
            value = {k: float(v) for k, v in zip(value[::2], value[1::2])}
            if not value:
                value = {}
    

        if dtype == 'path':
            if value:
                value = resolve_path_name(value)
                # if value.startswith('<ROOT_OUTPUT>'):
                #     value = fill_in_root_folder(value)

                if docker_run:
                    value = value.replace(self.d_prms["body"]["RAM"]["paths"]["script_folder"]["@value"],
                                          self.d_prms["body"]["RAM"]["paths"]["docker_script_folder"]["default"])
                    value = value.replace(self.d_prms["body"]["RAM"]["paths"]["data_folder"]["@value"],
                                          self.d_prms["body"]["RAM"]["paths"]["docker_data_folder"]["default"])

                value = Path(value)

        if value == 'all':
            if not all:
                pass
            else:
                value = range(1, all)

        if value == 'None' or value == ['None']:
            value = None

        return value  # default= string

    def __repr__(self):
        print('file=', str(self.file))
        print('l_main_keys=', self.l_main_keys)
        print('self.d_prms=', self.d_prms)
        return

    def store_value(self, s_parm, value_to_store, l_path_keys=["body", "RAM"], persist=False, verbose=True):
        """ updates a value in the dictionary
        persist : if True : updates the xml-file. use this if you want this value to be passed on to future modules of the same run
        """

        def generate_xml_key(l_path_keys=["body", "RAM"]):
            xml_key = "./"
            for i in l_path_keys:
                if i == 'body':
                    continue
                else:
                    xml_key += f"{i}/"
            return xml_key

        # update tree dictionary
        d_prms = dict(self.d_prms)
        for i in l_path_keys:
            d_prms = d_prms[i]
        if not d_prms.get(s_parm):
            print('error : the key {0} is not present in the PARAM.xml file'.format(
                l_path_keys.append(s_parm)))
            return
        d_prms[s_parm]['@value'] = str(value_to_store)

        # update xml-file
        if persist:
            try:
                p_xml = self.prm['param_xml_mod']
                tree = ET.parse(p_xml)
                xml_element = tree.getroot().findall(
                    generate_xml_key(l_path_keys) + s_parm)[0]
                xml_element.set('value', str(value_to_store))
                tree.write(p_xml)  # update the xml-file
            except:
                import traceback
                traceback.print_exc()
                print(f"Persisting {s_parm} with value {value_to_store} was not succesfull. Does param_xml_mod exist in the param file ?")

        # update working dict
        # self.prm[s_parm]=value_to_store
        self.prm[s_parm] = self.get_value(
            s_parm, l_path_keys=l_path_keys, use_main_keys=False)

        if verbose:
            print (
                '-XML-value {0} stored at {1}'.format(str(value_to_store), l_path_keys + [s_parm]))
        return

    @staticmethod
    def get_param_xml(input_source, l_main_keys=[], add_to_dict=False, merge_with_base=False, p_merged_out=None, verbose=False):
        '''
        in order of priority
        1) if merge_with_base: the input source must be an path to an xml file with a base_xml field.  The merged xml will be written to disk
        2) if a Param_xml object, or an xml path is passed as the FIRST argument use this
        3) if a path is passed as the FIRST argument use this to create a new Param_xml object
        4) if no arguments are given, load the PARAM.xml file in the current working directory

        '''
        if merge_with_base:
            if not p_merged_out:
                p_merged_out = Path(input_source).parent / (Path(input_source).stem + "_merged.xml")
            tree = Param_xml.merge_xml_with_base(input_source, p_out_xml=p_merged_out)
            if not tree:
                return
            input_source = p_merged_out

        if isinstance(input_source, pathlib.PurePath) or isinstance(input_source, str):
            param_xml = Param_xml(file=input_source, l_main_keys=l_main_keys)
        elif len(input_source) > 1:
            if isinstance(input_source[1], Param_xml):
                param_xml = input_source[1]
            else:
                param_xml = Param_xml(
                    file=input_source[1], l_main_keys=l_main_keys)
        else:
            file_name = 'PARAMS.xml'
            param_xml = Param_xml(file=os.path.join(
                os.getcwd(), file_name), l_main_keys=l_main_keys)

        if add_to_dict:
            param_xml.add_prms_to_dict()

        if verbose:
            print('param_xml_object from location {0} is used'.format(
                param_xml.file))
        return param_xml

    def retrieve_element(self, l_path_keys):
        if len(l_path_keys) == 0:
            d_param_sub = self.d_prms
        elif len(l_path_keys) == 1:
            d_param_sub = self.d_prms[l_path_keys[0]]
        elif len(l_path_keys) == 2:
            d_param_sub = self.d_prms[l_path_keys[0]][l_path_keys[1]]
        elif len(l_path_keys) == 3:
            d_param_sub = self.d_prms[l_path_keys[0]
                                      ][l_path_keys[1]][l_path_keys[2]]
        elif len(l_path_keys) == 4:
            d_param_sub = self.d_prms[l_path_keys[0]
                                      ][l_path_keys[1]][l_path_keys[2]][l_path_keys[3]]
        elif len(l_path_keys) == 5:
            d_param_sub = self.d_prms[l_path_keys[0]][l_path_keys[1]
                                                      ][l_path_keys[2]][l_path_keys[3]][l_path_keys[4]]
        else:
            print('ERROR : exceeded max depth for a xml path', l_path_keys)
            return None
        return d_param_sub

    def add_prms_to_dict(self, l_keys=[]):
        """ resolve all the parameters in a param-object and add the parameters to a dict
        a parameter is identifed if it has a key= '@value'
        """
        def add_prms_to_dict_recursion(d_iter, prm={}, key='', keys=[]):
            if '@value' in d_iter:
                # print('key={},keys={}'.format(key,keys))
                prm[key] = self.get_value(key, keys)
            else:
                for ix, key_i in enumerate(d_iter.keys()):
                    if key and ix == 0:
                        keys.append(key)
                    add_prms_to_dict_recursion(d_iter[key_i], prm, key_i, keys)
                if keys:
                    keys.pop()
            return prm

        d_sel = self.retrieve_element(self.l_main_keys + l_keys)

        d_prm = add_prms_to_dict_recursion(d_sel, keys=l_keys)
        self.prm.update(d_prm)

        return self.prm

    def file_rescaled(self):
        """ flag to indicate if you are working with a rescaled input file or not"""

        return True if (self.prm['scaling_ZYX_rescaled'] and (self.prm['scaling_ZYX_rescaled'] != self.prm['scaling_ZYX'])) else False


    def get_file_info(self, field="", rescaled=None):
        """ This method will return the rescaled info by default.  
        setting rescaled to False will return the original image info, 
        setting it to True will return the rescaled image info"""

        if 'img_raw_file_rescaled' not in self.prm:
            key_restore = self.l_main_keys
            self.l_main_keys = ['body']
            self.add_prms_to_dict(l_keys=['RAM'])
            self.l_main_keys = key_restore

        search_fields = ['img_raw_file', 'RAW_IMG_DIM_TZYX', 'scaling_ZYX']
        if rescaled is None:
            rescaled = self.file_rescaled()

        values = [self.prm[i + "_rescaled" if rescaled else i]
                  for i in search_fields if (i == field or field == "")]

        return values[0] if len(values) == 1 else values

    def repr_prm(self):
        import pprint
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(self.prm)
        return

    @staticmethod
    def merge_xml_with_base(param_mod, p_out_xml=None):
        ''' An xml will be merged with a base_xml'''
        tree_mod = ET.parse(param_mod)
        base_xml = tree_mod.getroot().findall("./sdt/paths/base_xml")
        if not base_xml:
            print(f"The XML file {param_mod} does not contain a reference to a base_xml file.  Abort merging")
            return
        base_xml = base_xml[0].get('value')
        tree_base = ET.parse(Path(param_mod).parent / base_xml)

        for xml_key in tree_mod.iter():
            update_value = xml_key.get('value')
            if update_value and xml_key.tag not in ['base_xml']:
                xml_element = tree_base.getroot().findall(f".//*/{xml_key.tag}")
                if not xml_element:
                    print(f"{xml_key.tag} was not found in {base_xml}.  This field is therefore not updated")
                    continue
                xml_element[0].set('value', update_value)
        if p_out_xml:
            tree_base.write(p_out_xml)
            
        return tree_base
