'''
Created on 27 dec 2021
locate parallel "z-membranes" in an confocal image of an embryo
@author: wimth
'''
import numpy as np
from tifffile import imsave
from skimage.filters import gaussian
from skimage.morphology import reconstruction
from skimage.draw import circle

from scipy.ndimage.morphology import distance_transform_edt
from skimage.feature import peak_local_max


def _remove_strong_membrane_signal(a_raw, a_membrane_mask, p_out=None):
    '''# step 1 : remove strong membrane signal, using an extended mask.
    this extended mask can later be used to remove artificial gradients
    introduced at the border of the mask
    '''

    a_membrane_overlay = np.where(a_membrane_mask > 0, 0, a_raw)
    # return a_membrane_mask
    a_extended_mask = np.where(a_membrane_overlay > 0, 0, 1)
    ix_z_max = a_membrane_overlay.shape[0] - 1
    for ix_z, a_slice in enumerate(a_membrane_overlay):
        if ix_z > 0:
            a_extended_mask[ix_z - 1] = np.where(np.logical_and(
                a_slice > 0, a_extended_mask[ix_z - 1] == 1), 0,
                a_extended_mask[ix_z - 1])
        if ix_z < ix_z_max:
            a_extended_mask[ix_z + 1] = np.where(np.logical_and(
                a_slice > 0, a_extended_mask[ix_z + 1] == 1), 0,
                a_extended_mask[ix_z + 1])

    # will not be used_just for vizualization
    a_raw_memb_stripped = np.where(a_extended_mask == 1, a_raw, 0)

    if p_out:
        imsave(p_out, a_raw_memb_stripped.astype('int16'))

    return a_raw_memb_stripped, a_extended_mask


def _smooth_in_XY(a_stack, a_raw=None, a_membrane_mask=None,
                  sigma=1, p_out=None):
    '''# smooth in XY (avoiding the strong membranes signal) to turn
    z-membrane into continuous patches
    if the raw image and membrane mask are passed,
    the smoothed image is overlayed with the raw image
    '''

    d_gaussian = {'sigma': [0, sigma, sigma], 'mode': 'nearest',
                  'preserve_range': True, 'truncate': 4.0}
    a_smoothedXY = gaussian(a_stack, **d_gaussian)

    if a_raw is not None and a_membrane_mask is not None:
        a_smoothedXY = np.where(a_membrane_mask == 0, a_raw, a_smoothedXY)

    if p_out:
        if np.max(a_smoothedXY) < 1:
            a_smoothedXY = a_smoothedXY * 100  # vis
        imsave(p_out, a_smoothedXY.astype('uint16'))

    return a_smoothedXY


def _calc_gradients(a_stack, a_extended_mask, Zres, p_out=None):
    '''# Calculate second derivate over Z (ddz)
    '''
    
    if Zres is not None:
        z_membrane_thickness_micron = 1
        d_gaussian = {'sigma': [z_membrane_thickness_micron / Zres, 0, 0], 'mode': 'nearest',
                  'preserve_range': True, 'truncate': 4.0}
        a_stack = gaussian(a_stack, **d_gaussian)
    else:
        sigma_z = 0

    a_ddz = np.gradient(np.gradient(a_stack, Zres, axis=0), Zres, axis=0)
    # we only want negative second derivative
    a_ddz = np.where(a_ddz > 0, 0, a_ddz * (-1))
    if p_out:
        a_ddz_viz = np.where(a_extended_mask == 0, 0,
                             a_ddz) if a_extended_mask is not None else a_ddz
        a_ddz_viz = a_ddz_viz * 100
        imsave(p_out, a_ddz_viz.astype('int16'))

    return a_ddz


def _find_local_max(a_stack, a_membrane_overlay, a_exterior_mask,
                    min_dist_peak_membrane, min_dist_between_peaks=3,
                    peak_thresh_rel=0.4, peak_thresh_abs=None,
                    scaling_ZYX=[1, 1, 1], a_raw=None,
                    df_thresholds=None,
                    p_out=None, verbose=False):
    ''' find local maxima (peaks)
    Will find peak values in a_stack using certain positional restrictions
        min_dist_peak_membrane : peak are only allowed a certain
        pixel distance from the membrane
        min_dist_between_peaks : peaks are not allowed too close to one another
        peak_tresh_rel : peaks must be higher than
        np.max(a_stack) * peak_thresh_rel
    '''

    a_dist_transf = distance_transform_edt(
        a_membrane_overlay == 0,
        sampling=[scaling_ZYX[0] / scaling_ZYX[1], 1, 1],
        return_distances=True, return_indices=False)
    a_mask = a_dist_transf > min_dist_peak_membrane

    a_stack_masked = np.where(a_exterior_mask == 0, 0, a_stack)

    if df_thresholds is not None and 'peak_thresh_abs' in df_thresholds:
        peak_thresh_abs = df_thresholds.iloc[0]['peak_thresh_abs']
        peak_thresh_rel = peak_thresh_abs / np.max(a_stack)
    else:
        peak_thresh_rel = (peak_thresh_abs / np.max(a_stack)
                           ) if peak_thresh_abs else peak_thresh_rel
        peak_thresh_abs = (peak_thresh_rel * np.max(a_stack)
                           ) if not peak_thresh_abs else peak_thresh_abs
    d_peak_local_max = {'labels': a_mask,
                        'min_distance': min_dist_between_peaks,
                        'threshold_abs': peak_thresh_abs,
                        'threshold_rel': None,
                        'exclude_border': True,
                        'indices': False,
                        'num_peaks': 10000,
                        'footprint': None,
                        'labels': None,
                        'num_peaks_per_label': 10000}

    a_bool_peak_img = peak_local_max(a_stack_masked, **d_peak_local_max)
    d_peak_local_max['indices'] = True
    a_l_indices = peak_local_max(a_stack_masked, **d_peak_local_max)

    if verbose:
        print("{0} peaks detected".format(a_l_indices.shape[0]))

    if p_out:  # visualize

        imsave(p_out.parent / "5_stack_masked.tif", (np.where(a_mask, a_stack_masked, 0)).astype('int16'))

        a_stack_copy = a_stack.copy()
        a_raw_copy = a_raw.copy() if a_raw is not None else None

        z, y, x = a_stack.shape
        for peak_idx in a_l_indices:
            rr, cc = circle(peak_idx[1], peak_idx[2], radius=4, shape=(y, x))
            a_stack_copy[peak_idx[0], rr, cc] = 0
            if a_raw_copy is not None:
                a_raw_copy[peak_idx[0], rr, cc] = 0

        imsave(p_out, (a_stack_copy * 100).astype('int16'))
        if a_raw_copy is not None:
            imsave(p_out.parent / "5_original_peak_overlay.tif",
                   a_raw_copy.astype('int16'))

    return (a_bool_peak_img, a_l_indices, a_dist_transf,
            peak_thresh_abs, np.max(a_stack))


def _filter_local_maxima(a_bool_peak_img, a_dist_transf, z_metric_mode,
                         SN_thresh=None, a_raw=None, a_membrane_mask=None,
                         df_thresholds=None, z_range_loc_max=None,
                         p_out=None, verbose=False):
    '''filter peaks at the bottom (and top) of the stack using heuristics.
    - A 'z_metric' is calculated which  uses the summed distance transform
    values at the peaks as a proxy of the number of significant peaks.
    When entering the embryo from the bottom we always see a 'bump'
    in the number of peaks (false positives)
    - secondly the number of peaks is also used
    - third S/N levels : we use the first peak slice as a proxy for
    overall stack quality.  If S/N ratio of this slice drops below 2,
    we switch of z-membrane completely
    '''
    d_log_filter_local_max = {}

    if z_metric_mode not in ['top-bottom', 'bottom']:
        return a_bool_peak_img, {}
    if z_range_loc_max:
        ix_min, ix_max = z_range_loc_max
    elif df_thresholds is not None and 'has_signal' in df_thresholds:
        ix_min = np.min(np.where(df_thresholds.has_signal))
        ix_max = np.max(np.where(df_thresholds.has_signal))
    else:
        #  determine z-metric peaks from both sides
        a_z_metric = np.sum(
            np.where(a_bool_peak_img, a_dist_transf, 0), axis=(1, 2))
        l_z_metric = list(a_z_metric)
        d_log_filter_local_max['z_metric'] = l_z_metric

        ix_min = 0
        z_metric_high = 0
        for ix_z, z_metric_i in enumerate(l_z_metric):
            if z_metric_i < z_metric_high:
                break
            else:
                z_metric_high = z_metric_i
                ix_min = ix_z

        # alter z-range based on number of peaks found
        ix_max = len(l_z_metric) - 1
        z_metric_high = 0
        a_nb_peaks = np.sum(a_bool_peak_img, axis=(-2, -1))
        d_log_filter_local_max['nb_peaks'] = a_nb_peaks
        for ix in range(len(l_z_metric) - 1, -1, -1):
            if l_z_metric[ix] < z_metric_high:
                ix_max = ix + 1
                if a_nb_peaks[ix_max] < 2 and ix_max + 1 < len(a_nb_peaks):
                    ix_max += 1
                break
            else:
                z_metric_high = l_z_metric[ix]

    # heuristic based on S/N
    if SN_thresh is not None:
        a_non_membrane_overlay = np.where(a_membrane_mask == 0, 0, a_raw)
        a_membrane_overlay = np.where(a_membrane_mask == 0, a_raw, 0)
        l_MS_signal, l_MS_noise = [], []
        for ix_z in range(a_membrane_mask.shape[0]):
            slice_signal = a_membrane_overlay[ix_z]
            slice_noise = a_non_membrane_overlay[ix_z]
            l_MS_signal.append(
                np.mean(np.square(slice_signal[slice_signal > 0])))
            l_MS_noise.append(np.mean(np.square(slice_noise[slice_noise > 0])))
        l_SN_ratio = [signal / noise for signal,
                      noise in zip(l_MS_signal, l_MS_noise)]
        d_log_filter_local_max['SN_ratio'] = l_SN_ratio

        if l_SN_ratio[ix_min] < SN_thresh:
            ix_max = ix_min

    # filter peaks according the z_metric_mode and the determined z-range
    if z_metric_mode == 'top-bottom':
        a_bool_peak_img[:ix_min + 1, ...] = 0
        a_bool_peak_img[ix_max:, ...] = 0

    elif z_metric_mode == 'bottom':
        a_bool_peak_img[ix_max:, ...] = 0
        ix_min = 'NA'
    d_log_filter_local_max['z_filter_range'] = [ix_min, ix_max]

    if p_out:
        imsave(p_out, a_bool_peak_img.astype('int16'))

    return a_bool_peak_img, d_log_filter_local_max


def _flood_peaks(a_stack, a_bool_peak_img, peak_thresh_rel_slice,
                 a_raw=None, df_thresholds=None,
                 p_out=None, verbose=False):

    def _calc_peak_thresh_abs_slice():

        if peak_thresh_rel_slice > 0:
            peak_thresh_abs_slice = np.max(a_slice_i) * (peak_thresh_rel_slice)
        else:
            non_zero_peaks = a_seed[ix_z][np.nonzero(a_seed[ix_z])]
            if len(non_zero_peaks) == 0:
                peak_thresh_abs_slice = 0
            else:
                # the threshold will be extended to the lowest peak detected
                peak_thresh_abs_slice = np.min(non_zero_peaks)
                peak_thresh_abs_slice *= 0.95

        return peak_thresh_abs_slice

    a_peaks_flooded = a_bool_peak_img.copy()
    a_seed = np.where(a_bool_peak_img, a_stack, 0)
    if df_thresholds is not None \
            and 'peak_thresh_abs_slice' in df_thresholds:
        l_peak_thresh_abs_slice = list(
            df_thresholds['peak_thresh_abs_slice'])
    else:
        l_peak_thresh_abs_slice = [None] * len(a_stack)

    for ix_z, (a_slice_i, peak_thresh_abs_slice) in enumerate(
            zip(a_stack, l_peak_thresh_abs_slice)):
        if not peak_thresh_abs_slice:
            peak_thresh_abs_slice = _calc_peak_thresh_abs_slice()
            l_peak_thresh_abs_slice[ix_z] = peak_thresh_abs_slice

        if np.sum(a_seed[ix_z]) == 0:  # no peaks or peaks with zero value
            a_peaks_flooded[ix_z] = a_bool_peak_img[ix_z]
            continue

        seed = a_slice_i.copy()
        seed[seed < peak_thresh_abs_slice] = 0
        seed[a_bool_peak_img[ix_z] == 0] = 0
        a_slice_i[a_slice_i < peak_thresh_abs_slice] = 0
        a_peaks_flooded[ix_z] = reconstruction(
            seed=seed, mask=a_slice_i, method='dilation')

    if p_out and a_raw is not None:
        a_peaks_flooded_over_original = np.where(
            a_peaks_flooded > 0, np.max(a_raw), a_raw)
        imsave(p_out, a_peaks_flooded_over_original.astype('int16'))

    return a_peaks_flooded, l_peak_thresh_abs_slice


def locate_z_membrane(a_raw, a_membrane_mask, a_exterior_mask,
                      scaling_ZYX, sigma_ddz_smoothing=4,
                      min_dist_peak_membrane=5, min_dist_between_peaks=3,
                      peak_thresh_rel=0.4, peak_thresh_rel_slice=0,
                      SN_thresh=2, z_metric_mode="top-bottom",
                      df_thresholds=None, z_range_loc_max=None,
                      p_out_folder=None, verbose=False):
    """
    in :
            - a_raw : nd_array of the original input
            - a_membrane_mask: a mask that  hides the bright
                membrane already detected (non parallel membranes).
            - a_exterior_mask: a mask that hides the outside of the embryo
            - sigma_ddz_smoothin : smoothing of the ddz values = d_gaussian2
            - min_dist_peak_membrane : the minimum distance of a peak to a
                membrane pixels. Used to give peaks near the
                center of the membrane

    out:
            nd-array : boolean: True = location of a z-membrane

    This module will try to detect parallel z-membranes
    - the strong signal that is already detected is removed
        (=the binary membrane image after thresholding the frangi (XY) output
    - makes use of an extended membrane mask i.e.  the all pixels above
        and below the membrane is added (this is because these pixels
        will have an incorrect z-gradient)
    - no z-smoothing is done ! This is because a membrane can occur over
        1 Z-pixel, so we do not want to blur this dimension.
        We do want to aggregate in XY to share plane info
    - because we only want parallel Z planes we only look at ddz info.
        (and ) ddx and ddy information turned out not to be informative)
    """

    d_log = {}
    if p_out_folder:
        p_out_folder.mkdir(parents=True, exist_ok=True)

    a_raw_memb_stripped, a_extended_mask = _remove_strong_membrane_signal(
        a_raw, a_membrane_mask,
        p_out=p_out_folder / "01_raw_memb_stripped"
        if verbose else None
    )

    a_smoothedXY_filled = _smooth_in_XY(
        a_raw_memb_stripped, a_raw, a_extended_mask,
        p_out=p_out_folder / "02_smoothedXY_filled.tif"
        if verbose else None
    )

    a_ddz = _calc_gradients(
        a_smoothedXY_filled, a_extended_mask=None, Zres=scaling_ZYX[0],
        p_out=p_out_folder / "03_ddz_extended_mask.tif"
        if verbose else None
    )

    a_ddz_smoothedXY_overlay = _smooth_in_XY(
        np.where(a_extended_mask == 0, 0, a_ddz), sigma=sigma_ddz_smoothing,
        p_out=p_out_folder / "04_ddz_smoothedXY_overlay.tif"
        if verbose else None
    )

    a_membrane_overlay = np.where(a_membrane_mask > 0, 0, a_raw)

    a_bool_peak_img, a_l_indices, a_dist_transf, \
        d_log['peak_thresh_abs'], d_log['max_stack'] = _find_local_max(
            a_ddz_smoothedXY_overlay, a_membrane_overlay, a_exterior_mask,
            min_dist_peak_membrane, min_dist_between_peaks, peak_thresh_rel,
            df_thresholds=df_thresholds,
            p_out=p_out_folder / "05_ddzsmooth_peak_overlay.tif"
            if verbose else None
        )

    a_bool_peak_img, d_log_filter_local_max = _filter_local_maxima(
        a_bool_peak_img, a_dist_transf,
        z_metric_mode, SN_thresh, a_raw, a_membrane_mask, df_thresholds,
        z_range_loc_max, p_out=p_out_folder / "06_bool_peak_img.tif"
        if verbose else None
    )

    a_ddz_smoothedXY_overlay_masked = np.where(
        a_exterior_mask == 0, 0, a_ddz_smoothedXY_overlay)
    a_peaks_flooded, d_log['peak_thresh_abs_slice'] = _flood_peaks(
        a_ddz_smoothedXY_overlay_masked, a_bool_peak_img,
        peak_thresh_rel_slice, a_raw=a_raw, df_thresholds=df_thresholds,
        p_out=p_out_folder / "07_extended a_peaks_extend_over_original.tif"
        if verbose else None
    )
    d_log['max_slice'] = np.max(a_ddz_smoothedXY_overlay_masked, axis=(1, 2))

    return a_peaks_flooded, d_log
