#!/bin/bash
# parses the arguments given to a testinfra execution

# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

# Initialize our own variables:
sleep=true
grouping=false
dry_run=false
ARG_COUNTER=1  # first argument is reserverd for main operation
ARG_p=0
ARG_r=0
ARG_a=0
ARG_s=0
ARG_w=0

while getopts "hbvgsrpdaw" opt; do
	case "$opt" in
	h)
		# echo "Documentation currently not provided (see script header)"
		echo "	-r : restart module
	-p : max number of parallel jobs allowed
	-g : grouping (bool)
	-b : batch mode set to true (breakpoints disabled)
	-a : switch to an alternate_location e.g. external drive (cfr testinfra_config)
	-d : dryrun 
	-w : sleeptime at end (after the final wait). Use when parallel run is chained with other jobs
	e.g ./testinfra.sh -r -p -g wt13 4 6  = executing of testcases with grouplabel starting with wt13 restarting at module 4, using max 6 parallel jobs
	The first argument should always reflect the testcase, and can be
	  - a testcase label e.g. wt1305
	  - a group label e.g. wt13S
	  - 'EXEC' : which will trigger all testcase marked with 'EXEC'"
		exit 0
		;;
	v)  verbose=true
		;;
	g)  grouping=true
		;;
	p)  let ARG_COUNTER++
		ARG_p=$ARG_COUNTER
		;;
	r)  let ARG_COUNTER++
		ARG_r=$ARG_COUNTER
		;;
	# s)  sleep=false
	# 	;;
	d)  dry_run=true
		;;
	a)  let ARG_COUNTER++
		ARG_a=$ARG_COUNTER
		;;
	s)  let ARG_COUNTER++
		ARG_s=$ARG_COUNTER
		;;
	w)  let ARG_COUNTER++
		ARG_w=$ARG_COUNTER
		;;
	b)  batch_mode=true
	esac
done

shift $((OPTIND-1))

[ "${1:-}" = "--" ] && shift

if [ $ARG_r != 0 ];then
	restart_step=${@: ${ARG_r}: 1}
else
	restart_step=0
fi

if [ $ARG_p != 0 ];then
	nb_parallel_jobs=${@: ${ARG_p}: 1}
else
	nb_parallel_jobs=3
fi

if [ $ARG_a != 0 ];then
	use_location=${@: ${ARG_a}: 1}
else
	use_location=0
fi

if [ $ARG_s != 0 ];then
	sleeptime=${@: ${ARG_s}: 1}
else
	sleeptime=10
fi

if [ $ARG_w != 0 ];then
	sleeptime_end=${@: ${ARG_w}: 1}
else
	sleeptime_end=1
fi

#Set variables --------------------------------------------------------------------
if [ $# -eq 0 ]
then
	testcase=""
else
	testcase=$1  # main module always first argument
	if [ $testcase == "EXEC" ]
	then
		flag_use_exec=true
	else
		flag_use_exec=false
	fi
fi


#Report-----------------------------------------------
echo "${0} - testcase:${testcase}, restart_step:${restart_step}, grouping:${grouping}"
if [ $0 == "./testinfra_parallel.sh" ]
then
	echo  "     => nb_parallel_jobs: ${nb_parallel_jobs}, dry_run: ${dry_run}, sleep: ${sleep}, use_location: ${use_location}"
fi

